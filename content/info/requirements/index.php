<?php
  $page_title = "KDE Compilation Requirements Listings";
  $site_root = "../../";
  include "header.inc";
?>

<ul>
  <li><a href="3.5.php">KDE 3.5</a></li>
  <li><a href="3.4.php">KDE 3.4</a></li>
  <li><a href="3.3.php">KDE 3.3</a></li>
  <li><a href="3.2.php">KDE 3.2</a></li>
  <li><a href="3.1.php">KDE 3.1</a></li>
  <li><a href="3.0.php">KDE 3.0</a></li>
</ul>

<?php
  include "footer.inc";
?>
