<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">SHA1&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.3.80/src/kdeaccessibility-4.3.80.tar.bz2">kdeaccessibility-4.3.80</a></td><td align="right">6,2MB</td><td><tt>f4680829d2166903ffa98d2df0f7a7495e1ddc75</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.3.80/src/kdeadmin-4.3.80.tar.bz2">kdeadmin-4.3.80</a></td><td align="right">1,8MB</td><td><tt>8581fd4906384cae189f818b9ce37cc5bd7d40bd</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.3.80/src/kdeartwork-4.3.80.tar.bz2">kdeartwork-4.3.80</a></td><td align="right">63MB</td><td><tt>d1ef0744deeb7cb4c0ca4309ba9edf6e1ca71d0a</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.3.80/src/kdebase-4.3.80.tar.bz2">kdebase-4.3.80</a></td><td align="right">3,6MB</td><td><tt>71ad02fbe12c9b32b64bb4167b280056669ed419</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.3.80/src/kdebase-runtime-4.3.80.tar.bz2">kdebase-runtime-4.3.80</a></td><td align="right">7,1MB</td><td><tt>71e16452590b6c3a2e47e3f8de9237210da8d4fa</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.3.80/src/kdebase-workspace-4.3.80.tar.bz2">kdebase-workspace-4.3.80</a></td><td align="right">60MB</td><td><tt>03b1ba3e46622ead0f0566384443914fb3391747</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.3.80/src/kdebindings-4.3.80.tar.bz2">kdebindings-4.3.80</a></td><td align="right">4,9MB</td><td><tt>485568172e0d4de11b49f2263d3175a2e59ce559</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.3.80/src/kdeedu-4.3.80.tar.bz2">kdeedu-4.3.80</a></td><td align="right">62MB</td><td><tt>2b8238fd7890d41b28de9b7ac24cf9d9e62ddc9e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.3.80/src/kdegames-4.3.80.tar.bz2">kdegames-4.3.80</a></td><td align="right">68MB</td><td><tt>95d474334eeb3c69745440db979ce8bb9595e6c1</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.3.80/src/kdegraphics-4.3.80.tar.bz2">kdegraphics-4.3.80</a></td><td align="right">3,7MB</td><td><tt>4180a0fd2c5ab52d88f22879c0606894b32c50bd</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.3.80/src/kdelibs-4.3.80.tar.bz2">kdelibs-4.3.80</a></td><td align="right">13MB</td><td><tt>004200a6546e9ef0221e6e949ee95adda55a3cb9</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.3.80/src/kdemultimedia-4.3.80.tar.bz2">kdemultimedia-4.3.80</a></td><td align="right">1,6MB</td><td><tt>a8c6d704a57b0c67d5892918c2c34fa0cf145f66</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.3.80/src/kdenetwork-4.3.80.tar.bz2">kdenetwork-4.3.80</a></td><td align="right">7,5MB</td><td><tt>6cf262e4e1f7a7d2175928e8fc0b95ceecf81766</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.3.80/src/kdepim-4.3.80.tar.bz2">kdepim-4.3.80</a></td><td align="right">11MB</td><td><tt>74a8a24a7bd7906bab103091e4b52fcfac270a5e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.3.80/src/kdepimlibs-4.3.80.tar.bz2">kdepimlibs-4.3.80</a></td><td align="right">2,4MB</td><td><tt>d8bb7a7f4edfa5771bda9eae65bc21ea020d5129</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.3.80/src/kdepim-runtime-4.3.80.tar.bz2">kdepim-runtime-4.3.80</a></td><td align="right">816KB</td><td><tt>a34991dca352fb788abe154474ae8f3aea606a2d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.3.80/src/kdeplasma-addons-4.3.80.tar.bz2">kdeplasma-addons-4.3.80</a></td><td align="right">1,6MB</td><td><tt>abde3340e69277acadfff01f84ca0d5e7306cb05</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.3.80/src/kdesdk-4.3.80.tar.bz2">kdesdk-4.3.80</a></td><td align="right">5,5MB</td><td><tt>964c442893393be326c51ba4f29912d4144bc31c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.3.80/src/kdetoys-4.3.80.tar.bz2">kdetoys-4.3.80</a></td><td align="right">1,3MB</td><td><tt>2f32562212e4134218b0f84d0809364e3c0f08af</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.3.80/src/kdeutils-4.3.80.tar.bz2">kdeutils-4.3.80</a></td><td align="right">2,5MB</td><td><tt>b49356c553410185b592f34c6aa60a643011072a</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.3.80/src/kdewebdev-4.3.80.tar.bz2">kdewebdev-4.3.80</a></td><td align="right">2,1MB</td><td><tt>a496399d35dbbdd34dc96c9b80bf09249b00135f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.3.80/src/oxygen-icons-4.3.80.tar.bz2">oxygen-icons-4.3.80</a></td><td align="right">127MB</td><td><tt>13d0bd6c43ba1712b28cedaf249574f17700dad8</tt></td></tr>
</table>
