<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.92/src/kdeaccessibility-3.92.0.tar.bz2">kdeaccessibility-3.92.0</a></td><td align="right">7.5MB</td><td><tt>1c5cdd52eb5995fa01e7257470596c54</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.92/src/kdeaddons-3.92.0.tar.bz2">kdeaddons-3.92.0</a></td><td align="right">753KB</td><td><tt>a6186a1f1e8723bf94ffcea31e960928</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.92/src/kdeadmin-3.92.0.tar.bz2">kdeadmin-3.92.0</a></td><td align="right">1.4MB</td><td><tt>ba66c4967c76153e5b3487ca0feaf1db</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.92/src/kdeartwork-3.92.0.tar.bz2">kdeartwork-3.92.0</a></td><td align="right">43MB</td><td><tt>7fd03e734e46618b44471c3efd5d8666</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.92/src/kdebase-3.92.0.tar.bz2">kdebase-3.92.0</a></td><td align="right">21MB</td><td><tt>7a8731fc696512044463537314b75281</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.92/src/kdebase-workspace-3.92.0.tar.bz2">kdebase-workspace-3.92.0</a></td><td align="right">8.4MB</td><td><tt>6f102ab8918e985025abd04e1313c14c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.92/src/kdeedu-3.92.0.tar.bz2">kdeedu-3.92.0</a></td><td align="right">37MB</td><td><tt>aed596af10a241746429ca481bd4cc03</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.92/src/kdegames-3.92.0.tar.bz2">kdegames-3.92.0</a></td><td align="right">23MB</td><td><tt>515d77676c4c6d3c201cc09fcfa5d693</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.92/src/kdegraphics-3.92.0.tar.bz2">kdegraphics-3.92.0</a></td><td align="right">2.5MB</td><td><tt>467b71ae9e4d3c5b147d88e6d31dda9f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.92/src/kdelibs-3.92.0.tar.bz2">kdelibs-3.92.0</a></td><td align="right">56MB</td><td><tt>1a32d2892c9a7ac4a7fd4d77a86f720b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.92/src/kdemultimedia-3.92.0.tar.bz2">kdemultimedia-3.92.0</a></td><td align="right">4.0MB</td><td><tt>3dbe042cd6ec6a818e4ce3e36387949c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.92/src/kdenetwork-3.92.0.tar.bz2">kdenetwork-3.92.0</a></td><td align="right">6.8MB</td><td><tt>1ebdfc1478d75d4c5dce2f80fbd6dacd</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.92/src/kdepim-3.92.0.tar.bz2">kdepim-3.92.0</a></td><td align="right">13MB</td><td><tt>f984587a26d69e980bc54ab47126f116</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.92/src/kdepimlibs-3.92.0.tar.bz2">kdepimlibs-3.92.0</a></td><td align="right">1.6MB</td><td><tt>0fa2ff6f04f5176717776dec6753edba</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.92/src/kdesdk-3.92.0.tar.bz2">kdesdk-3.92.0</a></td><td align="right">5.3MB</td><td><tt>15613a0e9c20219ba796fcdab4050f89</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.92/src/kdetoys-3.92.0.tar.bz2">kdetoys-3.92.0</a></td><td align="right">2.2MB</td><td><tt>8d274cd9597844a8ae980b7be2b99fbf</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.92/src/kdeutils-3.92.0.tar.bz2">kdeutils-3.92.0</a></td><td align="right">2.0MB</td><td><tt>3f483c6657c85817a08b82a39f866ec2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.92/src/koffice-1.9.92.tar.bz2">koffice-1.9.92</a></td><td align="right">61MB</td><td><tt>5da2a2d1a306750c76a54599abe2fb73</tt></td></tr>
</table>
