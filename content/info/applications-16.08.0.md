---
version: "16.08.0"
title: "KDE Applications 16.08.0 Info Page"
announcement: /announcements/announce-applications-16.08.0
type: info/application-v1
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
