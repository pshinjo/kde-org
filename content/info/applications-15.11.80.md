---
version: "15.11.80"
title: "KDE Applications 15.11.80 Info Page"
announcement: /announcements/announce-applications-15.12-beta
type: info/application-v1
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
