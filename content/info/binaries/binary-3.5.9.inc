<ul>

<!-- ARCH LINUX -->
<!-- <li><a href="http://www.archlinux.org/">Arch Linux</a>
  :
  <ul type="disc">
    <li>i686 Packages: <a href="ftp://ftp.archlinux.org/extra/os/i686">ftp://ftp.archlinux.org/extra/os/i686</a></li>
    <li>
      To install: pacman -S kde
    </li>
  </ul>
  <p />
</li> -->

<!-- Debian -->
<li><a href="http://www.debian.org/">Debian</a>
    <ul type="disc">
      <li>
         Debian packages will soon be in Debian unstable.
      </li>
    </ul>
  <p />
</li>

<!-- Fedora -->
<li><a href="http://fedoraproject.org/">Fedora</a>
    <ul type="disc">
      <li>
         KDE 3.5.9 updates for Fedora 7 and 8 have been built and pushed as updates" 
      </li>
    </ul>
  <p />
</li>

<!-- KUBUNTU -->
<li><a href="http://www.kubuntu.org/">Kubuntu</a>
    <ul type="disc">
      <li>
         <a href="http://kubuntu.org/announcements/kde-359.php">Kubuntu Hardy and 7.10</a>
      </li>
    </ul>
  <p />
</li>

<!-- Mandriva -->
<li>
	<a href="http://www.mandriva.com/">Mandriva</a>
Mandriva packages are provided now in official Mandriva repositories.
For Mandriva 2008.0, users should enabled backports repositories. More info on how to enable backports, you can find in
<a href="http://wiki.mandriva.com/en/Docs/Basic_tasks/Installing_and_removing_software#Advanced_use:_Backports_and_candidate_updates">Backports and Candidate Updates</a>.
2008.0 backports are kindly provided by Nicolas Lecureuil.
For cooker and pre 2008.1 release candidate users, cooker repositories already have packages updated, just need choose your regular mirror and update your kde as usual.
</li>

<!-- Pardus -->
<!-- <li>
 <a href="http://www.pardus.org.tr/">Pardus</a>
    <ul type="disc">
      <li>
         Pardus 2007.2: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.9/Pardus/2007.2/">Intel i386</a>
      </li>
    </ul>
  <p />
</li> -->

<!-- kde-redhat -->
<!--
<li><a href="http://kde-redhat.sourceforge.net/">KDE RedHat (unofficial) Packages</a>:
(<a href="http://apt.kde-redhat.org/apt/kde-redhat/kde-org.txt">README</a>)
<ul type="disc">

 <li>All distributions: 
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/all/">(noarch,SRPMS)</a></li>

 <li>Red Hat Enterprise Linux 4:
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/redhat/kde.repo">(kde.repo)</a>,
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/redhat/4/i386/">(i386)</a>

 	<a href="http://apt.kde-redhat.org/apt/kde-redhat/redhat/4/x86_64/">(x86_64)</a>
 </li>

 <li>Fedora Core 5:
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/kde.repo">(kde.repo)</a>,
 	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/5/i386/">(i386)</a>
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/5/x86_64/">(x64_64)</a>
</li>

 <li>Fedora Core 6:
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/kde.repo">(kde.repo)</a>,
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/6/i386/">(i386)</a>
 	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/6/x86_64/">(x64_64)</a>
</li>

<li>Fedora 7:
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/kde.repo">(kde.repo)</a>,
        <a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/7/i386/">(i386)</a>
        <a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/7/x86_64/">(x64_64)</a>

</li> 

</ul>
 <p />
</li>
-->

<!-- openSUSE -->
<li>
  <a href="http://www.opensuse.org">openSUSE</a><br>
  The openSUSE KDE packages are developed in the openSUSE <a
href="http://en.opensuse.org/Build_Service">Build Service</a>. Please read <a
href="http://en.opensuse.org/KDE/Upgrade">this page</a> for how to upgrade your KDE installation.<br>
  <ul type="disc">
    <li>
        <a
href="http://software.opensuse.org/download/repositories/KDE:/KDE3/openSUSE_10.3/">openSUSE
10.3 YUM repository</a> (64bit and 32bit)
    </li>
    <li>
        <a
href="http://software.opensuse.org/download/repositories/KDE:/KDE3/openSUSE_10.2/">openSUSE
10.2 YUM repository</a> (64bit and 32bit)
    </li>
    <li>
        <a
href="http://software.opensuse.org/download/repositories/KDE:/KDE3/SUSE_Linux_10.1/">SUSE
Linux 10.1 YUM repository</a> (64bit and 32bit)
    </li>
  </ul>
  <p />
</li>


</ul>
