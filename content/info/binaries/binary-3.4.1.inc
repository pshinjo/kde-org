<ul>

<!-- CONECTIVA LINUX -->
<li><a href="http://www.conectiva.com/">Conectiva Linux</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.1/Conectiva/README-CL10">README</a>)
  :
  <ul type="disc">
    <li>
      Conectiva 10:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.1/Conectiva/conectiva/">Intel i386</a>
    </li>
  </ul>
  <p />
</li>

<!-- KUBUNTU -->
<li><a href="http://www.kubuntu.org/">Kubuntu</a>
    <ul type="disc">
      <li>
         Hoary (Intel i386): <tt><a href="http://www.kubuntu.org/announcements/hoary-kde-341.php">Kubuntu KDE 3.4.1 packages page</a></tt>
      </li>
    </ul>
  <p />
</li>

<!-- KDE-RedHat -->
<li>
 <a href="http://kde-redhat.sourceforge.net/">KDE-RedHat (unofficial) Packages</a>:
 <ul type="disc">
 <li>
 <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.1/contrib/kde-redhat/all/">all platforms</a>
 </li>
 <li>
 <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.1/contrib/kde-redhat/redhat/9/">Red Hat 9</a>
 </li>
 <li>
 <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.1/contrib/kde-redhat/fedora/2/i386/">Fedora Core 2</a>
 </li>
 <li>
 <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.1/contrib/kde-redhat/fedora/3/i386/">Fedora Core 3</a>
 </li>
 <li>
 <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.1/contrib/kde-redhat/redhat/el3/i386/">Red Hat Enterprise 3</a>
 </li>
 <li>
 <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.1/contrib/kde-redhat/redhat/el4/i386/">Red Hat Enterprise 4</a>
 </li>

 </ul>
 <p />
</li>

<!-- SLACKWARE LINUX -->
<li>
  <a href="http://www.slackware.org/">Slackware</a> (Unofficial contribution)
 (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.1/contrib/Slackware/10.1/README">README</a>)
   :
   <ul type="disc">
     <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.1/contrib/Slackware/noarch/">Language packages</a>
     </li>
     <li>
        10.1: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.1/contrib/Slackware/10.1/">Intel i486</a>
     </li>
   </ul>
  <p />
</li>

<!-- SOLARIS -->
<li>
 <a href="http://solaris.kde.org/">KDE Solaris</a> (Unofficial contribution)
(<a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.1/contrib/Solaris/10/SunStudio10/README">README</a>)
 :
 <ul type="disc">
 <li>
 10: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.1/contrib/Solaris/10/SunStudio10/IA32AMD32/">Intel/AMD (SunStudio)</a> and
     <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.1/contrib/Solaris/10/SunStudio10/ULTRASPARC/">UltraSPARC-II 32-bit (SunStudio)</a>
 </li>
 </ul>
 <p />
</li>

<!--   SUSE LINUX -->
<li>
  <a href="http://www.novell.com/linux/suse/">SuSE Linux</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.1/SuSE/README">README</a>)
      :
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.1/SuSE/noarch/">Language
        packages</a> (all versions and architectures)
    </li>
    <li>
      9.3:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.1/SuSE/ix86/9.3/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.1/SuSE/x86_64/9.3/">AMD x86-64</a>
    </li>
    <li>
      9.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.1/SuSE/ix86/9.2/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.1/SuSE/x86_64/9.2/">AMD x86-64</a>
    </li>
    <li>
      9.1:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.1/SuSE/ix86/9.1/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.1/SuSE/x86_64/9.1/">AMD x86-64</a>
    </li>
    <li>
      9.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.1/SuSE/ix86/9.0/">Intel i586</a>
    </li>
  </ul>
  <p />
</li>



</ul>
