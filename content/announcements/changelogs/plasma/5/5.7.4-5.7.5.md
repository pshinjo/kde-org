---
aliases:
- /announcements/plasma-5.7.4-5.7.5-changelog
hidden: true
plasma: true
title: Plasma 5.7.5 Complete Changelog
type: fulllog
version: 5.7.5
---

### <a name='breeze' href='http://quickgit.kde.org/?p=breeze.git'>Breeze</a>

- Homogenize how toolbutton arrow is detected between. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=325c8f610659fa48a38783f31260f970be7600f3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/367723'>#367723</a>

### <a name='kde-cli-tools' href='http://quickgit.kde.org/?p=kde-cli-tools.git'>kde-cli-tools</a>

- Make sure people are not trying to sneak invisible characters on the kdesu label. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=5eda179a099ba68a20dc21dc0da63e85a565a171'>Commit.</a>
- Include KDECompilerSettings earlier to silence cmake warning. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=f24fb29f91fecf253a034b8376c8a7b204b20d52'>Commit.</a>

### <a name='kdeplasma-addons' href='http://quickgit.kde.org/?p=kdeplasma-addons.git'>Plasma Addons</a>

- [Weather] Fix: use translated name of windDirection for display. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=3075c13c94a6d9c572752e5658a8bec9d1aa868a'>Commit.</a>

### <a name='kwayland-integration' href='http://quickgit.kde.org/?p=kwayland-integration.git'>KWayland-integration</a>

- Silence CMake policy CMP0063 warning. <a href='http://quickgit.kde.org/?p=kwayland-integration.git&amp;a=commit&amp;h=2a7872e3e84da37b607156ea093355b8abef08f9'>Commit.</a>

### <a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a>

- [kwinglutils] Skip ShaderManager::selfTest for NVIDIA Quadro hardware. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e9e936b6c1bde338fb51ea3ada0897ad70055c44'>Commit.</a> See bug <a href='https://bugs.kde.org/367766'>#367766</a>

### <a name='oxygen' href='http://quickgit.kde.org/?p=oxygen.git'>Oxygen</a>

- Re-added WindowDragEnabled, for backward compatibility with oxygen-gtk. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=06ac09ba9196d9968feefae294b6b361fb26cfd6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/367894'>#367894</a>
- Homogenize how toolbutton arrow is detected between. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=6f6a9ad34d24a14abd0c2159715a559b9ea87c69'>Commit.</a>

### <a name='plasma-integration' href='http://quickgit.kde.org/?p=plasma-integration.git'>plasma-integration</a>

- Set transientParent window for file dialog. <a href='http://quickgit.kde.org/?p=plasma-integration.git&amp;a=commit&amp;h=4a50ee3212d734db4a1cdd4fc24308be23cdf7da'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/367699'>#367699</a>

### <a name='plasma-nm' href='http://quickgit.kde.org/?p=plasma-nm.git'>Plasma Networkmanager (plasma-nm)</a>

- Allow negative priorities since NetworkManager's specification allows that [1]. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=65391985791da862adf2f6f678a540b3963db3db'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366708'>#366708</a>
- Add upload speed into account for maximum plotter value. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=8962e3b6837e8a6a047a848763f0ef5b2b91632d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368247'>#368247</a>. Code review <a href='https://git.reviewboard.kde.org/r/128832'>#128832</a>
- Export tls-auth tag for the other connection types too. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=4eede9a350e36c84594952dd1f2c999c0d02d2ec'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365458'>#365458</a>

### <a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a>

- [Weather] Use QNetworkConfigurationManager to know about online status. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6a86b1ef05f446eb71593fc6a18b25b03e90f82d'>Commit.</a> See bug <a href='https://bugs.kde.org/366827'>#366827</a>
- [Weather] Remove outdated ion loading code, use Plasma::DataEngineConsumer API. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f995cbb95314eaf19bc518d936fa8fff412f5c07'>Commit.</a>
- Fix applets not being added on service restart. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f148bdaf2c10e25c8fe5df32b267f0093875c861'>Commit.</a>
- Fix some status notifier items not appearing. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=df4387a21f6eb5ede255ea148143122ae4d5ae9c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366283'>#366283</a>. Fixes bug <a href='https://bugs.kde.org/367756'>#367756</a>
- Don't set Service as parent to KJob. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=19e1d474d99acb6fdfb46751742aa3e9abebf392'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361450'>#361450</a>
- Exempt tasks demanding attention from by-activity filtering as well. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=87f8b790702d12571ce964d89dfb1f0cadeed834'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365970'>#365970</a>

### <a name='sddm-kcm' href='http://quickgit.kde.org/?p=sddm-kcm.git'>SDDM KCM</a>

- Fix themes list with SDDM 0.14. <a href='http://quickgit.kde.org/?p=sddm-kcm.git&amp;a=commit&amp;h=d4ca70001222c5b0a9f699c4639c891a6a5c0c05'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128815'>#128815</a>
- Silence CMake policy CMP0063 warning. <a href='http://quickgit.kde.org/?p=sddm-kcm.git&amp;a=commit&amp;h=24dd11cdf8d739ffc619d268e93791e7530ab22a'>Commit.</a>