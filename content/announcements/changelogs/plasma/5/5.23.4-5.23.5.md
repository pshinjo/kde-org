---
title: Plasma 5.23.5 complete changelog
version: 5.23.5
hidden: true
plasma: true
type: fulllog
---
{{< details title="Bluedevil" href="https://commits.kde.org/bluedevil" >}}
+ Show connected but non-paired devices. [Commit.](http://commits.kde.org/bluedevil/efc104fa11c9b669bacbd107f758d484a61bd31d) Fixes bug [#432715](https://bugs.kde.org/432715)
+ [kcm] Fix device type detection for AudioVideo devices. [Commit.](http://commits.kde.org/bluedevil/9fb13aa2c3998bf54977b502960c766ee2c40068) 
+ [sendfile] Use error icon that actually exists. [Commit.](http://commits.kde.org/bluedevil/342ccaa5742e4bc212e521ce9f5343cbcc01781d) 
+ Save bluetooth status on teardown. [Commit.](http://commits.kde.org/bluedevil/daaa18920ae93b482585eb3df790a6d5a647b6db) Fixes bug [#445376](https://bugs.kde.org/445376)
{{< /details >}}

{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ [kstyle] Fix logic error in drawIndicatorButtonDropDownPrimitive. [Commit.](http://commits.kde.org/breeze/2aa08ed366917b7b207842844b78e5dadd5a06ed) 
{{< /details >}}

{{< details title="breeze-gtk" href="https://commits.kde.org/breeze-gtk" >}}
+ Gtk3: custom GTK properties are case sensitive. [Commit.](http://commits.kde.org/breeze-gtk/73dd1f2a6b06a7b43aef2bce9c6d89b5caeab573) 
+ Gtk3, gtk4: don't treat buttons with icons as toolbuttons. [Commit.](http://commits.kde.org/breeze-gtk/cc0df194d8a6e6e9310997e0fd8e1bfe3ab2ff87) 
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Flatpak: Properly fetch the version we are upgrading to. [Commit.](http://commits.kde.org/discover/01cd6a0f9a1bb54959488b43d0a8047946308208) Fixes bug [#447033](https://bugs.kde.org/447033)
+ Flatpak: Properly filter installed apps. [Commit.](http://commits.kde.org/discover/7043d9d44cc42f69d53a3198efb5f9b12589fbde) Fixes bug [#446501](https://bugs.kde.org/446501)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Fix memory leak in some plasma components. [Commit.](http://commits.kde.org/kwin/8accc777a5b3ab1e53341f1838ed636652dc1221) Fixes bug [#444429](https://bugs.kde.org/444429). Fixes bug [#444381](https://bugs.kde.org/444381). Fixes bug [#444077](https://bugs.kde.org/444077). Fixes bug [#444306](https://bugs.kde.org/444306)
+ XDGShellClient: Protect from invalid frameGeometry on updateDecoration. [Commit.](http://commits.kde.org/kwin/77b678c522cce0e8e7b39087f74e2ac591cd28d0) Fixes bug [#445140](https://bugs.kde.org/445140)
+ Input: set tablet cursor hotspot. [Commit.](http://commits.kde.org/kwin/c58e50ec379b0e3508335ca49197e50d2cae7f73) 
+ Platforms/drm: remove the dpms filter when outputs are added. [Commit.](http://commits.kde.org/kwin/8a0036fdee0eaa7dc5808dfe2dfc516fc32dfd8e) Fixes bug [#446699](https://bugs.kde.org/446699)
+ Scripting: Fix type of KWinComponents.Workspace. [Commit.](http://commits.kde.org/kwin/fd8d01d9892f12c48a82c6e2ab675396bf6ec113) 
+ Autotests: Add a test that checks one possible corner case during xdg-toplevel initialization. [Commit.](http://commits.kde.org/kwin/9f8676801c7f82e04e0ba3a20f226db73455e072) 
+ Wayland: Resize the client to last requested client size if decoration is destroyed. [Commit.](http://commits.kde.org/kwin/6a84b9454c869a16c2fa5fd49a8fac4a9497f30f) Fixes bug [#444962](https://bugs.kde.org/444962)
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Handle process parent changes in ProcessDataModel. [Commit.](http://commits.kde.org/libksysguard/311faef0ef0e5f60eebed2a5a00c43f5cb60aab1) Fixes bug [#446534](https://bugs.kde.org/446534)
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ I forgot to git commit --amend before pushing, whoops. [Commit.](http://commits.kde.org/plasma-desktop/301b8112f9e8d326a2c0b442193ecf5fa1412126) 
+ [kcms/keyboard] Fix setting options on X11. [Commit.](http://commits.kde.org/plasma-desktop/90b27ab4170a2b9d6c2c2209874089ad9b52e2aa) Fixes bug [#443609](https://bugs.kde.org/443609)
+ [kcms/keyboard] Fix fallback handling in X11Helper::getGroupNames. [Commit.](http://commits.kde.org/plasma-desktop/701d94ff1399eb18dabc77693f76893c95b7c768) See bug [#433265](https://bugs.kde.org/433265)
+ SwitcherBackend: Only create the workaround window on wayland. [Commit.](http://commits.kde.org/plasma-desktop/c8bcd716800b1e842301fbef8a0adf16d9042eb1) Fixes bug [#443968](https://bugs.kde.org/443968)
+ Touchpad KCM: Don't write default values to active config. [Commit.](http://commits.kde.org/plasma-desktop/294fd6306d94238d16288e78bdf544f0ea82b790) Fixes bug [#427771](https://bugs.kde.org/427771)
+ Change ListView code to match GridView. [Commit.](http://commits.kde.org/plasma-desktop/25c7c9dbd7a80622d0bcb2706676f015d0da3f78) 
+ Fix GridView size/position. [Commit.](http://commits.kde.org/plasma-desktop/1bd320b79cbc91d9aa8540f054dcc04e5e9ecce1) 
+ Move plasmoid specific properties out of singleton. [Commit.](http://commits.kde.org/plasma-desktop/a609b639a3aa2493d1db3922fb7d733733d97c20) Fixes bug [#443131](https://bugs.kde.org/443131)
+ Kcms/touchpad/applet: Fix mousearea lookup. [Commit.](http://commits.kde.org/plasma-desktop/aadab772fb81ff3d97c8b5342a8a018f51b2ef60) 
+ Containments/panel: Fix initial sizing. [Commit.](http://commits.kde.org/plasma-desktop/9f19af95e9699420b057b6ff7e99968faf8948a3) 
+ Kcms/touchpad/applet: Make the popup close on click. [Commit.](http://commits.kde.org/plasma-desktop/2131daa05195a2b55f593acf3d323cf4c2a89936) Fixes bug [#445982](https://bugs.kde.org/445982)
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ Fixes toggling Configure button on click. [Commit.](http://commits.kde.org/plasma-pa/473292a336a6f02aa1990480058983fe25c4eba1) 
{{< /details >}}

{{< details title="Plasma Phone Components" href="https://commits.kde.org/plasma-phone-components" >}}
+ Panel: Fix SIM Locked being shown when no sim is inserted. [Commit.](http://commits.kde.org/plasma-phone-components/d0662f815e3b413359acd960e10e7c29fb645bbc) 
{{< /details >}}

{{< details title="Plasma Systemmonitor" href="https://commits.kde.org/plasma-systemmonitor" >}}
+ Add comma separated filtering to ApplicationsTableView. [Commit.](http://commits.kde.org/plasma-systemmonitor/b18f98fee76104d5be6b871dea330be32863ad27) 
+ ApplicationsTable: Remove unneeded ProcessSortFilterModel. [Commit.](http://commits.kde.org/plasma-systemmonitor/50f6378144ed244b9c1cd3e325ecbe45dd7544d4) Fixes bug [#445544](https://bugs.kde.org/445544)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Applets/systemtray: Follow panel opacity. [Commit.](http://commits.kde.org/plasma-workspace/10aa6382887be57526a259f2ba21251eb5a83c16) Fixes bug [#439025](https://bugs.kde.org/439025)
+ [webshortcutsrunner] Fix private browsing with some Firefoxes. [Commit.](http://commits.kde.org/plasma-workspace/21c58773a5c75ff72a4e0c17e7301b4097ba81ab) 
+ [kcms/style] Don't call setNeedsSave when style config changes. [Commit.](http://commits.kde.org/plasma-workspace/46fde8e84cbeb0a4cf7c1ee65f981e8ef8a4e046) Fixes bug [#439297](https://bugs.kde.org/439297)
+ Digital-clock: fix calendar popup contrast when opened from desktop. [Commit.](http://commits.kde.org/plasma-workspace/aaf9d475fd39fddb7671b33dca6b1a8ed74e2c06) Fixes bug [#446991](https://bugs.kde.org/446991)
+ [libnotificationmanager] Fix memory leak. [Commit.](http://commits.kde.org/plasma-workspace/1960f10576462d9ab80986eb5092546c067edf7d) 
+ [kcms/lookandfeel] Guard reading invalid first entry. [Commit.](http://commits.kde.org/plasma-workspace/042761bd0cf24e90e36a5302051e9fc784286daa) Fixes bug [#446100](https://bugs.kde.org/446100)
+ [Global Menu] Set translation domain. [Commit.](http://commits.kde.org/plasma-workspace/b21349189f99db30cdfca0c3aff5626b6e43f4fd) 
+ Lookandfeel: Add missing check if list of files is empty. [Commit.](http://commits.kde.org/plasma-workspace/a8b613505e326dd047707e0a0e4d6f5ecc4a9baa) Fixes bug [#439797](https://bugs.kde.org/439797)
+ [KSplash] Start with zero opacity. [Commit.](http://commits.kde.org/plasma-workspace/c4ae3c2de36bc8a44743fb8d2eea3e54169f94aa) 
+ Fix Klipper Actions content truncation. [Commit.](http://commits.kde.org/plasma-workspace/beb34e3d9781c7728260d486a835c52be21a6d83) Fixes bug [#444365](https://bugs.kde.org/444365)
+ [klipper] Use full text for DBus return values. [Commit.](http://commits.kde.org/plasma-workspace/c8b8c8dd389ae381831fdd23f354617f48f3edd4) Fixes bug [#446441](https://bugs.kde.org/446441)
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Powerprofileconfig: Use m_profileCombo as the watcher's parent. [Commit.](http://commits.kde.org/powerdevil/575ae1a1ef14e8a0c5b8382a283a8776dbbc3d08) See bug [#443858](https://bugs.kde.org/443858)
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ Close screencast session when we stop streaming. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/18bd44d0541291ffb2e7d40603319395c51edc6c) 
{{< /details >}}

