---
aliases:
- /announcements/plasma-5.21.1-5.21.2-changelog
title: Plasma 5.21.2 complete changelog
version: 5.21.2
hidden: true
plasma: true
type: fulllog
---
{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ Revert "[kstyle]: Add double-ringed focus for text fields". [Commit.](http://commits.kde.org/breeze/bf7c35808a3a29aff9359b44ab54d48426f95d25) Fixes bug [#430944](https://bugs.kde.org/430944). Fixes bug [#430943](https://bugs.kde.org/430943). Fixes bug [#433421](https://bugs.kde.org/433421)
+ Start the window drag from the QStyle. [Commit.](http://commits.kde.org/breeze/da9593d379c25d08e8cd356f472fa561f9fda5f8) Fixes bug [#433178](https://bugs.kde.org/433178)
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Flatpak: Support querying by mimetype. [Commit.](http://commits.kde.org/discover/9c694b41283e026e4c938e768aa39f93e669f5a3) Fixes bug [#433291](https://bugs.kde.org/433291)
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/discover/039887c72b69564a6cec6e02acc659c5e8572c3c) 
{{< /details >}}

{{< details title="KDE GTK Config" href="https://commits.kde.org/kde-gtk-config" >}}
+ Use unique temporary directory for CSD assets, and clean it up. [Commit.](http://commits.kde.org/kde-gtk-config/83289058a4332f25c664f2f6b495c72c349e15c0) Fixes bug [#433608](https://bugs.kde.org/433608)
+ Support svgz buttons in Aurorae themes. [Commit.](http://commits.kde.org/kde-gtk-config/cbfb855ac2f50f91fb39085a337e3151780c861a) Fixes bug [#432712](https://bugs.kde.org/432712)
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/kdeplasma-addons/79c5c3da83d4865007a74d83b34717497e2be1cc) 
{{< /details >}}

{{< details title="Info Center" href="https://commits.kde.org/kinfocenter" >}}
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/kinfocenter/a1c344b61cdb22e4901ef2d839a1d3a41e21f347) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/kwin/7d8f717b7f00b18adf37e8384c9fcd8f13a9ea16) 
+ DrmGpu: add gbm device nullptr check. [Commit.](http://commits.kde.org/kwin/c2b2127f3ae0da05b9bc7f4968f7c6292c54de51) Fixes bug [#433145](https://bugs.kde.org/433145)
+ Wayland: Fix PrepareForSleep dbus connection. [Commit.](http://commits.kde.org/kwin/a4eede95b2893955ed248b41d2dda7fa89136879) 
+ Ftrace: use quint32 for context counter. [Commit.](http://commits.kde.org/kwin/8f5c77ee7b0f7e1faae2705c33294b9203d470e6) 
+ 3rdparty/xcursor: build with _DEFAULT_SOURCE. [Commit.](http://commits.kde.org/kwin/759a035b468e86d79887ed820559dbee412ec1c2) 
+ Helper: wl-socket: fix build for musl. [Commit.](http://commits.kde.org/kwin/83a859c8ec73104810ab76b265adb450c55a3575) 
+ Wayland: Honor NoPlugin option. [Commit.](http://commits.kde.org/kwin/3dc79a6f93f689c2dd8950831fcb15358f5b7f33) 
+ Wayland: Track already existing subsurfaces in SubSurfaceMonitor. [Commit.](http://commits.kde.org/kwin/85d6715b3b6d29ca88647ba3bea85e6ee3c630b9) Fixes bug [#433511](https://bugs.kde.org/433511)
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Set line chart amount of history spinbox max to actual maximum value. [Commit.](http://commits.kde.org/libksysguard/240191d453fe4798b8dee8837c7886adf5cf4eb8) 
+ Only emit readyChanged when we are in fact ready. [Commit.](http://commits.kde.org/libksysguard/c5057b10b92a17f395601d462d058642bda8bbff) 
+ Properly fix column count and inserting new columns on metadata load. [Commit.](http://commits.kde.org/libksysguard/b3f7e503276700cd58b685bc1b46f67aad1407df) Fixes bug [#433064](https://bugs.kde.org/433064)
+ Horizontal bars: Show full name if possible. [Commit.](http://commits.kde.org/libksysguard/496802c21d1bddfdaf6ac81ad4298c7b67311f60) Fixes bug [#433169](https://bugs.kde.org/433169)
+ Set min/max for Y axis range spinboxes to minimum/maximum possible value. [Commit.](http://commits.kde.org/libksysguard/e01caf4fb1bb4245d1f1fdb20cf58a9695037628) Fixes bug [#433007](https://bugs.kde.org/433007). Fixes bug [#424527](https://bugs.kde.org/424527)
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Avoid using non-integer numbers as spacing. [Commit.](http://commits.kde.org/plasma-desktop/1d53973d633dcd9073efe6b38d0cafb6344d89d0) 
+ [Kickoff] Don't capitalize full word sections. [Commit.](http://commits.kde.org/plasma-desktop/97944e4f84200ed6a1ca8a60e5a128cc45677030) Fixes bug [#433574](https://bugs.kde.org/433574)
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/plasma-desktop/6c996c1f0f428bbd19d02f87ea289df2c6ff07f4) 
+ Kcms/keyboard: handle botched 5.21.0 migration. [Commit.](http://commits.kde.org/plasma-desktop/c03bba6eb40bf12f497f05417ef7ab3d40f7edae) Fixes bug [#431923](https://bugs.kde.org/431923)
+ [kcms/activities] Fixup DBus path usage. [Commit.](http://commits.kde.org/plasma-desktop/6273f0ba5926d775a8db057e7ea1e0d2c8f17f06) Fixes bug [#433203](https://bugs.kde.org/433203)
{{< /details >}}

{{< details title="Plasma Firewall" href="https://commits.kde.org/plasma-firewall" >}}
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/plasma-firewall/280e6fd7a76c170c2ca56635e89825fd39deadca) 
{{< /details >}}

{{< details title="Plasma Phone Components" href="https://commits.kde.org/plasma-phone-components" >}}
+ Fix the SignalStrength indicator on lockscreen. [Commit.](http://commits.kde.org/plasma-phone-components/382648bf59ee2af05463bdea895b408fadd1f5f2) 
+ Use Header colors for top panel. [Commit.](http://commits.kde.org/plasma-phone-components/598eb9df801c46df4bd7a32716180510f7151831) 
+ Shell: keep panelsfix.js in sync with layout.js. [Commit.](http://commits.kde.org/plasma-phone-components/43ea8ac1c1dbe79f7499bf00b8f524f216b3a6e7) 
+ Fix horizontal scrolling of applets. [Commit.](http://commits.kde.org/plasma-phone-components/72f9f7c1e830388173bb70b4fef3982e3614ac2e) 
{{< /details >}}

{{< details title="Plasma SDK" href="https://commits.kde.org/plasma-sdk" >}}
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/plasma-sdk/26f32ca4ebb7023c3c7d61751a1e9be39a98e729) 
{{< /details >}}

{{< details title="Plasma Systemmonitor" href="https://commits.kde.org/plasma-systemmonitor" >}}
+ Initialize m_pageData. [Commit.](http://commits.kde.org/plasma-systemmonitor/22202e061fe3299af7c2ea39c2811b261fa67bde) Fixes bug [#433532](https://bugs.kde.org/433532)
+ Add downloaded pages instantly again. [Commit.](http://commits.kde.org/plasma-systemmonitor/8fceebcd12b6c623c3cc0cc065c5a1f986056d5f) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ KRunner: Handle escape key in history view more gracefully. [Commit.](http://commits.kde.org/plasma-workspace/c4db9bd1e960e3ec7b6c460441ee622c4a582fc9) Fixes bug [#433723](https://bugs.kde.org/433723)
+ [Notifications] Don't change tooltip when paused. [Commit.](http://commits.kde.org/plasma-workspace/e328b546ce8df063bf40063b0495dfa3bb627c21) 
+ Locations runner: Fix empty list on invalid shell quotes. [Commit.](http://commits.kde.org/plasma-workspace/6c759a80506a3cfe0aba2b040d99d571894fecba) 
+ [applets/devicenotifier] Add placeholder icon when no device icon exists. [Commit.](http://commits.kde.org/plasma-workspace/e8a7d8dac4655e2b5e75a250fb9e034246e0cb23) Fixes bug [#433534](https://bugs.kde.org/433534)
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/plasma-workspace/ea26b92c882b0a88b0f9d1a590b5f4ac4f9b36ed) 
+ [kcms/icons] Collapse buttons if the row is too wide for the display. [Commit.](http://commits.kde.org/plasma-workspace/d9e2924833637de39c4e04895dd8030711f258df) 
+ Do not start faulty coronas. [Commit.](http://commits.kde.org/plasma-workspace/e02496a02926429b5d6b6b1fa2902f4c585ca640) 
{{< /details >}}

{{< details title="qqc2-breeze-style" href="https://commits.kde.org/qqc2-breeze-style" >}}
+ Remove ApplicationWindow. [Commit.](http://commits.kde.org/qqc2-breeze-style/610bb1a85dd3f8b238d1724ed78184636f1a4270) 
+ [RangeSlider] Remove inset, endVisualPosition and palette. [Commit.](http://commits.kde.org/qqc2-breeze-style/ca8d5262c9ce17b7b9ce2c8970a6a4d43294c498) 
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/b5c91b11b4df81fc61493a03738762c266f44d0f) 
{{< /details >}}

