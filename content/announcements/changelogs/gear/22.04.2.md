---
aliases:
- ../../fulllog_releases-22.04.2
title: KDE Gear 22.04.2 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akonadi" title="akonadi" link="https://commits.kde.org/akonadi" >}}
+ Close memory leak. [Commit.](http://commits.kde.org/akonadi/7e140c4efcad541f3a3f529b2d79cafa24ad214a) Fixes bug [#423575](https://bugs.kde.org/423575)
{{< /details >}}
{{< details id="akonadi-calendar" title="akonadi-calendar" link="https://commits.kde.org/akonadi-calendar" >}}
+ Add missing connect for the "Dismiss" action. [Commit.](http://commits.kde.org/akonadi-calendar/e1b7964a91c7fe47a1d909e4fd079cb6c37d21b3) Fixes bug [#453567](https://bugs.kde.org/453567)
+ Handle empty incidence. [Commit.](http://commits.kde.org/akonadi-calendar/c9999701f5d07dfc8797e730ee581f730c203f67) Fixes bug [#453395](https://bugs.kde.org/453395)
+ Set the Bugzilla product/component in the reminder daemon about data. [Commit.](http://commits.kde.org/akonadi-calendar/064a95fefb9d491c91036b7642fdc9955a0bad48) 
{{< /details >}}
{{< details id="akonadi-import-wizard" title="akonadi-import-wizard" link="https://commits.kde.org/akonadi-import-wizard" >}}
+ Fix import ldap settings. [Commit.](http://commits.kde.org/akonadi-import-wizard/47abe68a9884d227db7e813554b3712b8954bc75) 
{{< /details >}}
{{< details id="ark" title="ark" link="https://commits.kde.org/ark" >}}
+ Update runtime version for flatpak manifest. [Commit.](http://commits.kde.org/ark/41c885f45f51d2c1a5ccd78fd503eeaa7f89d8e9) 
+ Cleanup commit b5ee900e2c. [Commit.](http://commits.kde.org/ark/907ab1cb9b5596b2fa7a727a9a7cd29fd11f175f) 
+ Cover one more p7zip vs 7-Zip difference. [Commit.](http://commits.kde.org/ark/d020d3904000ecde3471461ce6388f0bdd3c58d5) See bug [#440135](https://bugs.kde.org/440135)
+ Cli7z: conditionally add the -l switch. [Commit.](http://commits.kde.org/ark/b5ee900e2caa47e93d8e490500b6859809f4a6bd) See bug [#440135](https://bugs.kde.org/440135)
+ Remove -l flag to 7z a. [Commit.](http://commits.kde.org/ark/35b397c84fdc7c46a98d02aeca13d1cee1facf6e) See bug [#440135](https://bugs.kde.org/440135)
+ Support '7zip' as well as 'p7zip'. [Commit.](http://commits.kde.org/ark/5943a22ac4696158b40cee3a3873e88b81d5539b) See bug [#440135](https://bugs.kde.org/440135)
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Kfileitemmodel: sortRoleCompare: allow to sort by access time. [Commit.](http://commits.kde.org/dolphin/81792ef4cf414b3ed97724e910b1a7dae46bf469) 
+ Remove hover highlight before opening view context menu. [Commit.](http://commits.kde.org/dolphin/fe6e2f367d8af476d2a110921d4abd60d468e8f5) 
+ Fix paste on row while in details view mode. [Commit.](http://commits.kde.org/dolphin/2fd7e7e866d0c00e6867cbbad05c8f8027580bf2) 
+ Fix QDirIterator wildcard when fallback install script is searched for. [Commit.](http://commits.kde.org/dolphin/f932b6eb9fc8567f4615d6b32e8cea1a9839c3f1) Fixes bug [#453870](https://bugs.kde.org/453870)
{{< /details >}}
{{< details id="eventviews" title="eventviews" link="https://commits.kde.org/eventviews" >}}
+ TodoViewQuickAddLine: fix adding a todo using the quick add line. [Commit.](http://commits.kde.org/eventviews/3965ec58eae7fd8f8e79ae809e092b74b7610821) 
{{< /details >}}
{{< details id="filelight" title="filelight" link="https://commits.kde.org/filelight" >}}
+ Make build_testing actually do what it says. [Commit.](http://commits.kde.org/filelight/35cd49b637af447b427d133ccecbeeee57da1e62) 
+ Prevent building in source. [Commit.](http://commits.kde.org/filelight/75d024497dc15673a792547285d31eea10df5cbd) 
{{< /details >}}
{{< details id="incidenceeditor" title="incidenceeditor" link="https://commits.kde.org/incidenceeditor" >}}
+ Unchecking the "Due" checkbox of a recurring to-do had no effect. [Commit.](http://commits.kde.org/incidenceeditor/777cf46be59f9b922d8bf91eba5b1bf8d7ea95a1) 
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Add 22.04.2 release notes. [Commit.](http://commits.kde.org/itinerary/ab4de16e1d4fb9fc1460624cd04ee34a1b442ed6) 
{{< /details >}}
{{< details id="kaddressbook" title="kaddressbook" link="https://commits.kde.org/kaddressbook" >}}
+ Use openStateConfig for storing dialog size. [Commit.](http://commits.kde.org/kaddressbook/ec2d34aa6cfb3c705238d083f234641f5bc50726) 
{{< /details >}}
{{< details id="kalendar" title="kalendar" link="https://commits.kde.org/kalendar" >}}
+ Don't call window->show() on a visible window. [Commit.](http://commits.kde.org/kalendar/8aa6e24e4ca3515e05d6c6c6bb7b7df660c4391f) 
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Fix crash in compiler-explorer plugin. [Commit.](http://commits.kde.org/kate/f8a54bf5d1adc2cddf378aa11893a1f97a4f7ca6) 
+ Revert "Create filebrowser on demand". [Commit.](http://commits.kde.org/kate/36ede44fc7bef479e94f76121309a900bee2b29f) Fixes bug [#453795](https://bugs.kde.org/453795)
+ Revert "fix crash on file browser activation". [Commit.](http://commits.kde.org/kate/e002696b0673dff7ec9584e00b1546d2f850121c) Fixes bug [#453795](https://bugs.kde.org/453795)
+ Katetabbar: Fix drag pixmap size on high-dpi. [Commit.](http://commits.kde.org/kate/9ccb169070a81fb7d716bbc61ff390b07076ab42) 
{{< /details >}}
{{< details id="kdenetwork-filesharing" title="kdenetwork-filesharing" link="https://commits.kde.org/kdenetwork-filesharing" >}}
+ Reload combobox on reuse. [Commit.](http://commits.kde.org/kdenetwork-filesharing/f1323083f30a0cc72e690d1e6d7f2cc723fec0b4) 
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix icon color change in some situations (eg. Appimage). [Commit.](http://commits.kde.org/kdenlive/460910bcea5bdd9d1ecd2ca2dc3fc45e960afe0b) Fixes bug [#450556](https://bugs.kde.org/450556)
+ Fix incorrect lambda capture leading to crash. [Commit.](http://commits.kde.org/kdenlive/056d146294df253c8124594df399b015d8f75897) 
+ Fix AppImage icons. [Commit.](http://commits.kde.org/kdenlive/9a5315d3e52c9caede81f9ccaae2b5c1cd054b1b) See bug [#451406](https://bugs.kde.org/451406)
+ Online resources: only show warning about loading time once. [Commit.](http://commits.kde.org/kdenlive/6c7e607f955991f64feae81efd4a40f2b6d37905) See bug [#454470](https://bugs.kde.org/454470)
+ Clang format fixes. [Commit.](http://commits.kde.org/kdenlive/f4cdefe0e47601fcfae483884009708a21207ff5) 
+ Fix crash clicking ok in empty transcoding dialog. [Commit.](http://commits.kde.org/kdenlive/038b7c03acc621fdad7d02ef6d80f59990c31354) 
+ Fix possible crash when load task is running on exit. [Commit.](http://commits.kde.org/kdenlive/1920dd4c15f4ec35e0d16c0e93c40468124efa8c) 
+ Fix file watcher broken, changed clips were not detected anymore. [Commit.](http://commits.kde.org/kdenlive/16b177c6a900fea6eb2be13439005a25552f49f6) 
+ Fix timeremap clip always using proxies on rendering. [Commit.](http://commits.kde.org/kdenlive/bfab72ad7079f52516e285f985039d3044b980de) Fixes bug [#454089](https://bugs.kde.org/454089)
+ Ensure internal effects like subtitles stay on top so that they are not affected by color or transform effects. [Commit.](http://commits.kde.org/kdenlive/49543041f85981d731b23c07fecc1f3f2e65368f) 
+ Fix crash on undo center keyframe. [Commit.](http://commits.kde.org/kdenlive/d9a9c00dca9b7065b551ddcc6c6c25ea94bbdc43) 
+ Fix crash changing clip monitor bg color when no clip is selected. [Commit.](http://commits.kde.org/kdenlive/ce28f4d2520edbeba455d7b23fdbeea6c42726dd) 
+ Fix crash on undo selected clip insert. [Commit.](http://commits.kde.org/kdenlive/660858733e4eeda7fc5902ca03b73cd0ae90b073) 
+ Fix nvenc codec. [Commit.](http://commits.kde.org/kdenlive/456bb4fb76c6f1066ab637687f97658ea47abee3) See bug [#454469](https://bugs.kde.org/454469)
+ Fix clip thumbs not discarded on property change. [Commit.](http://commits.kde.org/kdenlive/338e677b54629df877d4f0091c8d08ddad8bc3fe) 
+ On document loading, also check images for changes. [Commit.](http://commits.kde.org/kdenlive/937e2061349c94c3f3411013772b315d2404e3f4) 
+ Fix tests and mix direction regression. [Commit.](http://commits.kde.org/kdenlive/e9a62a1c0a273e07388b90743719b3c2e740a4ea) 
+ Fix major corruption on undo/redo clip cut, with tests. [Commit.](http://commits.kde.org/kdenlive/919596ad105dd4c5d6e4f3419963c39d61732ea8) 
+ Project loading: detect and fix corruption if audio or video clips on the same track use a different producer. [Commit.](http://commits.kde.org/kdenlive/5e9d56a96feeeac8fbf77871ea4c1a0b8eb75c27) 
+ Fix crash dropping an effect on the clip monitor. [Commit.](http://commits.kde.org/kdenlive/a7fc91dbabc05e9778698ab6aafd747a9507db49) 
+ Speedup maker search. [Commit.](http://commits.kde.org/kdenlive/006c7a1761669b3fbd0000eda130cf7aae366111) 
+ Fix cannot put monitor in fullscreen with mirrored screens. [Commit.](http://commits.kde.org/kdenlive/13c5f4434cf01dacec300f1a0e0490cd783c6efa) 
+ Fix mix on very short AV clips broken, with test. [Commit.](http://commits.kde.org/kdenlive/a6c365a3d01441bf6bf77782df9db2a323498927) 
+ Fix Slide mix not correctly updated when creating a new mix on the previous clip, add tests. [Commit.](http://commits.kde.org/kdenlive/019491631e59fc735fe1b3c7be33edea9314bed6) See bug [#453770](https://bugs.kde.org/453770)
+ Fix mix mix not correctly reversed in some cases and on undo. [Commit.](http://commits.kde.org/kdenlive/8142abe5c2754ed09945af51ffa16284c2df0542) 
+ Fix slide composition going in wrong direction (mix is still todo). [Commit.](http://commits.kde.org/kdenlive/d332064716d3ee9eaedaaf9402c9ffcd15a42de5) See bug [#453770](https://bugs.kde.org/453770)
+ Fix several small glitches in bin selection. [Commit.](http://commits.kde.org/kdenlive/cf5e56aa4620395b0895a4885482054b489c86c1) 
+ Fix clip height not aligned to its track. [Commit.](http://commits.kde.org/kdenlive/cf5c0a473a59f4eda17bdf367377197b0868bcba) 
+ Fix speech to text on Mac. [Commit.](http://commits.kde.org/kdenlive/8ffbef7fb62a5b70b8ee31e6dcc457ae3e79886c) 
+ Fix crash/corruption in overwrite mode when moving grouped clips above or below existing tracks. [Commit.](http://commits.kde.org/kdenlive/b128c3b384addfba1228ab931dc08c0ee16f8d97) 
+ Fix missing audio with "WebM-VP9/Opus (libre)" preset. [Commit.](http://commits.kde.org/kdenlive/57d849d69077fb0abd6faed2bfdc3d29c2d58428) See bug [#452950](https://bugs.kde.org/452950)
+ [Render Widget] Allow more steps for quality slider. [Commit.](http://commits.kde.org/kdenlive/5060037f97a4f487f2b9ae161d4fafa24f6ec455) 
+ [Render Presets] Fix wrongly reversed quality with custom presets. [Commit.](http://commits.kde.org/kdenlive/01ca6009148dd49f52823852cda17bbd65e70ca6) 
+ [Render Presets] Add more speed preset steps for x254 and x256. [Commit.](http://commits.kde.org/kdenlive/854c1aef1a96ece1831048cef7ec837e0c410f24) 
+ Fix mixers don't display levels if a track was added/removed with collapsed mixer. [Commit.](http://commits.kde.org/kdenlive/ee7a06a3ada559a82ad1505254b8ee817ca8dc04) 
+ Fix possible crash in transcoding dialog if there are no clips to convert. [Commit.](http://commits.kde.org/kdenlive/b0b13930d16402ba28ba90b987f4ef15c565094b) 
+ [RenderWidget] Add scrollbar to improve experience on small screens. [Commit.](http://commits.kde.org/kdenlive/8131671594ead2d318418359c0a1b7e175979fbd) 
{{< /details >}}
{{< details id="kdevelop" title="kdevelop" link="https://commits.kde.org/kdevelop" >}}
+ On Apple, use project version format compatible with Apple linker. [Commit.](http://commits.kde.org/kdevelop/7d02895fd0ef22fb93c4f2a5fc3fe5dfc7aba4cb) Fixes bug [#448152](https://bugs.kde.org/448152)
{{< /details >}}
{{< details id="kimagemapeditor" title="kimagemapeditor" link="https://commits.kde.org/kimagemapeditor" >}}
+ Remove %i parameter from Exec line. [Commit.](http://commits.kde.org/kimagemapeditor/a411c70d468f295905f9190d466a6a121a9d4838) 
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ Man: remove the section suffix from page names in listDir(). [Commit.](http://commits.kde.org/kio-extras/d676e8085e86b7f78a86877c038bd98bcc02bde4) Fixes bug [#452920](https://bugs.kde.org/452920)
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Fix hex vs decimal mix-up in barcode content detection. [Commit.](http://commits.kde.org/kitinerary/0a201e6877cab003cd0bc045d8c86c61787c4a89) 
+ Slightly relax the square barcode aspect ratio threshold. [Commit.](http://commits.kde.org/kitinerary/da2622ed4ee1827c514e9c524696605cbe584477) 
+ Try harder to clean Eventbrite street addresses. [Commit.](http://commits.kde.org/kitinerary/68748049851bd404fea3f2c997c3f6227efc0951) 
+ Add extractor script for UK national rail PDF tickets. [Commit.](http://commits.kde.org/kitinerary/2e45f1b28661422845412055d84a3f872f2e869a) 
+ Add extractor script for Thalys PDF tickets. [Commit.](http://commits.kde.org/kitinerary/276c9e91daf818ecc100d6e2c68e08e1958d6d95) 
{{< /details >}}
{{< details id="kmahjongg" title="kmahjongg" link="https://commits.kde.org/kmahjongg" >}}
+ Fix issue when restaring a game with a selected tile. [Commit.](http://commits.kde.org/kmahjongg/c49e284e180195aacf7797842d3c930b3a2241bd) Fixes bug [#454903](https://bugs.kde.org/454903)
{{< /details >}}
{{< details id="kmail" title="kmail" link="https://commits.kde.org/kmail" >}}
+ Fix update menu when we switch tab. [Commit.](http://commits.kde.org/kmail/0ef1c9191eac2812e689b696dbe8c28192a74d1f) 
+ Follow BinaryUnitDialect option. [Commit.](http://commits.kde.org/kmail/3150b97b3e33971b01f8c6d71c2c5d11430fe729) See bug [#454182](https://bugs.kde.org/454182)
+ Fix crash when we add/edit/remove not. [Commit.](http://commits.kde.org/kmail/1206c629da48c2eaec8205cca47e4c42ddefe7c3) 
{{< /details >}}
{{< details id="kolourpaint" title="kolourpaint" link="https://commits.kde.org/kolourpaint" >}}
+ Add missing StartupWMClass. [Commit.](http://commits.kde.org/kolourpaint/c80060f4ae7221e589d196255ca5a0cd49d63449) Fixes bug [#453325](https://bugs.kde.org/453325)
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Fix scroll position jumps regression. [Commit.](http://commits.kde.org/konsole/a6fce70f6c4e4d355c14475be8f5669a83c7023f) Fixes bug [#452955](https://bugs.kde.org/452955). Fixes bug [#453112](https://bugs.kde.org/453112)
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" link="https://commits.kde.org/kpublictransport" >}}
+ Discard Hafas load values outside of the expected range. [Commit.](http://commits.kde.org/kpublictransport/f418c258c4f228e8804205b1aa490fe6da9b0650) 
{{< /details >}}
{{< details id="ksirk" title="ksirk" link="https://commits.kde.org/ksirk" >}}
+ Fix KNewStuff includes. [Commit.](http://commits.kde.org/ksirk/6e8ae29aa621df7e02cf5cf897352e20d28c4986) 
{{< /details >}}
{{< details id="ktp-text-ui" title="ktp-text-ui" link="https://commits.kde.org/ktp-text-ui" >}}
+ Fix kcm_ktp_chat_messages appearing in global search. [Commit.](http://commits.kde.org/ktp-text-ui/075980c5db8430cfcd06280246faa6caeb00b0a2) 
{{< /details >}}
{{< details id="libkcddb" title="libkcddb" link="https://commits.kde.org/libkcddb" >}}
+ Needs kdoctools_install since it has docs. [Commit.](http://commits.kde.org/libkcddb/f9e58d3b294ba6c07d9e0ae2c600ac253d9e0cf4) 
{{< /details >}}
{{< details id="libkexiv2" title="libkexiv2" link="https://commits.kde.org/libkexiv2" >}}
+ Remove usage of register keyword. [Commit.](http://commits.kde.org/libkexiv2/54a2c9c974291a42959d5721a3dcb3b5ade80522) Fixes bug [#454050](https://bugs.kde.org/454050)
{{< /details >}}
{{< details id="lokalize" title="lokalize" link="https://commits.kde.org/lokalize" >}}
+ Update mailing lists for Norwegian translation teams. [Commit.](http://commits.kde.org/lokalize/0bdd8bd236459338bfc52b7ae57e78eed29e2268) 
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ Fix bug 454182: message (part) size display: please follow BinaryUnitDialect everywhere. [Commit.](http://commits.kde.org/messagelib/7b956ab759445ef9b556ee67b60ba3a6292bed9e) Fixes bug [#454182](https://bugs.kde.org/454182)
+ We only need a close button here. [Commit.](http://commits.kde.org/messagelib/ba074c52e53351a7c210c43423990700623fc4b7) 
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Fix crash while undoing with the menu on an empty annotation. [Commit.](http://commits.kde.org/okular/1a980008be43e006c21254c45a06b7dd17a215bd) Fixes bug [#453987](https://bugs.kde.org/453987)
{{< /details >}}
{{< details id="partitionmanager" title="partitionmanager" link="https://commits.kde.org/partitionmanager" >}}
+ Mark KDE Partition Manager as a single main window program. [Commit.](http://commits.kde.org/partitionmanager/abea3179aff78d7692c874ff4618d5ad7ff8336c) 
+ Fix info pane layout alignment to top. [Commit.](http://commits.kde.org/partitionmanager/6c195278ce608b5df4462fe986ea686c6a63e243) 
+ Fix empty frame in resize/move dialog. [Commit.](http://commits.kde.org/partitionmanager/e351dfd21d8a8b8a45bc086059e27080e0a9338f) 
{{< /details >}}
{{< details id="pim-data-exporter" title="pim-data-exporter" link="https://commits.kde.org/pim-data-exporter" >}}
+ Fix copy file. [Commit.](http://commits.kde.org/pim-data-exporter/af84327bb50b1c4ffe08143b5594124cd9ce57df) 
+ Use mkpath here. [Commit.](http://commits.kde.org/pim-data-exporter/5865c8c41235f3ddd544336347ca00eebdddfc06) 
+ Fix autotest. [Commit.](http://commits.kde.org/pim-data-exporter/c8d5bd2fc1b91e01966b94a5dddc8d6f148c2aeb) 
+ Fix reactivate action when we cancel sync. [Commit.](http://commits.kde.org/pim-data-exporter/c344824ddf8216fa8814fd79ebfe3f54b6a2cbb7) 
+ Exclude akonadi_google_resource too for setPath. [Commit.](http://commits.kde.org/pim-data-exporter/80367de70202849f4977d77956658e2360e3c199) 
+ Fix export default maildir folder. [Commit.](http://commits.kde.org/pim-data-exporter/f35ff0f4ff28704ffd28ab980ad45460c9b536f7) 
+ We need to keep order. [Commit.](http://commits.kde.org/pim-data-exporter/f31d77aaf651fc60044ea8b1e82f358be2a2b7c4) 
+ Create on stack. [Commit.](http://commits.kde.org/pim-data-exporter/b73366af51a28e6362ece401fc6742538a436026) 
+ These resources doesn't have setPath method. [Commit.](http://commits.kde.org/pim-data-exporter/8cf1eae78345cd0a450d79e19d6932723b4601a7) 
{{< /details >}}
{{< details id="pimcommon" title="pimcommon" link="https://commits.kde.org/pimcommon" >}}
+ Fix typo. [Commit.](http://commits.kde.org/pimcommon/97cd0b7e44f09e11f9b3b449edca0d591ee3b850) 
{{< /details >}}
{{< details id="spectacle" title="spectacle" link="https://commits.kde.org/spectacle" >}}
+ Update org.kde.spectacle.desktop.cmake to include both required parameters for WindowUnderCursor. [Commit.](http://commits.kde.org/spectacle/8beca3b9fdc1beffc7172155f3ae3a239ec9993d) Fixes bug [#446971](https://bugs.kde.org/446971)
{{< /details >}}
{{< details id="yakuake" title="yakuake" link="https://commits.kde.org/yakuake" >}}
+ Fix KNewStuff includes. [Commit.](http://commits.kde.org/yakuake/3c90b54a2c880f251d51a8f8d10bd0e2c1353116) 
{{< /details >}}
