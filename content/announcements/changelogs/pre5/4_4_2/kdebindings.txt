------------------------------------------------------------------------
r1097209 | arnorehn | 2010-03-01 06:32:58 +1300 (Mon, 01 Mar 2010) | 2 lines

backport build fixes from trunk

------------------------------------------------------------------------
r1100356 | rdale | 2010-03-08 00:33:15 +1300 (Mon, 08 Mar 2010) | 6 lines

* Fixed Qt::Object properties that were no longer working. Fixes bug 229784
  reported by ruby.twiddler

BUG: 229784


------------------------------------------------------------------------
r1100363 | rdale | 2010-03-08 00:49:47 +1300 (Mon, 08 Mar 2010) | 5 lines

* Drag and drop events in Plasma applets weren't being handled correctly.
  Fixes problem reported by Robert Riemann

CCMAIL: kde-bindings@kde.org

------------------------------------------------------------------------
r1100563 | rdale | 2010-03-08 10:05:02 +1300 (Mon, 08 Mar 2010) | 7 lines

* QtRuby overrides public_methods(), singleton_methods() and 
  protected_methods(), but omitted the optional 'all' argument. Thanks to
  Paolo Capriotti for the fix and bug report.

CCMAIL: kde-bindings@kde.org


------------------------------------------------------------------------
r1102304 | sedwards | 2010-03-12 20:30:26 +1300 (Fri, 12 Mar 2010) | 2 lines

Added missing Nepomuk query classes.

------------------------------------------------------------------------
r1102339 | rdale | 2010-03-12 23:05:31 +1300 (Fri, 12 Mar 2010) | 6 lines

* Added the missing Soprano::BackendSetting class. Thanks to Thomas Scholtes
for reporting it missing.

BUG: 230370
CCMAIL: kde-bindings@kde.org

------------------------------------------------------------------------
r1103346 | rdale | 2010-03-15 10:48:15 +1300 (Mon, 15 Mar 2010) | 6 lines

* The QtDBus classes weren't being correctly initialized as the count of 
  classes was being taken from the QtSvg smoke lib. Thanks to Paulo 
  Capriotti for finding the bug.

CCMAIL: kde-bindings@kde.org

------------------------------------------------------------------------
r1104150 | arnorehn | 2010-03-17 10:15:01 +1300 (Wed, 17 Mar 2010) | 2 lines

backport from trunk: add missing classes.

------------------------------------------------------------------------
r1105793 | arnorehn | 2010-03-22 01:14:54 +1300 (Mon, 22 Mar 2010) | 2 lines

backport missing class from trunk

------------------------------------------------------------------------
r1105797 | arnorehn | 2010-03-22 01:18:13 +1300 (Mon, 22 Mar 2010) | 2 lines

backport from trunk: fix parent class for enums in namespaces

------------------------------------------------------------------------
r1105814 | arnorehn | 2010-03-22 02:01:38 +1300 (Mon, 22 Mar 2010) | 2 lines

backport another fix from trunk

------------------------------------------------------------------------
r1107704 | mmrozowski | 2010-03-27 03:56:32 +1300 (Sat, 27 Mar 2010) | 1 line

Do not link smoke/plasma to smokeqtwebkit
------------------------------------------------------------------------
