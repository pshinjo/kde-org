------------------------------------------------------------------------
r836919 | rdale | 2008-07-23 13:09:13 +0200 (Wed, 23 Jul 2008) | 3 lines

* The ktexteditor extension doesn't need to link against the katepart lib


------------------------------------------------------------------------
r837069 | pino | 2008-07-23 19:59:51 +0200 (Wed, 23 Jul 2008) | 2 lines

extra ';' at the end of a namespace definition

------------------------------------------------------------------------
r837099 | sebsauer | 2008-07-23 21:35:14 +0200 (Wed, 23 Jul 2008) | 5 lines

fix -jMany compile-breaks cause of wrong/missing dependencies again.

CC_MAIL:mueller@kde.org


------------------------------------------------------------------------
r837314 | arnorehn | 2008-07-24 14:40:12 +0200 (Thu, 24 Jul 2008) | 3 lines

* Backport some bugfixes from trunk.


------------------------------------------------------------------------
r837427 | rdale | 2008-07-24 18:52:40 +0200 (Thu, 24 Jul 2008) | 3 lines

* Add missing libs to link against


------------------------------------------------------------------------
r837428 | rdale | 2008-07-24 18:54:58 +0200 (Thu, 24 Jul 2008) | 3 lines

* Add missing libs to link against


------------------------------------------------------------------------
r837539 | mueller | 2008-07-25 03:39:37 +0200 (Fri, 25 Jul 2008) | 1 line

revert pedanticness fix as it breaks the sip compiler and we can't afford that in a stable branch
------------------------------------------------------------------------
r837912 | mueller | 2008-07-26 02:39:37 +0200 (Sat, 26 Jul 2008) | 4 lines

fix python/ruby build mixup. Many thanks to Kevin Kofler
for the analysis and the patch
BUG: 167450

------------------------------------------------------------------------
r838440 | arnorehn | 2008-07-27 22:58:44 +0200 (Sun, 27 Jul 2008) | 3 lines

* Fix P/Invoke function names.


------------------------------------------------------------------------
r838457 | arnorehn | 2008-07-28 00:10:49 +0200 (Mon, 28 Jul 2008) | 3 lines

* Fix the delegate invocation.


------------------------------------------------------------------------
r838460 | arnorehn | 2008-07-28 00:17:39 +0200 (Mon, 28 Jul 2008) | 3 lines

* Allocate GCHandles for delegates so they won't be collected.


------------------------------------------------------------------------
r839858 | arnorehn | 2008-07-31 01:33:33 +0200 (Thu, 31 Jul 2008) | 3 lines

* Backport fixes from trunk.


------------------------------------------------------------------------
r841294 | sebsauer | 2008-08-03 03:27:22 +0200 (Sun, 03 Aug 2008) | 3 lines

backport r841293 and r841289
more -jMany compile-fixes

------------------------------------------------------------------------
r846591 | arnorehn | 2008-08-13 20:04:02 +0200 (Wed, 13 Aug 2008) | 3 lines

backports


------------------------------------------------------------------------
r846592 | arnorehn | 2008-08-13 20:05:24 +0200 (Wed, 13 Aug 2008) | 3 lines

more backports


------------------------------------------------------------------------
r846599 | arnorehn | 2008-08-13 20:22:01 +0200 (Wed, 13 Aug 2008) | 2 lines

backport

------------------------------------------------------------------------
