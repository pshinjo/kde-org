2006-07-24 14:46 +0000 [r565820]  toivo

	* branches/KDE/3.5/kdeadmin/kpackage/debInterface.cpp: Fix for
	  Unicode characters in package descriptions Robert Kovacs
	  <robert.kovacs@mail.bme.hu>

2006-08-07 13:15 +0000 [r570625]  jriddell

	* branches/KDE/3.5/kdeadmin/knetworkconf/backends/platform.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/network.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/service.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/network-conf.in:
	  Add support for Kubuntu 6.10, Edgy

2006-08-09 10:30 +0000 [r571342]  mueller

	* trunk/extragear/utils/Makefile.am.in,
	  trunk/extragear/graphics/Makefile.am.in,
	  branches/KDE/3.5/kdeaddons/Makefile.am.in,
	  branches/KDE/3.5/kdebase/Makefile.am.in,
	  branches/KDE/3.5/kdeedu/Makefile.am.in,
	  branches/KDE/3.5/kdesdk/Makefile.am.in,
	  branches/koffice/1.5/koffice/Makefile.am.in,
	  branches/KDE/3.5/kdepim/Makefile.am.in,
	  branches/KDE/3.5/kdeaccessibility/Makefile.am.in,
	  branches/KDE/3.5/kdeadmin/Makefile.am.in,
	  branches/KDE/3.5/kdenetwork/Makefile.am.in,
	  branches/KDE/3.5/kdelibs/Makefile.am.in,
	  branches/KDE/3.5/kdeartwork/Makefile.am.in,
	  branches/arts/1.5/arts/Makefile.am.in,
	  trunk/extragear/pim/Makefile.am.in,
	  branches/KDE/3.5/kdemultimedia/Makefile.am.in,
	  branches/KDE/3.5/kdegames/Makefile.am.in,
	  trunk/playground/sysadmin/Makefile.am.in,
	  trunk/extragear/network/Makefile.am.in,
	  trunk/extragear/libs/Makefile.am.in,
	  branches/KDE/3.5/kdebindings/Makefile.am.in,
	  branches/KDE/3.5/kdetoys/Makefile.am.in,
	  trunk/extragear/multimedia/Makefile.am.in,
	  branches/KDE/3.5/kdeutils/Makefile.am.in,
	  branches/KDE/3.5/kdegraphics/Makefile.am.in: update automake
	  version

2006-08-10 07:18 +0000 [r571635]  mlaurent

	* branches/KDE/3.5/kdeadmin/knetworkconf/backends/platform.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/network.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/service.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/network-conf.in:
	  Add support for mandriva 2007

2006-08-11 08:06 +0000 [r571939-571938]  mlaurent

	* branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/.kdbgrc.knetworkconf
	  (removed): Not necessary to have it into svn

	* branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconfmodule.h,
	  branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconfmodule.cpp:
	  Fix signal conflict with kcmodule

2006-08-16 14:05 +0000 [r573526]  toivo

	* branches/KDE/3.5/kdeadmin/kpackage/kpPty.cpp: Fix executing
	  commands in BSD

2006-08-21 15:05 +0000 [r575427]  jriddell

	* branches/KDE/3.5/kdeadmin/COPYING-DOCS (added): Add FDL licence
	  for documentation

2006-08-24 18:57 +0000 [r576757]  helio

	* branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconfigparser.cpp:
	  - Move to Mandriva

2006-08-24 19:02 +0000 [r576758]  helio

	* branches/KDE/3.5/kdeadmin/knetworkconf/README,
	  branches/KDE/3.5/kdeadmin/knetworkconf/LEAME,
	  branches/KDE/3.5/kdeadmin/knetworkconf/pixmaps/mandriva.png
	  (added),
	  branches/KDE/3.5/kdeadmin/knetworkconf/pixmaps/mandrake.png
	  (removed), branches/KDE/3.5/kdeadmin/knetworkconf/backends/NEWS,
	  branches/KDE/3.5/kdeadmin/knetworkconf/pixmaps/Makefile.am: -
	  Move to Mandriva

2006-08-25 16:02 +0000 [r577118]  toivo

	* branches/KDE/3.5/kdeadmin/kpackage/main.cpp,
	  branches/KDE/3.5/kdeadmin/kpackage/kpackage.cpp,
	  branches/KDE/3.5/kdeadmin/kpackage/kpackage.h: Fix kpackage
	  stopping kde shutdown

2006-08-28 13:33 +0000 [r578130]  toivo

	* branches/KDE/3.5/kdeadmin/kpackage/rpmInterface.cpp: add
	  --allmatches option for removing rpm packages

2006-08-29 14:58 +0000 [r578529]  toivo

	* branches/KDE/3.5/kdeadmin/kpackage/rpmInterface.cpp: Fix
	  uninstall flag setting for RPM

2006-09-13 14:57 +0000 [r583805]  toivo

	* branches/KDE/3.5/kdeadmin/kpackage/debAptInterface.cpp: Get APT
	  upgrade correct

2006-09-18 14:53 +0000 [r586033]  toivo

	* branches/KDE/3.5/kdeadmin/kpackage/managementWidget.cpp: Remove
	  some unecessary updates with package install/uninstall

2006-09-20 07:49 +0000 [r586643]  fabo

	* branches/KDE/3.5/kdeadmin/knetworkconf/backends/platform.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/network.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/service.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/network-conf.in:
	  support debian etch 4.0, thanks to Sune Vuorela

2006-09-20 08:58 +0000 [r586668]  fabo

	* branches/KDE/3.5/kdeadmin/knetworkconf/backends/platform.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/service.pl.in:
	  Update Kubuntu 6.10 support

2006-10-02 11:07 +0000 [r591324]  coolo

	* branches/KDE/3.5/kdeadmin/kdeadmin.lsm: updates for 3.5.5

