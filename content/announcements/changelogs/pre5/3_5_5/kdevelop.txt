2006-08-18 09:32 +0000 [r574154]  mueller

	* branches/KDE/3.5/kdevelop/parts/documentation/interfaces/kdevdocumentationplugin.cpp:
	  This fixes the famous XIM crash that is "caused by a broken Qt"

2006-08-21 10:29 +0000 [r575334]  dymo

	* branches/KDE/3.5/kdevelop/parts/documentation/plugins/kdevtoc/Makefile.am,
	  branches/KDE/3.5/kdevelop/parts/documentation/plugins/chm/Makefile.am,
	  branches/KDE/3.5/kdevelop/parts/documentation/plugins/devhelp/Makefile.am,
	  branches/KDE/3.5/kdevelop/parts/documentation/plugins/doxygen/Makefile.am:
	  Backported build fixes by Jeremy Line from 3.4 branch.

2006-08-21 15:04 +0000 [r575425]  jriddell

	* branches/KDE/3.5/kdevelop/COPYING-DOCS (added): Add FDL licence
	  for documentation

2006-08-21 16:56 +0000 [r575545]  jriddell

	* branches/KDE/3.5/kdevelop/parts/appwizard/common/Makefile.am,
	  branches/KDE/3.5/kdevelop/parts/appwizard/common/COPYING-DOCS
	  (added): Add FDL

2006-08-29 15:08 +0000 [r578531-578530]  apaku

	* branches/KDE/3.5/kdevelop/parts/appwizard/common/admin (removed):
	  Delete admin dir to replace with an svn:external

	* branches/KDE/3.5/kdevelop/parts/appwizard/common: Add the
	  svn:externals

2006-09-12 15:47 +0000 [r583543]  rgruber

	* branches/KDE/3.5/kdevelop/parts/snippet/snippet_widget.cpp: Fixed
	  a crash that happened when the QListViewItem from the dropped()
	  signal is NULL BUG:133876

2006-09-28 21:49 +0000 [r589891]  escuder

	* branches/KDE/3.5/kdevelop/languages/php/phpfile.cpp: BUG: 134011
	  Fix parsing functions with { at next line by
	  mathias.dufresne@free.fr

2006-10-02 11:08 +0000 [r591338]  coolo

	* branches/KDE/3.5/kdevelop/kdevelop.lsm: updates for 3.5.5

2006-10-02 11:12 +0000 [r591341]  coolo

	* branches/KDE/3.5/kdevelop/configure.in.in: updates for 3.5.5

