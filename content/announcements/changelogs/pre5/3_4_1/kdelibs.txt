2005-03-04 11:55 +0000 [r394820]  waba

	* kio/kssl/ksslpeerinfo.cc: Match certificate based on the punycode
	  version of the hostname

2005-03-04 12:17 +0000 [r394828]  waba

	* kio/kssl/ksslpeerinfo.cc: Don't lower(), the punycode conversion
	  does normalisation already.

2005-03-04 12:48 +0000 [r394841]  goffioul

	* kdeprint/management/smbview.cpp,
	  kdeprint/management/kmdriverdbwidget.cpp,
	  kdeprint/marginpreview.cpp: [Backport] Fix resize cursors in
	  margin page and only use KDE cursors.

2005-03-04 17:54 +0000 [r394906]  waba

	* kio/kssl/Makefile.am: Make it compile on first install

2005-03-04 18:38 +0000 [r394914]  mattr

	* kdeui/kdockwidget.cpp: Backport the fix for
	  restoreFromForcedFixedSize (revision 1.172)

2005-03-04 18:47 +0000 [r394917]  mattr

	* kdeui/kdockwidget.cpp: backport revision 1.170
	  ----------------------- fix: keep the right size when redock
	  undocked widgets. Tested with: mainly k3b and some other apps.
	  Now I can drag/drop/undock/dock widgets and the size is always
	  correct. PS: There is a huge bug with highResolution, which has
	  effects here. I have fixed that locally here as well. So very
	  probably a patch follows :)

2005-03-04 22:14 +0000 [r394946]  dfaure

	* kioslave/ftp/ftp.cc: Backport: Fix parsing of Netware FTP
	  servers, so that files don't all appear with a lock (no
	  permissions). BUG: 76442

2005-03-04 23:00 +0000 [r394958]  dhaumann

	* kdeui/kdockwidget_private.cpp: BACKPORT: Fix missing statement
	  (which fixes yet another bug(s) :)

2005-03-05 02:30 +0000 [r394983]  thiago

	* kdecore/network/ksockssocketdevice.cpp: (Backport 1.6:1.7)
	  Backporting the fix to KDE 3.4.x. Sorry, this didn't make into
	  3.4.0. CCBUG:98018

2005-03-05 10:02 +0000 [r395003]  lukas

	* kio/Mainpage.dox: fixed mainpage so that kio overview docu shows
	  again

2005-03-05 12:16 +0000 [r395022]  amantia

	* knewstuff/knewstuffsecure.cpp, knewstuff/engine.cpp: Backport
	  KNewStuff fixes.

2005-03-05 14:27 +0000 [r395045]  dhaumann

	* kmdi/kmdi/dockcontainer.cpp: fix bug: 1. open sidebar on bottom.
	  2. overlap it 3. close kate 4. restart kate 5. Now the window was
	  totally borked :)

2005-03-05 14:43 +0000 [r395049]  dhaumann

	* kmdi/kmdidockcontainer.cpp: fix kmdi bug: 1. open sidebar on
	  bottom. 2. overlap it 3. close kate 4. restart kate 5. Now the
	  window was totally borked :)

2005-03-05 14:46 +0000 [r395051]  dhaumann

	* kmdi/kmdidockcontainer.cpp: fix for previous fix, just the other
	  way round.

2005-03-05 22:03 +0000 [r395143]  cullmann

	* kate/part/katebuffer.cpp: fix problem with utf16le fix stolen out
	  of khtml ;);) backport BUG: 98166

2005-03-06 14:47 +0000 [r395253]  waba

	* kioslave/http/kcookiejar/kcookiewin.cpp: Reverse the port number
	  and hostname when dislplaying it to the user.

2005-03-06 16:04 +0000 [r395262]  adawit

	* kioslave/http/kcookiejar/kcookiejar.cpp,
	  kioslave/http/kcookiejar/tests/cookie_settings.test: - Backport
	  fix for bug 51580 & 75250 along with a unit test for the fix.

2005-03-06 16:58 +0000 [r395281]  waba

	* kio/kio/job.cpp: Fix progress bar.

2005-03-06 19:37 +0000 [r395328]  dhaumann

	* kmdi/kmdi/dockcontainer.cpp, kmdi/kmdidockcontainer.cpp:
	  BACKPORT: fix bug (you all know it, I'm sure): 1. open sidebar 2.
	  non-overlap mode 3. close sidebar 4. restart app 5. Now the
	  non-overlap mode was lost, the flag just was not saved somehow.

2005-03-06 22:25 +0000 [r395354]  tokoe

	* kabc/lock.cpp: Don't change the identifier.

2005-03-07 11:25 +0000 [r395451]  wstephens

	* kdeui/kmainwindow.h: Fix session restoring for apps with 3 types
	  of KMainWindow

2005-03-07 12:17 +0000 [r395461]  mueller

	* kinit/kinit.cpp: fix duplicated program name in kdeinit'ed apps
	  (backport revision 1.148)

2005-03-07 15:50 +0000 [r395524]  staniek

	* kmdi/kmdimainfrm.cpp: Backported fix:
	  KMdiChildArea::focusTopChild() is called when QEvent::FocusIn
	  arrived. But for "not-childframe modes" this emits
	  lastChildFrmClosed().

2005-03-07 20:53 +0000 [r395613]  carewolf

	* khtml/html/html_formimpl.cpp: Backport of fix for #59701 CCBUG:
	  59701

2005-03-08 01:07 +0000 [r395650]  adawit

	* kio/kio/kprotocolmanager.cpp: Backport: Fix proxy autodetection
	  for webdav urls

2005-03-08 01:23 +0000 [r395651]  adawit

	* kdeui/kcompletionbox.cpp, kdeui/kcompletionbox.h: Backport: 1st
	  part of fix for Bug 100733. Reviewed by dfaure

2005-03-08 10:36 +0000 [r395716]  lunakl

	* kate/part/kateviewinternal.cpp: Backport r1.341 (initialize
	  m_selectionMode).

2005-03-08 11:07 +0000 [r395728]  dfaure

	* khtml/html/html_formimpl.cpp: Fixed infinite loop due to onChange
	  from textareas. BUG: 100963

2005-03-08 12:28 +0000 [r395759]  waba

	* kutils/kcmoduleproxy.cpp: Don't crash: clean up in the proper
	  order. Adrian: please include for SL9.3 CCMAIL: adrian@suse.de

2005-03-08 13:20 +0000 [r395767]  lunakl

	* kdeui/klineedit.cpp: Backport completion popup fix.

2005-03-09 15:19 +0000 [r396102]  waba

	* kabc/distributionlistdialog.cpp: Get rid of spurious & in e-mail
	  addresses, button->text() gets messed up by the automatic
	  accelerator code, fix button selection. SUSE BR67347

2005-03-09 16:41 +0000 [r396138]  mattr

	* kate/part/katerenderer.cpp: Backport revision 1.85 - Make colors
	  for marks work Should be in KDE 3.4.1 CCBUG: 100841 CCMAIL:
	  kwrite-devel@kde.org

2005-03-09 17:40 +0000 [r396151]  waba

	* kconf_update/kconf_update.cpp: Put logfile in a separate
	  directory to make sure it doesn't trigger KDirWatch which will
	  result in kconf_update getting started again. CCBUG: 100732

2005-03-09 18:06 +0000 [r396159]  dfaure

	* khtml/ecma/kjs_window.cpp, khtml/ecma/kjs_window.h: Don't crash
	  when JS is disabled, based on patch by Mario Weilguni.

2005-03-09 19:07 +0000 [r396176]  mattr

	* kate/part/kateconfig.cpp, kate/part/katerenderer.cpp: backport
	  kateconfig.cpp revision 1.67 and katerenderer.cpp 1.86 This is
	  the more correct solution for bug 100841. CCBUG: 100841 CCMAIL:
	  kwrite-devel@kde.org

2005-03-10 12:50 +0000 [r396329]  thiago

	* kioslave/http/http.cc: (Backport 1.640:1.641) Backporting the fix
	  thatfixed bug 101149. CCBUG:101149

2005-03-10 15:49 +0000 [r396374-396372]  reed

	* kinit/setproctitle.cpp: (backport) check for HAVE_SYS_EXEC_H
	  before including sys/exec.h

	* kioslave/http/configure.in.in: (backport) Kerberos is the proper
	  framework for GSSAPI on darwin

2005-03-10 19:03 +0000 [r396426]  reed

	* dnssd/configure.in.bot, dnssd/configure.in.in: (backport)
	  libdns_sd is part of libSystem (ie, libc) on darwin

2005-03-10 20:21 +0000 [r396447]  adridg

	* kdeui/kpopupmenu.h: (backport) For consistency, and for systems
	  (*cough* gcc 2.95-based 4-stable) where NULL is #defined stupidly

2005-03-10 21:02 +0000 [r396460]  bero

	* kdecore/kapplication.h: Fix typo

2005-03-10 22:21 +0000 [r396505]  mkretz

	* kutils/ksettings/README.dox: backport: Why do I write usefull
	  documentation if I don't commit it? This must have been lying on
	  my harddisk since at least aKademy.

2005-03-11 06:58 +0000 [r396579]  djarvie

	* kdecore/kwin.h: Fix documentation typos

2005-03-11 21:09 +0000 [r396797]  dhaumann

	* kate/data/cpp.xml: backport Qt4 keyword: foreach

2005-03-12 19:22 +0000 [r397038]  dhaumann

	* kmdi/kmdi/mainwindow.cpp: Backport: Fix toolview accessors. The
	  signal was connected twice, so a CTRL+ALT+SHIFT+l opened and
	  closed the toolview immediately. (This bug does not exist in
	  kmdi1).

2005-03-12 21:14 +0000 [r397065]  dhaumann

	* kate/part/katedocument.cpp: BACKPORT cullmann's fix for a crash
	  :)

2005-03-13 14:45 +0000 [r397265]  waba

	* kdecore/kkeynative_x11.cpp, kdecore/kkeyserver_x11.cpp: Fix
	  casting for 64-bit PPC.

2005-03-15 09:54 +0000 [r397755]  waba

	* kdeui/kspell.cpp: Fix recursion problem (SUSE BR72773)

2005-03-16 04:02 +0000 [r397976]  thiago

	* kdecore/network/kresolver.cpp: (Backport 1.44:1.45) Make all
	  domains lowercase everywhere. CCBUG:101558

2005-03-16 15:52 +0000 [r398122]  dfaure

	* khtml/kmultipart/kmultipart.cpp: Fix KMultipart's reaction to
	  bugzilla sending "text/html; charset=utf-8" as Content-Type BUGS:
	  77333, 83476

2005-03-17 15:26 +0000 [r398400]  lunakl

	* arts/knotify/knotify.cpp: Backport r1.96 (#98960).

2005-03-17 16:11 +0000 [r398415]  pletourn

	* kio/kfile/kdirsize.cpp: Don't assume there is a '.' entry, i.e
	  for tar:/

2005-03-17 17:23 +0000 [r398429]  dfaure

	* kdeui/ksconfig.cpp: Backport CVS commit by woebbe: Apply the
	  patch to fillDicts() too to make KMail's DictionaryComboBox work
	  with newest ASpell.

2005-03-17 19:48 +0000 [r398463]  dhaumann

	* kate/data/fortran.xml: Backport fix for borked rule.

2005-03-18 00:03 +0000 [r398576]  carewolf

	* kio/magic: Backport magic

2005-03-18 03:24 +0000 [r398603]  thiago

	* kdecore/network/kresolverstandardworkers.cpp: (Backport
	  1.17:1.18) Backporting the fix for the ipv6 blacklist in
	  ioslaves.

2005-03-18 07:17 +0000 [r398614]  mueller

	* kdeui/kactioncollection.h: fix compile (gcc 4.1) (backport
	  revision 1.187)

2005-03-18 10:00 +0000 [r398647]  mueller

	* kate/part/katetextline.h: fix compile (gcc 4.1) (backport
	  revision 1.76)

2005-03-18 10:51 +0000 [r398660]  waba

	* kio/bookmarks/kbookmarkmenu.cc: Typo alert: Fix Netscape
	  bookmarks (SUSE BR71888)

2005-03-18 12:54 +0000 [r398689]  dfaure

	* kdeui/kedittoolbar.cpp: Ooops, didn't intend to commit that part,
	  that was only a test. BUG: 101735

2005-03-18 13:09 +0000 [r398693]  dfaure

	* kdeui/kedittoolbar.cpp: same thing here - wasn't intended. Thx
	  again to coolo for noticing.

2005-03-18 16:23 +0000 [r398756-398750]  ggarand

	* khtml/html/html_imageimpl.cpp: backport crash fix CCBUG: 78205,
	  84173

	* khtml/rendering/break_lines.cpp: backport "fix crash in Thai word
	  breaking code"

	* khtml/ecma/kjs_dom.cpp: backport regression fix CCBUG: 99380

	* khtml/rendering/render_flow.cpp, khtml/rendering/render_box.cpp:
	  backport crash fix CCBUG: 97085

	* khtml/css/cssparser.cpp: backport parseShortHand bug fix CCBUG:
	  100947

	* khtml/html/html_baseimpl.cpp: backport crash fix CCBUG: 86973,
	  98975

2005-03-18 22:21 +0000 [r398853]  pletourn

	* kio/kio/job.cpp: Don't crash if the slave emits an error inside
	  SimpleJob::start() (this will delete the job) CCBUG:95488 I guess
	  DataSlave needs to implement 'stat' to make it work

2005-03-20 18:03 +0000 [r399278]  adawit

	* kdeui/kcompletionbox.cpp: Backport: Emit signal iff an item is
	  selected in the dropdown list.

2005-03-20 23:31 +0000 [r399372]  dhaumann

	* kate/part/katesearch.cpp: Backport Anders' fix for bug: allow to
	  replace with nothing. CCBUG: 101895

2005-03-21 14:16 +0000 [r399416]  waba

	* kinit/klauncher.cpp: Improved guessing of DCOP name, prevents
	  spurious "could not start /bin/sh" error from kicker.

2005-03-21 14:20 +0000 [r399418]  waba

	* kio/kio/krun.cpp: Handle tilde in Exec line of .desktop files
	  BUG: 99440

2005-03-21 17:58 +0000 [r399476]  savernik

	* kate/part/katesearch.cpp: Fixed nasty crash on accepting Find and
	  Replace dialogs. BUG: 99842

2005-03-22 16:22 +0000 [r399738]  lunakl

	* kdecore/kapplication.cpp: Backport #99205.

2005-03-22 20:03 +0000 [r399797]  dhaumann

	* kate/part/katedocument.cpp: backport: fix possibly crash due to
	  an invalid cursor position.

2005-03-23 00:02 +0000 [r399865]  thiago

	* dnssd/servicebrowser.cpp: Backporting the fix from r1.34:1.35,
	  fixing a compilation problem with gcc2.96.

2005-03-23 08:37 +0000 [r399930-399929]  lukas

	* kdeui/qxembed.cpp: backport CVS commit by lunakl: The widget for
	  XSetInputFocus() must be visible. (#99267)

	* knewstuff/downloaddialog.cpp: backport CVS commit by josef: - fix
	  sorting for date and rating columns too

2005-03-23 08:53 +0000 [r399932]  lukas

	* kdeprint/kprinter.cpp: backport CVS commit by pfeifle: - Do no
	  re-order specified pages or remove duplicated ones. Print what
	  the user specified instead. (#99769) - Suppress the use of
	  deprecated functions

2005-03-23 08:57 +0000 [r399935]  coolo

	* dnssd/servicebrowser.cpp: reverting patch in branch too

2005-03-23 20:50 +0000 [r400087]  dfaure

	* kdecore/kcmdlineargs.cpp: Backport fix for "kontact --module
	  kmail" + "kmail faure@kde.org"

2005-03-24 10:30 +0000 [r400198]  dfaure

	* kioslave/ftp/ftp.cc: * Try non-passive mode if passive mode fails
	  (this got broken by the big rewrite) * Don't re-use the same port
	  number for all commands - although this should be fine, that
	  server doesn't work when we reuse the same port (this patch is
	  from Thiago)

2005-03-24 12:47 +0000 [r400215]  coolo

	* kdecore/kcheckaccelerators.cpp: catch self changing GUIs
	  (#102116)

2005-03-24 17:15 +0000 [r400273]  savernik

	* khtml/rendering/render_flow.cpp,
	  khtml/rendering/render_table.cpp: Two pending backports. - Fixed
	  repaints of dynamically changed table elements (bug 100649) -
	  Fixed repaints of inline elements with top or bottom padding(bug
	  100779) CCBUGS: 100649, 100779

2005-03-25 09:10 +0000 [r400442]  larkang

	* kdoctools/main_ghelp.cpp: Backport: Fix export

2005-03-25 11:45 +0000 [r400475]  adridg

	* dnssd/publicservice.cpp: Need types to compile

2005-03-25 19:58 +0000 [r400601]  dhaumann

	* kmdi/kmdi/mainwindow.h: BACKPORT: fix unknown slot. This doesn't
	  have anything to do with two mainwindows though I fear.
	  CCBUG:102474

2005-03-26 11:14 +0000 [r400721]  danimo

	* kdeui/klistviewsearchline.cpp: Backport fix from HEAD: Work with
	  Qt Designer. Workaround for those who use KDE 3.4.0: Use
	  KDevDesigner. BUG:

2005-03-27 15:43 +0000 [r401019-401018]  brade

	* kio/kio/kdirlister.cpp, kio/kio/kdirlister_p.h,
	  kio/kio/kdirlister.h: Backport the stuff I did lately: - fix
	  crashes because of not fully handled redirections - optimization:
	  speed updates up by about 11-12% - fix crashes in stop()

	* kio/kio/kfileitem.cpp, kio/kio/kfileitem.h: Backport KFileItem
	  improvements: - removed duplicated code, which is already in
	  init(), from refresh() - less new/delete operations in assign() -
	  new setUDSEntry() to create a new KFileItem from the current one
	  without any new/delete operations or stack allocations

2005-03-27 15:47 +0000 [r401020]  brade

	* kio/kio/jobclasses.h: Backport the new accessor for
	  m_redirectionURL.

2005-03-27 18:58 +0000 [r401058]  staniek

	* kate/part/katedocument.cpp: typo fixed (backported)

2005-03-28 21:16 +0000 [r401409]  kloecker

	* kabc/scripts/addressee.src.cpp: Backport fix for bug 96414:
	  Replace the email address parser by the parser which is used in
	  KMail and which supports basically all RFC 2822 compliant email
	  addresses.

2005-03-29 11:55 +0000 [r401577]  dfaure

	* khtml/khtmlview.cpp: Send mouse release event to KHTMLPart even
	  when no url was opened (e.g. konqueror with intro disabled). This
	  allows to handle MMB-pasting. BUG: 61179

2005-03-29 12:23 +0000 [r401584]  lunakl

	* kdecore/kwin.cpp: Backport, initialize atom values before using
	  them.

2005-03-29 14:27 +0000 [r401619]  lunakl

	* kdecore/kglobalaccel_x11.cpp, kdecore/kkeynative.h,
	  kdecore/kkeynative_x11.cpp, kdecore/kkeyserver_x11.cpp,
	  kdecore/kkeyserver_x11.h: Backport #102742.

2005-03-29 16:59 +0000 [r401675]  aacid

	* kdeui/ktoolbarbutton.cpp: Fix crash if m_parent is not a KToolBar
	  like happens in Umbrello Aproved by David Faure

2005-03-29 20:24 +0000 [r401741]  dhaumann

	* kate/data/progress.xml: update progress.xml file.

2005-03-30 14:43 +0000 [r401943]  waba

	* kdecore/kmultipledrag.h: Docu: Don't use obsolete function in
	  example

2005-03-30 20:03 +0000 [r402001]  zander

	* kdeui/kkeydialog.cpp: Don't claim keysequence is already in use
	  when the keysequence is really empty

2005-03-31 03:54 +0000 [r402068-402066]  ggarand

	* khtml/rendering/break_lines.cpp: backport "compile with defined
	  HAVE_LIBTHAI"

	* khtml/rendering/render_object.cpp: backport "fix logic error in
	  removeFromObjectLists" (#99854)

	* khtml/xml/dom_nodeimpl.cpp: backport traverseNextNode related
	  crash fix (#101711)

2005-03-31 11:50 +0000 [r402132]  dfaure

	* kio/kio/kzip.cpp: Fixed double-deletion bug when trying to write
	  to a non-writable directory.

2005-03-31 13:48 +0000 [r402153]  waba

	* kioslave/http/http.cc: Don't close connection on 304 Not Modified
	  BUG: 88007

2005-03-31 19:04 +0000 [r402224]  aacid

	* kdoctools/customization/ca/user.entities,
	  kdoctools/customization/ca/lang.entities,
	  kdoctools/customization/ca/catalog: Updates by Antoni Bella

2005-04-01 20:06 +0000 [r402496]  dhaumann

	* kate/data/fortran.xml: backport update.

2005-04-01 20:19 +0000 [r402501]  dhaumann

	* kate/data/progress.xml: update.

2005-04-03 16:30 +0000 [r402887]  thiago

	* kdeprint/tools/escputil/escpwidget.cpp: Backporting the fix for
	  the library. BACKPORT:1.11:1.12 CCBUG:103005

2005-04-03 20:55 +0000 [r402953]  binner

	* kio/kfile/kurlbar.cpp: Don't show edit dialog when drop-adding
	  items

2005-04-04 08:08 +0000 [r403043]  dhaumann

	* kate/data/sql.xml: updabe by Yury Lebedev.

2005-04-04 10:59 +0000 [r403087]  ervin

	* kio/kio/job.cpp, kio/kio/jobclasses.h: Add the ability to disable
	  message box use from Jobs, because the can be used with a
	  KApplication with gui disabled (and then make it crash).
	  Backporting to 3.4.x branch. CCBUGS:102086

2005-04-04 19:10 +0000 [r403171]  dhaumann

	* kate/part/kateautoindent.cpp, kate/part/katerenderer.cpp:
	  BACKPORT due to requests: kateaudoinent: "* /" -> "*/" for C
	  Style and S&S katerenderer : make matching {} () [] bold.

2005-04-04 22:05 +0000 [r403199]  dfaure

	* kio/kio/krun.cpp: Backport: First half of the fix for
	  print:/manager not embedding the right part (should help any
	  ioslave using inode/directory derivatives) : don't assume that
	  S_IFDIR means inode/directory, check if stat() returned a
	  mimetype. This can also save a roundtrip for files where the
	  ioslave knows the mimetype right away.

2005-04-05 02:06 +0000 [r403232]  thiago

	* kdecore/network/kresolver.cpp: Backporting the fix for the
	  separators. BACKPORT:1.45:1.46

2005-04-05 15:14 +0000 [r403320]  binner

	* kio/kfile/kdiroperator.cpp: If Shift is pressed when menu opens
	  show 'Delete' instead of 'Trash' (#100394)

2005-04-06 23:48 +0000 [r403676]  dfaure

	* kutils/tests/kfindtest.cpp, kutils/kreplace.cpp,
	  kutils/tests/kreplacetest.cpp, kutils/tests/Makefile.am,
	  kutils/kfind.cpp, kutils/kreplace.h: Backport all find/replace
	  fixes from head: - Fixed the case of replacing the empty string
	  with something - Fixed support for back ref \0 (entire matched
	  text) (#102495) - Fixed QDict runtime warning The tests run fine,
	  although kreplacetest keeps kwin really really busy with all its
	  dialog boxes....

2005-04-07 10:22 +0000 [r403732]  wgreven

	* kio/kio/kdirlister.cpp: Backport fix for #103391.

2005-04-07 12:39 +0000 [r403763]  dfaure

	* kio/kio/kmimetype.cpp: Make iconForURL("trash:/") use the trash
	  icon, i.e. prefer the protocol icon [or favicon] when there's
	  one, over the mimetype icon, for the root of a protocol.
	  +untabify BUG: 100321

2005-04-07 13:39 +0000 [r403784]  akrille

	* kdoctools/customization/de/user.entities: Make artsbuilder-docu
	  work. At least for the germans... CVS_SILENT

2005-04-07 13:54 +0000 [r403789]  dhaumann

	* kate/part/katesupercursor.cpp: backport cullmann's commit: fix
	  that return moves the current view if you press it at start of
	  first line :/ This is in CVS since months, and nobody noticed

2005-04-07 21:26 +0000 [r403908]  dhaumann

	* kate/part/katedocument.cpp: cullmann-backport: security fix: use
	  right permissions for the backup files if the filepermissions of
	  the original are not accessable, why ever, use 0600 as fallback
	  CCBUG:103331

2005-04-08 12:00 +0000 [r404041]  thiago

	* kdecore/network/kresolvermanager.cpp: Backporting the "random
	  resolver failure" problem to KDE 3.4.x. BACKPORT:1.34:1.35
	  CCBUG:94703

2005-04-08 18:27 +0000 [r404121]  dhaumann

	* kate/part/kateviewinternal.cpp: backport: wrong int -><- uint
	  conversion caused all that sluggish scrolling at the first few
	  lines

2005-04-08 19:38 +0000 [r404140]  dhaumann

	* kate/part/kateviewinternal.cpp: backport: make selection-continue
	  work as supposed. Had to change m_view to m_doc, as the selection
	  in HEAD is per view now ;) CCBUG:99598

2005-04-09 16:15 +0000 [r404408]  grossard

	* kdoctools/customization/fr/user.entities: changed a name

2005-04-11 09:44 +0000 [r404739]  tilladam

	* kio/kio/davjob.cpp, kio/kio/davjob.h: Fix encoding of webdav
	  documents by keeping the stream from the server as a byte array
	  internally, instead of a QString and auto-detecting encoding in
	  QDomDocument::setContent(). Keep if BC to be on the safe side.
	  Whoever maintains this, ok to backport? Jan-Pascal? CCMAIL:
	  janpascal@vanbest.org

2005-04-12 22:40 +0000 [r405186]  dfaure

	* kio/kio/kdirlister_p.h: Don't call KDirWatch for remote urls,
	  fixes a runtime warning when reloading a FTP url.

2005-04-13 00:57 +0000 [r405206]  mueller

	* kio/misc/kntlm/kswap.h, kdecore/Makefile.am,
	  kdecore/kmdcodec.cpp, kdecore/kswap.h (removed): don't install
	  kswap.h. Yes this breaks SC, but consensus seems its worth it.

2005-04-13 12:56 +0000 [r405305-405304]  lunakl

	* khtml/khtmlview.cpp, khtml/khtmlview.h, khtml/khtml_part.cpp:
	  Backport #87793 (no grab for type-ahead).

	* khtml/misc/loader_jpeg.cpp: Backport proper loading of
	  progressive JPEGs.

2005-04-13 14:19 +0000 [r405335]  staikos

	* kio/misc/kssld/kssld.cpp, kio/kssl/ksslcertificate.cc,
	  kio/kssl/ksslcertificate.h, kio/misc/kssld/kssld.h: backport fix
	  for 61446

2005-04-13 15:37 +0000 [r405349]  tokoe

	* kabc/plugins/dir/resourcedir.cpp: Backport of bugfix #103575
	  BUGS:103575

2005-04-14 09:46 +0000 [r405501]  coolo

	* doc/kdelibs/Makefile.am: first build meinproc _then_ manpages

2005-04-15 08:07 +0000 [r405679]  dhaumann

	* kate/part/katecodecompletion.cpp: Backport Anders' fix: Don't
	  show the completion box if it is allready in use. This seems to
	  solve conflicts when more clients use the completion. CCMAIL:
	  kwrite-devel@kde.org

2005-04-15 11:30 +0000 [r405713]  lunakl

	* kdecore/netwm.cpp: Backport remap properly the properties array.

2005-04-16 10:50 +0000 [r405870]  dhaumann

	* kate/data/cpp.xml: oops, almost missed the backport for #if
	  0...#endif comments.

2005-04-18 13:08 +0000 [r406297]  lunakl

	* kimgio/g3r.cpp: Backport check for failed memory allocation.

2005-04-18 15:17 +0000 [r406339]  dfaure

	* kioslave/http/webdav.protocol, kioslave/http/webdavs.protocol:
	  Backport massive speed improvement for deleting Webdav folders

2005-04-19 10:37 +0000 [r406522]  mueller

	* kimgio/pcx.cpp: improve out-of-memory checks

2005-04-19 10:48 +0000 [r406527]  mueller

	* kimgio/pcx.cpp, kimgio/rgb.cpp, kimgio/tiffr.cpp, kimgio/jp2.cpp,
	  kimgio/exr.cpp, kimgio/pcx.h, kimgio/xcf.cpp, kimgio/xview.cpp,
	  kimgio/xcf.h, kimgio/psd.cpp: various improvements to input
	  handling

2005-04-19 18:50 +0000 [r406586]  pletourn

	* khtml/khtmlview.cpp: Don't interpret a right click, just after a
	  double click, as a triple click Don't interpret a double click,
	  long after a click, as a triple click

2005-04-19 19:00 +0000 [r406588]  pletourn

	* kdeui/kcompletionbox.cpp: Don't close the completion box after
	  clicking on the scrollbar when currentItem() != 0

2005-04-19 19:01 +0000 [r406589]  dhaumann

	* kate/data/doxygen.xml: fix hl bug 103656.

2005-04-19 19:13 +0000 [r406591]  pletourn

	* kdeui/kpopupmenu.cpp: Don't relay the mouse release event to
	  QPopupMenu when a context menu is shown BUG:87764

2005-04-19 19:18 +0000 [r406593]  dhaumann

	* kate/data/doxygen.xml: fix for previous fix.

2005-04-19 20:23 +0000 [r406618]  dhaumann

	* kate/data/doxygen.xml: this is the 3rd attempt to fix #103656.
	  CCBUG:103656

2005-04-20 08:17 +0000 [r406683]  binner

	* pics/crystalsvg/cr22-action-edittrash.png,
	  pics/crystalsvg/cr32-action-edittrash.png,
	  pics/crystalsvg/cr16-action-edittrash.png,
	  pics/crystalsvg/cr22-action-emptytrash.png (added),
	  pics/crystalsvg/cr32-action-emptytrash.png (added),
	  pics/crystalsvg/cr16-action-emptytrash.png (added): Fix/extend
	  trash icons as posted on kde-artists

2005-04-21 13:20 +0000 [r406920]  mlaurent

	* kdeui/kbugreport.cpp: Backport fix use kcm_useraccount

2005-04-21 18:02 +0000 [r406975]  savernik

	* kdecore/kstringhandler.cpp: Don't divide by zero. This fixes a
	  crash in kopete 0.9, and also seems to fix a crash in akregator.
	  BUG: 101577

2005-04-21 22:41 +0000 [r407032]  woebbe

	* kmdi/kmdidockcontainer.cpp: backport of rev. 1.75

2005-04-22 15:33 +0000 [r407160]  thiago

	* kdecore/network/kresolvermanager.cpp,
	  kdecore/network/kresolver_p.h: Fixing two issues, backported from
	  HEAD (minus the debugging). BACKPORT:1.35:1.36 for
	  kresolvermanager.cpp BACKPORT:1.17:1.18 for kresolver_p.h

2005-04-22 20:43 +0000 [r407227]  carewolf

	* khtml/misc/loader.cpp: Backport of #79065 fix

2005-04-22 20:47 +0000 [r407230-407229]  carewolf

	* khtml/css/css_base.cpp, khtml/css/parser.y, khtml/css/css_base.h,
	  khtml/css/cssstyleselector.cpp, khtml/css/parser.cpp: Backport of
	  strict pseudoclass parsing

	* khtml/html/htmltokenizer.cpp: Backport of #82393 and #93025 fixes

2005-04-22 20:55 +0000 [r407233]  carewolf

	* khtml/rendering/render_replaced.cpp,
	  khtml/html/html_tableimpl.cpp, khtml/rendering/table_layout.cpp,
	  khtml/rendering/render_table.cpp, khtml/rendering/render_table.h:
	  Backport patch for table padding

2005-04-23 10:31 +0000 [r407326]  lukas

	* khtml/rendering/render_table.cpp: compile

2005-04-24 09:20 +0000 [r407462]  kuvatov

	* kspell2/ui/dialog.cpp,
	  kspell2/plugins/aspell/kspell_aspelldict.cpp,
	  kspell2/backgroundchecker.cpp, kspell2/backgroundengine.cpp,
	  kspell2/backgroundchecker.h, kspell2/backgroundengine.h: Backport
	  fix for 104130. CCBUGS:104130

2005-04-24 09:38 +0000 [r407464]  kuvatov

	* kspell2/plugins/aspell/kspell_aspelldict.cpp: Backport fix for
	  bug 87250. CCBUGS:87250

2005-04-24 15:28 +0000 [r407538]  waba

	* kio/kfile/kicondialog.cpp: Pressing Enter selects icons. BUG:
	  104475

2005-04-24 18:19 +0000 [r407580]  porten

	* khtml/html/html_formimpl.cpp: value() should never return a null
	  string. BUGS:99970

2005-04-24 18:26 +0000 [r407581]  porten

	* khtml/ChangeLog: value() fix

2005-04-24 22:38 +0000 [r407635]  porten

	* khtml/html/html_formimpl.cpp, khtml/ChangeLog,
	  khtml/dom/html_form.cpp: moved the value fix out into the DOM
	  layer so that null DOMStrings remain an internal special value.
	  Whether it should be the responsibility of the DOM API or
	  ECMAScript bindings to convert DOMString() to "" is still
	  unclear.

2005-04-30 10:14 +0000 [r408771]  porten

	* khtml/ecma/kjs_window.cpp, khtml/ChangeLog: unbreak calling
	  close() on other windows. It has it own set of security checks.
	  CCBUGS:101178

2005-05-01 11:29 +0000 [r408993]  toma

	* kdoctools/customization/nl/user.entities: Sync with head.

2005-05-02 04:37 +0000 [r409143]  klichota

	* kdoctools/customization/pl/user.entities: Updated entities to
	  match documentation

2005-05-02 13:12 +0000 [r409182]  mfranz

	* kimgio/rgb.cpp: revert the stricter sanity check. It is wrong and
	  totally broke the importer. start + length *may* be equal to data
	  size: let's say we have 8 bytes data, and want to address a one
	  byte long data block on the last position. This makes a start
	  offset of 7 plus a data block length of 1 => 8.

2005-05-06 17:57 +0000 [r410100]  berendsen

	* kate/data/mup.xml: various fixes and small updates for Mup 5.1

2005-05-06 18:40 +0000 [r410114]  berendsen

	* kate/data/mup.xml: more 5.1 fixes

2005-05-07 10:54 +0000 [r410265]  adawit

	* kio/kio/kprotocolmanager.cpp: Backport for fox BR# 105206

2005-05-08 08:20 +0000 [r410647]  binner

	* kdoctools/Makefile.am: forgotten backport of .svn/ related fix

2005-05-08 08:56 +0000 [r410708]  binner

	* Makefile.am.in: Fix messages target for .svn/

2005-05-08 10:09 +0000 [r410736]  mbuesch

	* kmdi/kmdichildfrm.cpp: Backport: SVN commit 410734 by mbuesch:
	  fix memory leak. From QT documentation: bool
	  QApplication::sendEvent ( QObject * receiver, QEvent * event )
	  [static] <SNIP> The event is not deleted when the event has been
	  sent. The normal approach is to create the event on the stack,
	  e.g. QMouseEvent me( QEvent::MouseButtonPress, pos, 0, 0 );
	  QApplication::sendEvent( mainWindow, &me ); If you create the
	  event on the heap you must delete it.

2005-05-08 11:47 +0000 [r410820]  mbuesch

	* kmdi/kmdichildview.cpp: Backport: SVN commit 410819 by mbuesch:
	  Fix ==27012== ==27012== Conditional jump or move depends on
	  uninitialised value(s) ==27012== at 0x1BEB7946:
	  KMdiChildView::eventFilter(QObject*, QEvent*)
	  (kmdichildview.cpp:633)

2005-05-10 10:32 +0000 [r411876]  binner

	* kdelibs.lsm: update lsm for release

2005-05-10 10:38 +0000 [r411881]  binner

	* kdecore/kdeversion.h, README: Update version before everyone
	  checkout and compiles it :-)

2005-05-10 12:23 +0000 [r411902]  orlovich

	* kio/kfile/kdiroperator.cpp: Backport fix for missing i18n
	  (#104479)

2005-05-10 15:29 +0000 [r412000-411998]  carewolf

	* khtml/html/dtd.cpp: Backport of #101678 fix

	* khtml/ecma/kjs_html.cpp, khtml/ecma/kjs_html.h: Also disable
	  type() overload in branch

2005-05-12 18:25 +0000 [r412816]  pletourn

	* khtml/khtml_ext.cpp: Convert the pixmap to an image only when
	  needed BUG:105112

2005-05-12 18:31 +0000 [r412818]  dfaure

	* khtml/rendering/render_replaced.cpp: Returning false from
	  onkeypress should prevent the keys from being processed,
	  backport. BUG: 99749

2005-05-13 13:08 +0000 [r413143]  lunakl

	* khtml/khtml_part.cpp: Backport avoiding unneccessary horizontal
	  scrolling while searching.

2005-05-15 06:37 +0000 [r414028]  binner

	* kdeui/about/box-top-right.png: fix corrupted file

2005-05-15 21:24 +0000 [r414365]  thiago

	* kdecore/network/kresolver.cpp: .br now has IDN, and has
	  anti-phishing policies. BACKPORT:414347:414348

2005-05-15 21:53 +0000 [r414391]  pletourn

	* kdeui/ktoolbarbutton.cpp: Make the test more robust

2005-05-16 19:11 +0000 [r414720]  porten

	* kjs/error_object.cpp, kjs/lexer.cpp, kjs/regexp.h,
	  kjs/regexp_object.cpp, kjs/bool_object.cpp, kjs/string_object.h,
	  kjs/date_object.cpp, kjs/function.cpp, kjs/ChangeLog,
	  kjs/nodes.cpp, kjs/regexp.cpp, kjs/string_object.cpp: merged
	  fixes from trunk. see ChangeLog for list.

2005-05-16 20:40 +0000 [r414743]  grossard

	* kdoctools/customization/fr/user.entities: changed email address
	  for Yves Dessertine

2005-05-16 21:27 +0000 [r414758]  grossard

	* kdoctools/customization/fr/user.entities: added entoty for
	  Sebastien Renard

2005-05-17 09:32 +0000 [r414905]  deller

	* kio/kio/tcpslavebase.cpp: backport from HEAD: fix storing kssl
	  passwords in the wallet

2005-05-17 11:36 +0000 [r414936]  marchand

	* kdeprint/cups/cupsdconf (removed): drop it in 3.4 branch too (who
	  needs a checkout to do this these days :) => svn rm
	  svn+ssh://svn.kde.org/home/kde/branches/KDE/3.4/kdelibs/kdeprint/cups/cupsdconf
	  )

2005-05-17 17:13 +0000 [r415065]  dfaure

	* kio/kio/job.cpp: Backport r415056: Merge the two interactive
	  bools otherwise job->setInteractive() doesn't work if job is a
	  CopyJob that is known as a Job *.

2005-05-17 18:03 +0000 [r415100]  binner

	* kioslave/http/http.cc: prevent endless busy loop (#102925)

2005-05-17 18:44 +0000 [r415122]  brade

	* kio/kio/kdirlister.cpp: Yep yep, I do think about backports :)
	  But svnbackport doesn't work for me, so I had to read the docu of
	  how to do it properly :) Here's the fix from HEAD for bugs
	  #103795, #105827, #104973.

2005-05-17 19:46 +0000 [r415161]  porten

	* khtml/html/html_tableimpl.cpp, khtml/ecma/kjs_window.cpp,
	  khtml/ChangeLog, khtml/html/html_tableimpl.h: merged revisions
	  410300, 412785 and 410258.

2005-05-17 19:53 +0000 [r415170]  porten

	* khtml/html/html_elementimpl.cpp, khtml/ChangeLog: merged
	  innerHTML/Text fix from trunk (r413942).

2005-05-18 00:14 +0000 [r415253]  ggarand

	* khtml/rendering/render_layer.cpp, khtml/ecma/kjs_dom.cpp,
	  khtml/rendering/render_layer.h,
	  khtml/rendering/render_object.cpp,
	  khtml/rendering/render_box.cpp: backport scrolling layers
	  regression fix CCBUG: 102235 BUG: 103204

2005-05-18 00:38 +0000 [r415256]  porten

	* khtml/xml/dom2_eventsimpl.cpp, khtml/ChangeLog: dblclick event
	  listener fix

2005-05-18 16:34 +0000 [r415455]  deller

	* kio/kio/tcpslavebase.cpp: use original (already translated)
	  string

2005-05-19 13:54 +0000 [r415735]  staikos

	* kio/kssl/kssl/sf-class2-root.pem (added),
	  kio/kssl/kssl/gd-class2-root.pem (added),
	  kio/kssl/kssl/staatdernederlandenrotca.pem (added),
	  kio/kssl/kssl/netlock4.pem (added),
	  kio/kssl/kssl/caroot/ca-bundle.crt, kio/kssl/kssl/localcerts,
	  kio/kssl/kssl/ksslcalist: backport CA root update FEATURE: 101322
	  FEATURE: 100097

2005-05-19 14:49 +0000 [r415766-415765]  staikos

	* kio/kssl/kssl/geotrust-global-1.pem (added),
	  kio/kssl/kssl/caroot/ca-bundle.crt,
	  kio/kssl/kssl/geotrust-global-2.pem (added),
	  kio/kssl/kssl/localcerts, kio/kssl/kssl/ksslcalist: backport
	  addition of geotrust global roots

	* kio/kssl/kssl/xgca.pem (added), kio/kssl/kssl/belgacom.pem
	  (added), kio/kssl/kssl/oces.pem (added): these should be in the
	  branch too

2005-05-20 22:15 +0000 [r416248]  craig

	* kio/kio/slavebase.cpp: Output progress info when all bytes
	  processed, regardless of whether it has been more than 100ms or
	  not since the last update. (Should fix bug 96108)

2005-05-21 14:28 +0000 [r416422]  porten

	* khtml/html/html_documentimpl.cpp, khtml/ChangeLog: merged onload
	  fix from trunk (r416420)

2005-05-21 22:40 +0000 [r416576]  thiago

	* kdecore/network/ksocketdevice.cpp,
	  kdecore/network/kstreamsocket.cpp: Backporting the bug fixes that
	  were found in the KDE4 branch. BACKPORT:416572

2005-05-23 06:51 +0000 [r417238]  dgp

	* kdeui/Makefile.am: Make sure that .h file are created from .ui
	  before starting compile units. (Needed by Gentoo/FreeBSD for
	  instance.)

