2006-06-08 14:36 +0000 [r549416]  harris

	* branches/KDE/3.5/kdeedu/libkdeedu/extdate/extdatetime.cpp: Need
	  to distinguish the long/short forms of month and day names with
	  i18n() comments. In particular, this is necessary for the month
	  of May, which has the same long/short form. For consistency, I
	  added i18n() comment strings to all short-form names. This is a
	  backport from trunk, it closes bug #127011 CCBUG: 127011 CCMAIL:
	  kstars-devel@kde.org

2006-06-10 15:07 +0000 [r549991]  lueck

	* branches/KDE/3.5/kdeedu/doc/kmplot/introduction.docbook,
	  branches/KDE/3.5/kdeedu/doc/kturtle/using-kturtle.docbook:
	  documentation backport from trunk BUG:127787
	  CCMAIL:kde-doc-english@kde.org CCMAIL:kde-i18n-doc@kde.org

2006-06-10 16:54 +0000 [r550017]  aacid

	* branches/KDE/3.5/kdeedu/kgeography/data/flags/liechtenstein.png
	  (added), branches/KDE/3.5/kdeedu/kgeography/data/world.kgm,
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/Makefile.am,
	  branches/KDE/3.5/kdeedu/kgeography/src/mapsdatatranslation.cpp:
	  World map has a liechtenstein pixel but was not present in the
	  kgm so it gave the user an error Thanks Greg for reporting

2006-06-21 05:37 +0000 [r553460]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/tools/altvstime.cpp: Fixing
	  bug: Plotting an object in the AltVsTime tool temporarily messed
	  up its Alt/Az coordinates in the map. Will forward-port to trunk.
	  CCMAIL: kstars-devel@kde.org

2006-06-21 05:48 +0000 [r553461]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/tools/altvstime.cpp: Make
	  previous bugfix (AltVsTime messed up Alt/Az coordinates) apply to
	  all objects, not just solar system bodies.

2006-06-23 21:01 +0000 [r554362]  hedlund

	* branches/KDE/3.5/kdeedu/kwordquiz/src/kwordquiz.cpp: Don't crash
	  when closing a file during a quiz. BUG:129697

2006-06-23 21:47 +0000 [r554370]  pino

	* branches/KDE/3.5/kdeedu/kig/modes/popup.cc,
	  branches/KDE/3.5/kdeedu/kig/scripting/script_mode.cc,
	  branches/KDE/3.5/kdeedu/kig/scripting/script_mode.h,
	  branches/KDE/3.5/kdeedu/kig/misc/guiaction.cc,
	  branches/KDE/3.5/kdeedu/kig/scripting/newscriptwizard.cc,
	  branches/KDE/3.5/kdeedu/kig/scripting/newscriptwizard.h: New
	  feature: possibility to edit a Python script once constructed.
	  Approved by Stephan Kulow.

2006-06-23 22:06 +0000 [r554381]  pino

	* branches/KDE/3.5/kdeedu/kig/configure.in.in: bump version to
	  0.10.6

2006-06-27 05:34 +0000 [r555351]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/skymapdraw.cpp: Fixing bug
	  that caused the coordinate grid circles to be composed of way too
	  many points.

2006-06-27 05:38 +0000 [r555352]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/skymap.cpp: Bugfix: Don't
	  fade the transient label if the object becomes the focused
	  object.

2006-07-01 07:42 +0000 [r556679]  binner

	* branches/KDE/3.5/kdeedu/kig/misc/kigpainter.h: fix duplicate
	  parameter name

2006-07-02 11:36 +0000 [r557090]  lueck

	* branches/KDE/3.5/kdeedu/doc/kvoctrain/verb-query-dlg.png: fixed
	  wrong screenshot

2006-07-08 09:39 +0000 [r559746]  pino

	* branches/KDE/3.5/kdeedu/kig/TODO: scripting editing is done

2006-07-09 11:59 +0000 [r560145]  lueck

	* branches/KDE/3.5/kdeedu/doc/klettres/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/kvoctrain/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/khangman/index.docbook: documentation
	  backport from trunk CCMAIL:kde-doc-english

2006-07-12 20:18 +0000 [r561644]  aacid

	* branches/KDE/3.5/kdeedu/klettres/klettres/klettres.cpp,
	  branches/KDE/3.5/kdeedu/klettres/klettres/data/langs/de.txt
	  (added),
	  branches/KDE/3.5/kdeedu/klettres/klettres/data/langs/Makefile.am:
	  Add german support The paste special char adds instead of setting
	  the text FEATURE: 130629 CCMAIL: lueck@hube-lueck.de CCMAIL:
	  annma@kde.org

2006-07-12 20:32 +0000 [r561651]  aacid

	* branches/KDE/3.5/kdeedu/klettres/klettres/version.h,
	  branches/KDE/3.5/kdeedu/klettres/klettres/main.cpp: update
	  version now because i'm sure i won't remeber the release day

2006-07-14 16:31 +0000 [r562385]  pino

	* branches/KDE/3.5/kdeedu/kalzium/src/detailinfodlg.cpp: Set the
	  Whats This when updating the dialog, not when creating the
	  dialog. BUG: 130753

2006-07-19 09:53 +0000 [r564111]  mueller

	* branches/KDE/3.5/kdeedu/kstars/kstars/indi/fli/libfli-parport.c:
	  HZ actually doesn't exist anymore with kernel 2.6. I'm not sure
	  what to do with this, because the kernel module only exists for
	  linux 2.4 anyway. So just keep smiling and make it compile..

2006-07-21 17:28 +0000 [r564905]  amantia

	* branches/KDE/3.5/kdeedu/kstars/kstars/indi/fli/libfli-parport.c:
	  Fix compilation.

2006-07-22 14:24 +0000 [r565142]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/main.cpp: Update version
	  string

2006-07-22 18:38 +0000 [r565220]  aacid

	* branches/KDE/3.5/kdeedu/kgeography/src/main.cpp: version++

2006-07-23 14:02 +0000 [r565469]  coolo

	* branches/KDE/3.5/kdeedu/kdeedu.lsm: preparing KDE 3.5.4

