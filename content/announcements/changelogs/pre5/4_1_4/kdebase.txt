------------------------------------------------------------------------
r877950 | woebbe | 2008-10-30 19:08:14 +0000 (Thu, 30 Oct 2008) | 3 lines

set application icon so that sub windows like "KRunner Settings" dialog have an icon.

this make the window icon in Interface superfluous.
------------------------------------------------------------------------
r878037 | scripty | 2008-10-31 07:17:36 +0000 (Fri, 31 Oct 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r878485 | woebbe | 2008-11-01 12:00:07 +0000 (Sat, 01 Nov 2008) | 1 line

also an icon for the configure dialog (and its windows) is nice
------------------------------------------------------------------------
r878487 | woebbe | 2008-11-01 12:02:36 +0000 (Sat, 01 Nov 2008) | 1 line

draw focus rect for old style listviews (e.g. in KMail)
------------------------------------------------------------------------
r878674 | woebbe | 2008-11-01 16:06:04 +0000 (Sat, 01 Nov 2008) | 1 line

OK, draw focus rect ONLY for Q3ListView
------------------------------------------------------------------------
r878695 | pino | 2008-11-01 16:52:12 +0000 (Sat, 01 Nov 2008) | 3 lines

backport:
fix a typo, making the SSE3 capability outputted in the correct case (and not being mistaken as SSE2)

------------------------------------------------------------------------
r878756 | woebbe | 2008-11-01 18:51:42 +0000 (Sat, 01 Nov 2008) | 1 line

third try, widget can be 0
------------------------------------------------------------------------
r878776 | woebbe | 2008-11-01 19:20:52 +0000 (Sat, 01 Nov 2008) | 1 line

better solution, don't change configModules()
------------------------------------------------------------------------
r878939 | scripty | 2008-11-02 07:27:43 +0000 (Sun, 02 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r879159 | alexmerry | 2008-11-02 14:56:40 +0000 (Sun, 02 Nov 2008) | 7 lines

Fix Amarok support.  The old Amarok D-Bus interface is no longer supported by Amarok, but org.kde.Amarok still exists when Amarok is running, confusing the engine and making the applet ignore the working org.mpris.amarok interface.

Unfortunately missed 4.1.3 by two days.  C'est la vie.

BUG: 174044


------------------------------------------------------------------------
r879383 | scripty | 2008-11-03 07:25:27 +0000 (Mon, 03 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r879707 | lunakl | 2008-11-03 21:09:24 +0000 (Mon, 03 Nov 2008) | 5 lines

r873188
Backport of r835483 redraw buttons when redrawing the whole decoration
(BUG: 167886)


------------------------------------------------------------------------
r879750 | gladhorn | 2008-11-03 22:49:40 +0000 (Mon, 03 Nov 2008) | 2 lines

add kvtml icons for 4.1. nini.

------------------------------------------------------------------------
r880331 | scripty | 2008-11-05 08:11:02 +0000 (Wed, 05 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r880850 | lunakl | 2008-11-06 17:11:47 +0000 (Thu, 06 Nov 2008) | 7 lines

Use the '_k_session' shortcuts feature for the per-window shortcuts,
as those are definitely not to be handled by kdedglobalaccel besides
delivering the event, only KWin can keep track of those. Also add
a kconf_update script to dump all the possibly previously created
shortcuts that'd block actually using them.


------------------------------------------------------------------------
r880871 | lunakl | 2008-11-06 18:18:05 +0000 (Thu, 06 Nov 2008) | 4 lines

Use the '_k_session:' shortcut trick to prevent kdedglobalaccel
from doing more than just delivering events for shortcuts.


------------------------------------------------------------------------
r880872 | lunakl | 2008-11-06 18:18:41 +0000 (Thu, 06 Nov 2008) | 4 lines

The KDE4.1 version takes org.kde.khotkeys/KHotKeys on DBUS, so use
it consistently.


------------------------------------------------------------------------
r881004 | scripty | 2008-11-07 07:35:59 +0000 (Fri, 07 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r881249 | lunakl | 2008-11-07 15:11:42 +0000 (Fri, 07 Nov 2008) | 3 lines

Avoid a deadlock during first kde start.


------------------------------------------------------------------------
r881256 | lunakl | 2008-11-07 15:44:56 +0000 (Fri, 07 Nov 2008) | 3 lines

Fix porting error.


------------------------------------------------------------------------
r881353 | dfaure | 2008-11-07 21:03:48 +0000 (Fri, 07 Nov 2008) | 2 lines

backport 881350: remove useless configuration widgets for tooltips in konq

------------------------------------------------------------------------
r881363 | huftis | 2008-11-07 21:21:56 +0000 (Fri, 07 Nov 2008) | 1 line

Fixed locale-specific formatting for Norway (Norwegian Nynorsk and Norwegian Bokmål).
------------------------------------------------------------------------
r881465 | bando | 2008-11-08 10:03:48 +0000 (Sat, 08 Nov 2008) | 1 line

Fix default date formats
------------------------------------------------------------------------
r881981 | huftis | 2008-11-09 13:54:45 +0000 (Sun, 09 Nov 2008) | 1 line

Added default DateFormat for Norway.
------------------------------------------------------------------------
r882038 | jferrer | 2008-11-09 16:27:37 +0000 (Sun, 09 Nov 2008) | 2 lines

Added catalan default date format

------------------------------------------------------------------------
r882240 | scripty | 2008-11-10 07:34:56 +0000 (Mon, 10 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r882293 | mkretz | 2008-11-10 10:14:17 +0000 (Mon, 10 Nov 2008) | 11 lines

backport r880932:

This was quite a porting mess. The old code would fill up qt/4.4/libraryPath in Trolltech.conf with all kinds of paths, most of them duplicates. This hurt startup time and made applications load plugins from incorrect plugin dirs.

I've rewritten the code to do what it was intended to do:
- look at libraryPath
- remove all entries that KDE added last time
- add all the paths KStandardDirs returns for the qtplugins resource
- store what we added to the list
- store the new list

------------------------------------------------------------------------
r882355 | dfaure | 2008-11-10 15:48:03 +0000 (Mon, 10 Nov 2008) | 2 lines

Fix kioexec not asking to upload anymore, after making changes (the eventloop of the messagebox exited immediately...)

------------------------------------------------------------------------
r882363 | mkretz | 2008-11-10 16:24:02 +0000 (Mon, 10 Nov 2008) | 1 line

add kconf_update application to do a one-time cleanup of the libraryPath in Trolltech.conf
------------------------------------------------------------------------
r882881 | lunakl | 2008-11-11 15:07:14 +0000 (Tue, 11 Nov 2008) | 7 lines

Make sure the GLX context is destroyed before the window it's been
made current with. Even though it should be allowed to do it afterwards
it seems this can crash AIGLX.
http://lists.freedesktop.org/archives/xorg/2008-November/040168.html
BUG: 174782


------------------------------------------------------------------------
r882916 | lunakl | 2008-11-11 16:31:38 +0000 (Tue, 11 Nov 2008) | 4 lines

Mark hide() explicitly as a slot, since it's not virtual with Qt4.
Patch by "qiang zhang" <liubingzhq@gmail.com>.


------------------------------------------------------------------------
r883104 | scripty | 2008-11-12 08:13:46 +0000 (Wed, 12 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r884073 | scripty | 2008-11-14 07:29:22 +0000 (Fri, 14 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r884300 | sengels | 2008-11-14 15:48:44 +0000 (Fri, 14 Nov 2008) | 2 lines

add desktop kioslave
needed for plasma - backport r838873
------------------------------------------------------------------------
r885597 | bando | 2008-11-17 14:09:26 +0000 (Mon, 17 Nov 2008) | 1 line

Set default WeekDayOfPray to None
------------------------------------------------------------------------
r887116 | scripty | 2008-11-21 07:28:35 +0000 (Fri, 21 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r888693 | scripty | 2008-11-25 07:41:20 +0000 (Tue, 25 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r889150 | scripty | 2008-11-26 08:14:36 +0000 (Wed, 26 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r889439 | craig | 2008-11-26 20:51:46 +0000 (Wed, 26 Nov 2008) | 3 lines

Port fix from KTorrent (http://bugs.kde.org/show_bug.cgi?id=172198)
CCBUG:174243

------------------------------------------------------------------------
r889778 | dfaure | 2008-11-27 15:47:07 +0000 (Thu, 27 Nov 2008) | 3 lines

Too many bug reports with tons of useless (no debugging symbols found) lines...
 -> backporting -c829990 -c830011 -c840692 from trunk - all the fixes by Lubos.

------------------------------------------------------------------------
r890768 | scripty | 2008-11-30 07:16:40 +0000 (Sun, 30 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r890935 | orlovich | 2008-11-30 17:30:42 +0000 (Sun, 30 Nov 2008) | 6 lines

Fix a reference-counting bug, which could sometime cause plugins to 
stop working (or blow up)  in some scenarios involving tabs. Thanks
to Sebastian Sauer for pointing out it exists.
(No, the call won't crash if _loader is 0, as the method only accesses 
 statics)

------------------------------------------------------------------------
r891080 | scripty | 2008-12-01 07:58:08 +0000 (Mon, 01 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r891882 | scripty | 2008-12-03 08:21:45 +0000 (Wed, 03 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r891931 | ilic | 2008-12-03 10:21:16 +0000 (Wed, 03 Dec 2008) | 1 line

Seconds separator to dot. (bport: 891930)
------------------------------------------------------------------------
r892345 | scripty | 2008-12-04 07:25:03 +0000 (Thu, 04 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r892604 | terwou | 2008-12-04 18:19:42 +0000 (Thu, 04 Dec 2008) | 3 lines

backport r892601
Fix load plugins on demand

------------------------------------------------------------------------
r892739 | scripty | 2008-12-05 07:37:48 +0000 (Fri, 05 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r893179 | scripty | 2008-12-06 08:25:28 +0000 (Sat, 06 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r893661 | scripty | 2008-12-07 07:51:23 +0000 (Sun, 07 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r894203 | scripty | 2008-12-08 07:39:55 +0000 (Mon, 08 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r894623 | dfaure | 2008-12-08 23:28:17 +0000 (Mon, 08 Dec 2008) | 2 lines

Backport 830014 too, in order to fix wrongly added i18n string.

------------------------------------------------------------------------
r895611 | scripty | 2008-12-11 08:07:16 +0000 (Thu, 11 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r896304 | scripty | 2008-12-13 07:44:33 +0000 (Sat, 13 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r897397 | dfaure | 2008-12-15 23:30:42 +0000 (Mon, 15 Dec 2008) | 4 lines

Fix startup warning
   kwin(7438): Couldn't start knotify from knotify4.desktop:  "KDEInit could not launch '/d/kde/inst/kde-trunk/bin/knotify4'."
 Reason: knotify4 registers to DBUS as org.kde.knotify4, not org.kde.knotify.

------------------------------------------------------------------------
r897472 | scripty | 2008-12-16 07:24:21 +0000 (Tue, 16 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r897799 | dfaure | 2008-12-16 20:46:44 +0000 (Tue, 16 Dec 2008) | 2 lines

Backport: Fix duplicated entries in the recent folder list of the Copy To / Move To submenus, was due to /tmp != //tmp. BUG 177858.

------------------------------------------------------------------------
r898019 | dfaure | 2008-12-17 12:18:51 +0000 (Wed, 17 Dec 2008) | 2 lines

Backport fix for kded4 startup assert

------------------------------------------------------------------------
r898088 | woebbe | 2008-12-17 13:05:28 +0000 (Wed, 17 Dec 2008) | 1 line

use existing icon
------------------------------------------------------------------------
r898397 | scripty | 2008-12-18 07:25:06 +0000 (Thu, 18 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r899045 | dfaure | 2008-12-19 16:46:25 +0000 (Fri, 19 Dec 2008) | 3 lines

Backport fix for launching kfind on remote directories (r897114)
BUG: 169473

------------------------------------------------------------------------
r899546 | scripty | 2008-12-21 07:52:55 +0000 (Sun, 21 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r900427 | dfaure | 2008-12-22 20:45:59 +0000 (Mon, 22 Dec 2008) | 3 lines

Backport crashfix for 168934, will be in kde-4.1.4
CCBUG: 168934

------------------------------------------------------------------------
r901203 | dfaure | 2008-12-24 16:40:50 +0000 (Wed, 24 Dec 2008) | 2 lines

Backport fix for 178562 (\n in mimetype names, nasty)

------------------------------------------------------------------------
r904056 | aacid | 2009-01-01 12:05:10 +0000 (Thu, 01 Jan 2009) | 2 lines

Slovakia 2009 -> €

------------------------------------------------------------------------
r904386 | scripty | 2009-01-02 07:48:29 +0000 (Fri, 02 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r905309 | scripty | 2009-01-04 07:54:13 +0000 (Sun, 04 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r906142 | alake | 2009-01-05 16:40:24 +0000 (Mon, 05 Jan 2009) | 3 lines

backport 8990778 to fix bug that does not correctly paint 
transparency of "picture" element.

------------------------------------------------------------------------
r906305 | habacker | 2009-01-05 22:12:00 +0000 (Mon, 05 Jan 2009) | 4 lines

backported 905841
- fixed application icon path
- removed obsolate win32 kdeinit module hack, thanks to Alex

------------------------------------------------------------------------
r906307 | habacker | 2009-01-05 22:16:17 +0000 (Mon, 05 Jan 2009) | 1 line

backported win32 compile fix from trunk
------------------------------------------------------------------------
r906309 | habacker | 2009-01-05 22:21:55 +0000 (Mon, 05 Jan 2009) | 1 line

backported from trunk 905836
------------------------------------------------------------------------
r906316 | habacker | 2009-01-05 22:34:20 +0000 (Mon, 05 Jan 2009) | 1 line

backported from trunk 883380
------------------------------------------------------------------------
r906401 | scripty | 2009-01-06 07:21:57 +0000 (Tue, 06 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
