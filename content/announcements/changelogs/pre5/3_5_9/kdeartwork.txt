2007-10-31 15:53 +0000 [r731393]  lunakl

	* branches/KDE/3.5/kdeartwork/kscreensaver/kdesavers/Flux.cpp:
	  Handle properly display height/width ratio. Patch by Mike
	  Mulvanny <mike.mulvanny@gmail.com> BUG:76204

2007-10-31 16:00 +0000 [r731396]  ossi

	* branches/KDE/3.5/kdeartwork/kscreensaver/kdesavers/slideshow.cpp:
	  be more random

2007-11-22 15:56 +0000 [r740127]  lunakl

	* branches/KDE/3.5/kdeartwork/kwin-styles/smooth-blend/client/Makefile.am:
	  Don't specify direct path to a library. BUG: 143280

2007-11-22 17:29 +0000 [r740156]  mueller

	* branches/KDE/3.5/kdeartwork/kscreensaver/kdesavers/firesaver.cpp:
	  the usual daily unbreak compilation

2008-01-31 10:49 +0000 [r769056]  coolo

	* branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/glcells.desktop
	  (added),
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/moebiusgears.desktop
	  (added),
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/lockward.desktop
	  (added),
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/cwaves.desktop
	  (added),
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/m6502.desktop
	  (added),
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/glschool.desktop
	  (added),
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/voronoi.desktop
	  (added),
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/abstractile.desktop
	  (added),
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/topblock.desktop
	  (added): new xscreensavers

2008-01-31 11:04 +0000 [r769060]  coolo

	* branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/cwaves.desktop,
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/m6502.desktop,
	  branches/KDE/3.5/kdeartwork/kscreensaver/kxsconfig/ScreenSavers/abstractile.desktop:
	  give it a category

2008-02-13 09:52 +0000 [r774464]  coolo

	* branches/KDE/3.5/kdeartwork/kdeartwork.lsm: 3.5.9

