2008-03-07 17:00 +0000 [r783285]  gladhorn

	* branches/KDE/3.5/kdenetwork/knewsticker/common/configaccess.cpp:
	  Fix url to radio tux

2008-04-08 09:16 +0000 [r794662]  gianni

	* branches/KDE/3.5/kdenetwork/krfb/libvncserver/rfb.h: fix
	  compiling on Slackware 12.1

2008-05-28 21:08 +0000 [r813871]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/ssiparamstask.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/blmlimitstask.cpp:
	  SVN commit 813801 by rjarosz: Backport commit 813783. Fix bug
	  162479: ICQ server send "Invalid SNAC header" error. Fix bug
	  154280: ICQ contact looses authorization after moving to another
	  group. CCBUG: 162479 CCBUG: 154280

2008-06-06 08:58 +0000 [r817539]  mueller

	* branches/KDE/3.5/kdenetwork/krdc/vnc/kvncview.cpp: fix locking..
	  patch by Andreas Schwab schwab at suse.de

2008-06-29 22:52 +0000 [r826181]  anagl

	* branches/KDE/3.5/kdenetwork/wifi/kwireless/kwireless.desktop,
	  branches/KDE/3.5/kdenetwork/lanbrowsing/kcmlisa/kcmkiolan.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/kopete_yahoo.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/gadu/kopete_gadu.desktop,
	  branches/KDE/3.5/kdenetwork/ktalkd/kcmktalkd/kcmktalkd.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/translator/kopete_translator_config.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/alias/kopete_alias_config.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/autoreplace/kopete_autoreplace.desktop,
	  branches/KDE/3.5/kdenetwork/knewsticker/kntsrcfilepropsdlg/kntsrcfilepropsdlg.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteui.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/jabber/kopete_jabber.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/alias/kopete_alias.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/translator/kopete_translator.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/netmeeting/kopete_netmeeting_config.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/config/identity/kopete_identityconfig.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/aim/kopete_aim.desktop,
	  branches/KDE/3.5/kdenetwork/lanbrowsing/kcmlisa/kcmreslisa.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/history/kopete_history_config.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/config/kopete_msn_config.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/addbookmarks/kopete_addbookmarks_config.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/netmeeting/kopete_netmeeting.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/config/behavior/kopete_behaviorconfig.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteprotocol.desktop,
	  branches/KDE/3.5/kdenetwork/wifi/kcmwifi/kcmwifi.desktop,
	  branches/KDE/3.5/kdenetwork/krfb/kcm_krfb/kcmkrfb.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/history/kopete_history.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/addbookmarks/kopete_addbookmarks.desktop,
	  branches/KDE/3.5/kdenetwork/knewsticker/knewsticker.desktop,
	  branches/KDE/3.5/kdenetwork/kppp/Kppp.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/connectionstatus/kopete_connectionstatus.desktop,
	  branches/KDE/3.5/kdenetwork/kget/x-kgetlist.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/motionautoaway/kopete_motionaway.desktop,
	  branches/KDE/3.5/kdenetwork/filesharing/simple/fileshare.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/contactnotes/kopete_contactnotes.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/winpopup/kopete_wp.desktop,
	  branches/KDE/3.5/kdenetwork/lanbrowsing/kcmlisa/kcmlisa.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/config/accounts/kopete_accountconfig.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/chatwindow/chatwindow.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/meanwhile/kopete_meanwhile.desktop,
	  branches/KDE/3.5/kdenetwork/filesharing/advanced/propsdlgplugin/fileshare_propsdlgplugin.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/testbed/kopete_testbed.desktop,
	  branches/KDE/3.5/kdenetwork/krdc/krdc.desktop,
	  branches/KDE/3.5/kdenetwork/lanbrowsing/kio_lan/lan.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/autoreplace/kopete_autoreplace_config.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/nowlistening/kopete_nowlistening.desktop,
	  branches/KDE/3.5/kdenetwork/krfb/kinetd/kinetd.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/webpresence/kopete_webpresence.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/sms/kopete_sms.desktop,
	  branches/KDE/3.5/kdenetwork/ksirc/ksirc.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/kopete_smpppdcs_config.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/cryptography/kopete_cryptography_config.desktop,
	  branches/KDE/3.5/kdenetwork/krfb/krfb/kinetd_krfb.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/config/avdevice/kopete_avdeviceconfig.desktop,
	  branches/KDE/3.5/kdenetwork/krfb/krfb/krfb.desktop,
	  branches/KDE/3.5/kdenetwork/kppp/logview/kppplogview.desktop,
	  branches/KDE/3.5/kdenetwork/krfb/krfb_httpd/kinetd_krfb_httpd.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/kopete_smpppdcs.desktop,
	  branches/KDE/3.5/kdenetwork/wifi/kwifimanager.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/statistics/kopete_statistics.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/chatwindow/emailwindow.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/kopete.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/cryptography/kopete_cryptography.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/texteffect/kopete_texteffect_config.desktop,
	  branches/KDE/3.5/kdenetwork/kpf/kpfapplet.desktop,
	  branches/KDE/3.5/kdenetwork/filesharing/advanced/kcm_sambaconf/kcmsambaconf.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/x-kopete-emoticons.desktop,
	  branches/KDE/3.5/kdenetwork/kget/kget_download.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteplugin.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/kopete_msn.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/config/appearance/kopete_appearanceconfig.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/icq/x-icq.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/texteffect/kopete_texteffect.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/icq/kopete_icq.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/highlight/kopete_highlight_config.desktop,
	  branches/KDE/3.5/kdenetwork/kget/kget.desktop,
	  branches/KDE/3.5/kdenetwork/kdict/applet/kdictapplet.desktop,
	  branches/KDE/3.5/kdenetwork/lanbrowsing/kio_lan/lisa.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/kopete_groupwise.desktop,
	  branches/KDE/3.5/kdenetwork/kdnssd/kdedmodule/dnssdwatcher.desktop,
	  branches/KDE/3.5/kdenetwork/kdict/kdict.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/highlight/kopete_highlight.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/latex/kopete_latex_config.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/nowlistening/kopete_nowlistening_config.desktop,
	  branches/KDE/3.5/kdenetwork/knewsticker/knewstickerstub/knewstickerstub.desktop,
	  branches/KDE/3.5/kdenetwork/knewsticker/knewsticker-standalone.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/webpresence/kopete_webpresence_config.desktop,
	  branches/KDE/3.5/kdenetwork/kget/kget_plug_in/kget_plug_in.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/latex/kopete_latex.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/motionautoaway/kopete_motionaway_config.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/irc/kopete_irc.desktop,
	  branches/KDE/3.5/kdenetwork/kpf/kpfpropertiesdialogplugin.desktop,
	  branches/KDE/3.5/kdenetwork/kdnssd/ioslave/zeroconf.desktop,
	  branches/KDE/3.5/kdenetwork/kfile-plugins/torrent/kfile_torrent.desktop:
	  Desktop validation fixes: remove deprecated entries for Encoding.

2008-07-06 20:22 +0000 [r828859]  mattr

	* branches/KDE/3.5/kdenetwork/kopete/plugins/history/historydialog.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/history/historydialog.h:
	  Fix slowness in history searching for KDE 3.5 Thanks for the
	  patch! Sorry to take so long with it. Looking forward to the one
	  for KDE 4.0 so we can close this for real. CCBUG: 148659

2008-08-11 14:45 +0000 [r845361]  bminisini

	* branches/KDE/3.5/kdenetwork/kpf/src/AppletItem.cpp,
	  branches/KDE/3.5/kdenetwork/kpf/src/Applet.cpp,
	  branches/KDE/3.5/kdenetwork/kpf/src/BandwidthGraph.cpp,
	  branches/KDE/3.5/kdenetwork/kpf/src/AppletItem.h,
	  branches/KDE/3.5/kdenetwork/kpf/src/Applet.h,
	  branches/KDE/3.5/kdenetwork/kpf/src/BandwidthGraph.h: Fix the
	  transparency in the file server applet. Remove the frame border
	  of the file server applet.

2008-08-12 19:57 +0000 [r846026]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/kopetepluginmanager.cpp:
	  Backport commit 837114. Protocols aren't always in Plugins group
	  so we should ignore them. CCBUG: 167113

2008-08-15 18:33 +0000 [r847596]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/oscarversionupdater.cpp:
	  Update ICQ client version.

2008-08-19 19:48 +0000 [r849603]  coolo

	* branches/KDE/3.5/kdenetwork/kdenetwork.lsm: update for 3.5.10

