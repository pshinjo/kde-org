2006-10-24 20:47 +0000 [r598853]  pino

	* branches/KDE/3.5/kdeedu/kig/scripting/script_mode.cc,
	  branches/KDE/3.5/kdeedu/kig/scripting/script-common.cc,
	  branches/KDE/3.5/kdeedu/kig/scripting/script_mode.h,
	  branches/KDE/3.5/kdeedu/kig/scripting/script-common.h: backport
	  from trunk and from branch: use a list instead of a set otherwise
	  the objects order is lost

2006-10-24 20:58 +0000 [r598858]  pino

	* branches/KDE/3.5/kdeedu/kig/filters/cabri-filter.cc: Typo fix.
	  CCBUG: 135291

2006-10-27 18:27 +0000 [r599588]  aacid

	* branches/KDE/3.5/kdeedu/kgeography/data/canada.png: Fix canada
	  map, Nova Scoria and New Brunswick are not separate by water.
	  Thanks Ed for noticing. CCMAIL: edmonty@gmail.com

2006-11-02 10:37 +0000 [r601161]  fabo

	* branches/KDE/3.5/kdeedu/kstars/kstars/data/clines.dat: typo in
	  constellation line definition for Hercules causing the message:
	  kstars: WARNING: No star named alp2Her found. Fixes the
	  definition so the lines reflect the version at:
	  http://en.wikipedia.org/wiki/Hercules_(constellation) Thanks to
	  Jindrich Makovicka

2006-11-07 20:39 +0000 [r603101]  lueck

	* branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/Makefile.am,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/kva_io.cpp,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/kva_header.cpp,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/kv_resource.h,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/kvt-core/kvoctrainexpr.h,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/examples/sample-de.kvtml:
	  Fix of untranslatable strings -> 11 fuzzy and 8 untranslated
	  messages kde-xgettext is buggy, it does not extract messages from
	  #define FOO I18N_NOOP('bar'), so I changed it to static const
	  char *FOO = I18N_NOOP('bar'); thanks to Albert Astals Cid for the
	  hint kanagram, kdegraphics/libkscan and
	  kdebase/kcontrol/kfontinst have the same problem
	  CCMAIL:kde-i18n-doc@kde.org

2006-11-22 13:40 +0000 [r606961]  annma

	* branches/KDE/3.5/kdeedu/ktouch/src/ktouch.kcfg: in ktouch.cpp
	  default scheme is set to 1 which is Classic so let's set it to 1
	  here as well

2006-11-23 00:32 +0000 [r607078]  pino

	* branches/KDE/3.5/kdeedu/kig/filters/native-filter.cc: Do not
	  silently convert to an encoding different than UTF-8 when saving.
	  BUGS: 135564

2006-11-23 08:02 +0000 [r607114]  annma

	* branches/KDE/3.5/kdeedu/ktouch/keyboards/Makefile.am,
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/he.keyboard: fix he
	  keyboard layout I think however I established with cuco that
	  Hebrew does not work well due to the custom widget for typing the
	  letters not being RTL compliant - It'll be fixed for KDE4.
	  BUG=128266

2006-11-23 08:48 +0000 [r607116]  annma

	* branches/KDE/3.5/kdeedu/ktouch/training/hungarian_expert.ktouch.xml:
	  Fix in 3.5.5 branch encoding of this file following instructions
	  given by Egmont Koblinger in the bug report Thanks a lot Egmont
	  for having investigated and providing the fixe CCBUG=135580

2006-11-23 14:39 +0000 [r607179]  bram

	* branches/KDE/3.5/kdeedu/kgeography/src/mapasker.cpp: Show answers
	  in answer dialog translated.

2006-11-23 18:44 +0000 [r607223-607220]  aacid

	* branches/KDE/3.5/kdeedu/kgeography/src/mapasker.cpp: Fix Bram
	  fix, anyway thanks for showing me there was a wrong thing there.
	  CCMAIL: bramschoenmakers@kde.nl

	* branches/KDE/3.5/kdeedu/kgeography/src/mapasker.cpp: remove that
	  ugly temporal include

2006-11-23 18:55 +0000 [r607226]  annma

	* branches/KDE/3.5/kdeedu/ktouch/keyboards/fr.keyboard,
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/Makefile.am,
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/dvorak_fr.keyboard
	  (added),
	  branches/KDE/3.5/kdeedu/ktouch/training/dvorak-fr-1.ktouch.xml
	  (added),
	  branches/KDE/3.5/kdeedu/ktouch/training/dvorak-fr-2.ktouch.xml
	  (added), branches/KDE/3.5/kdeedu/ktouch/training/Makefile.am: add
	  French dvorak support (1 keyboard file and 2 training files)

2006-11-23 20:25 +0000 [r607242]  annma

	* branches/KDE/3.5/kdeedu/kiten/kiten.desktop: Freeze is lifted,
	  can close that one BUG=121580

2006-11-24 18:52 +0000 [r607461]  annma

	* branches/KDE/3.5/kdeedu/ktouch/training/slovenian.ktouch.xml: I
	  have written to the KTouch maintainer and he sent me the original
	  file which has the right encoding CCBUG=135569

2006-11-24 19:03 +0000 [r607464]  annma

	* branches/KDE/3.5/kdeedu/ktouch/training/slovenian.ktouch.xml: add
	  author name

2006-11-25 12:44 +0000 [r607630]  annma

	* branches/KDE/3.5/kdeedu/doc/kvoctrain/index.docbook: typo

2006-11-26 11:58 +0000 [r607938]  pino

	* branches/KDE/3.5/kdeedu/doc/kalzium/index.docbook: Backport typo
	  fix. CCBUG: 137902

2006-11-28 04:16 +0000 [r608615]  hedlund

	* branches/KDE/3.5/kdeedu/kwordquiz/src/kwordquizview.cpp: Apply
	  word wrapping also when printing. Thanks to Corien Bennink for
	  reporting this issue. CCMAIL:highwaykind@gmail.com

2006-11-30 22:03 +0000 [r609467]  hedlund

	* branches/KDE/3.5/kdeedu/kwordquiz/src/wqquiz.cpp,
	  branches/KDE/3.5/kdeedu/kwordquiz/src/wqquiz.h: Make sure a quiz
	  is correctly restarted when several vocabularies are open.
	  BUG:138132

2006-12-01 14:14 +0000 [r609642]  annma

	* branches/KDE/3.5/kdeedu/doc/klettres/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/kanagram/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/blinken/index.docbook: EBN fixes,
	  these were already done in trunk

2006-12-01 16:21 +0000 [r609678]  lueck

	* branches/KDE/3.5/kdeedu/doc/klettres/index.docbook: backport from
	  trunk CCMAIL:annma@kde.org

2006-12-03 00:21 +0000 [r609985]  pino

	* branches/KDE/3.5/kdeedu/kig/filters/latexexporter.cc: Do not
	  export lines of garbage to the LaTeX file.

2006-12-03 12:50 +0000 [r610059]  annma

	* branches/KDE/3.5/kdeedu/khangman/khangman/main.cpp,
	  branches/KDE/3.5/kdeedu/klettres/klettres/main.cpp: fix years
	  thanks to Benoit Jacob add contributor for KLettres en_GB sounds

2006-12-03 13:08 +0000 [r610063]  annma

	* branches/KDE/3.5/kdeedu/klettres/klettres/main.cpp: include all
	  languages and all contributors names

2006-12-04 09:46 +0000 [r610423]  annma

	* branches/KDE/3.5/kdeedu/khangman/khangman/khangman.cpp: fix bug:
	  when language present from l10n it was not checked in GHNS dialog
	  - still something to fix about fr specific - you must delete
	  khangmanrc to reset the date and get it work Thanks to Benoit
	  jacob for reporting

2006-12-06 15:43 +0000 [r611065]  lueck

	* branches/KDE/3.5/kdeedu/doc/kgeography/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/kvoctrain/index.docbook: backport
	  from trunk

2006-12-09 12:10 +0000 [r611775]  pino

	* branches/KDE/3.5/kdeedu/kig/configure.in.in: bump version

2006-12-10 15:46 +0000 [r612238]  annma

	* branches/KDE/3.5/kdeedu/ktouch/training/polish.ktouch.xml:
	  Improved Polish exercise file, thanks to Mikolaj Machowski
	  CCMAIL=mikmach@wp.pl

2006-12-10 17:31 +0000 [r612268]  pino

	* branches/KDE/3.5/kdeedu/kig/filters/latexexporter.cc: Export sane
	  sizes for circles.

2006-12-10 18:33 +0000 [r612274]  pino

	* branches/KDE/3.5/kdeedu/kig/filters/latexexporter.cc: When
	  drawing an ellipse, close its path so it looks like an ellipse.

2006-12-15 12:09 +0000 [r613837]  pino

	* branches/KDE/3.5/kdeedu/khangman/khangman/khangman.desktop,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/kvoctrain.desktop,
	  branches/KDE/3.5/kdeedu/kverbos/kverbos/kverbos.desktop,
	  branches/KDE/3.5/kdeedu/keduca/resources/keduca.desktop,
	  branches/KDE/3.5/kdeedu/klatin/klatin/klatin.desktop,
	  branches/KDE/3.5/kdeedu/klettres/klettres/klettres.desktop,
	  branches/KDE/3.5/kdeedu/kanagram/src/kanagram.desktop,
	  branches/KDE/3.5/kdeedu/keduca/resources/keducabuilder.desktop:
	  Use the proper freedesktop.org categories.

2007-01-04 18:41 +0000 [r619911]  aacid

	* branches/KDE/3.5/kdeedu/kgeography/data/africa.kgm,
	  branches/KDE/3.5/kdeedu/kgeography/data/world.kgm,
	  branches/KDE/3.5/kdeedu/kgeography/src/mapsdatatranslation.cpp:
	  Cape Town is not the only capital of South Africa BUGS: 139554

2007-01-05 00:33 +0000 [r620012]  pino

	* branches/KDE/3.5/kdeedu/kgeography/src/divisionflagasker.cpp: Use
	  the right translated form. BUG: 139616

2007-01-06 15:58 +0000 [r620554]  cies

	* branches/KDE/3.5/kdeedu/kturtle/src/parser.cpp: BUG 115555
	  fixed...

2007-01-06 16:11 +0000 [r620557]  cies

	* branches/KDE/3.5/kdeedu/kturtle/src/parser.cpp: BUG 139496
	  (spelling errors) fixed...

2007-01-07 08:15 +0000 [r620755]  annma

	* branches/KDE/3.5/kdeedu/kstars/kstars/data/Cities.dat: Toulouse
	  is not in Tarn department

2007-01-15 09:15 +0000 [r623692]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdewebdev/VERSION,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/VERSION,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: increase version

