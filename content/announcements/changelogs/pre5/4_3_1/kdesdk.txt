------------------------------------------------------------------------
r1005773 | scripty | 2009-08-02 03:02:04 +0000 (Sun, 02 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1009261 | aacid | 2009-08-09 14:08:59 +0000 (Sun, 09 Aug 2009) | 5 lines

Backport r1009260 | aacid | 2009-08-09 16:07:17 +0200 (Sun, 09 Aug 2009) | 3 lines

use i18n instead of tr, and while at it give a proper context
Problem found by Marta Rybczynska

------------------------------------------------------------------------
r1009995 | mlaurent | 2009-08-11 11:40:26 +0000 (Tue, 11 Aug 2009) | 4 lines

Backport:
Fix enable/disable ok button when we create dialog


------------------------------------------------------------------------
r1009998 | mlaurent | 2009-08-11 12:03:30 +0000 (Tue, 11 Aug 2009) | 4 lines

Backport:
Fix select button


------------------------------------------------------------------------
r1010424 | asserhal | 2009-08-12 12:35:59 +0000 (Wed, 12 Aug 2009) | 1 line

Fixed syntax highlighting for tags that span more than one line
------------------------------------------------------------------------
r1013103 | sars | 2009-08-18 19:55:25 +0000 (Tue, 18 Aug 2009) | 2 lines

Backport fix that ensures that a failed compilation is reported.
Backport the fix that re-enables the "quick compile" to compile the current file as it was supposed to do.
------------------------------------------------------------------------
r1013105 | sars | 2009-08-18 20:01:31 +0000 (Tue, 18 Aug 2009) | 1 line

I forgot the string freeze. Reuse an already translated string.
------------------------------------------------------------------------
r1013163 | scripty | 2009-08-19 03:20:53 +0000 (Wed, 19 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1014756 | binner | 2009-08-23 18:30:46 +0000 (Sun, 23 Aug 2009) | 2 lines

Patch of Raymond Wooninck <tittiatcoke@gmail.com> to fix void-return-in-non-void-function

------------------------------------------------------------------------
r1015179 | chehrlic | 2009-08-24 19:44:52 +0000 (Mon, 24 Aug 2009) | 6 lines

merge r1014826

sync documentModel session config with fileList config to make sure the correct sort order is displayed on right-click in fileList. Could fix bug #181178 although it's described the other way round (displaying was correct, order was wrong).

CCBUG: 181178

------------------------------------------------------------------------
r1015534 | sars | 2009-08-25 19:34:55 +0000 (Tue, 25 Aug 2009) | 4 lines

Back-port the bug fix for bug:205075

The url of the file contained the file name -> tried to compile in a non-existing directory.

------------------------------------------------------------------------
r1015982 | asserhal | 2009-08-26 18:13:45 +0000 (Wed, 26 Aug 2009) | 1 line

Fixes to always show correct values in status bar, only save catalog if changed, and save editor state when no editor tab open
------------------------------------------------------------------------
r1016097 | rzarazua | 2009-08-27 01:12:54 +0000 (Thu, 27 Aug 2009) | 1 line

Backport of r1006550, use iterators correctly to prevent a crash
------------------------------------------------------------------------
r1016107 | rzarazua | 2009-08-27 02:34:40 +0000 (Thu, 27 Aug 2009) | 2 lines

Port over changes in interface & implementation for linking with KDevelop
CCMAIL: kdevelop-devel@barney.cs.uni-potsdam.de
------------------------------------------------------------------------
