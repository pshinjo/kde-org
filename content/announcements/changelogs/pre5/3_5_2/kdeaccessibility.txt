2006-02-01 22:29 +0000 [r504714]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/filters/main.cpp:
	  Potential segfault. Relatively harmless since this is a test
	  program. Also add license info.

2006-02-03 19:20 +0000 [r505374]  jriddell

	* branches/KDE/3.5/kdeaccessibility/debian (removed): Remove debian
	  directory, now at
	  http://svn.debian.org/wsvn/pkg-kde/trunk/packages/kdeaccessibility
	  svn://svn.debian.org/pkg-kde/trunk/packages/kdeaccessibility

2006-02-07 03:15 +0000 [r506574]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/plugins/festivalint/festivalintplugin.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/players/akodeplayer/akodeplugin.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/libkttsd/player.h,
	  branches/KDE/3.5/kdeaccessibility/kttsd/libkttsd/pluginconf.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/plugins/hadifix/hadifixplugin.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/plugins/freetts/freettsplugin.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/plugins/command/commandplugin.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/xmltransformer/xmltransformerplugin.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/players/artsplayer/artsplugin.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/stringreplacerplugin.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/talkerchooser/talkerchooserplugin.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/players/gstplayer/gstplugin.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/ChangeLog,
	  branches/KDE/3.5/kdeaccessibility/kttsd/plugins/flite/fliteplugin.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/sbd/sbdplugin.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/libkttsd/filterconf.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/players/alsaplayer/alsaplugin.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/kcmkttsmgr/kcmkttsmgr.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/kttsjobmgr/kttsjobmgr.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/plugins/epos/eposplugin.cpp:
	  BUG:121427 Translations sometimes not working due to incorrect
	  catalog insertion/removal. Stephan, can you verify this for me
	  please.

2006-02-07 03:24 +0000 [r506575]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/ChangeLog,
	  branches/KDE/3.5/kdeaccessibility/kttsd/kttsmgr/kttsmgr.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/kttsd/main.cpp: Bump
	  version to 0.3.5.2.

2006-02-08 00:16 +0000 [r506962]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/app-plugins/kate/katekttsd.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/ChangeLog: Fix
	  translation of 'Speak Text' in kate/ktextedit. Thanks to Stephan
	  Johach

2006-02-10 00:41 +0000 [r507776]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/ChangeLog,
	  branches/KDE/3.5/kdeaccessibility/kttsd/configure.in.in: Use
	  KDE_CHECK_{HEADER,LIB} instead of AC_CHECK_{HEADER,LIB} to locate
	  alsalib.

2006-02-15 02:27 +0000 [r509539]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/doc/kttsd/index.docbook: Fixes
	  ala English Breakfast Network.

2006-02-15 22:07 +0000 [r509951]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/doc/kttsd/index.docbook: Couple
	  more EBN sanitizer fixes.

2006-02-18 23:19 +0000 [r511133]  annma

	* branches/KDE/3.5/kdeaccessibility/doc/kttsd/index.docbook: fix
	  122243, thanks Stephan for reporting BUG=122243

2006-02-21 14:58 +0000 [r512007]  mueller

	* branches/KDE/3.5/kdeaccessibility/kbstateapplet/kbstate.cpp:
	  fixing various obvious bugs

2006-03-06 12:47 +0000 [r516246]  mlaurent

	* branches/KDE/3.5/kdeaccessibility/kbstateapplet/configure.in.in:
	  Backport

2006-03-17 21:34 +0000 [r519775]  coolo

	* branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm: tagging
	  3.5.2

