---
aliases:
- ../announce-4.6-rc2
date: '2011-01-05'
description: KDE Ships Second Release Candidate of KDE SC 4.6 Series
title: KDE Plasma, Applications and Development Platform 4.6 RC2 Available
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
  KDE Readies 4.6 RC2 for Testing
</h3>

<p align="justify">
  <strong>
KDE Community Ships Second Release Candidate of the 4.6 Free Desktop, Applications and
Development Platform
</strong>
</p>

<p align="justify">
 KDE has made available a second release candidate for the impending new version of its workspace, applications and development platform. The release serves as final test candidate before 4.6.0 is released on January, 26th.
</p>

<div class="text-center">
	<a href="/announcements/4/4.6-rc2/announce-4.6-rc2-dolphin.png">
	<img src="/announcements/4/4.6-rc2/announce-4.6-rc2-dolphin_thumb.png" class="img-fluid" alt="Dolphin File Manager in 4.6 RC2">
	</a> <br/>
	<em>Dolphin File Manager in 4.6 RC2</em>
</div>
<br/>

<p>
To find out more about the KDE Plasma desktop and applications, please also refer to the
<a href="/announcements/4.5/">4.5.0</a>,
<a href="/announcements/4.4/">4.4.0</a>,
<a href="/announcements/4.3/">4.3.0</a>,
<a href="/announcements/4.2/">4.2.0</a>,
<a href="/announcements/4.1/">4.1.0</a> and
<a href="/announcements/4.0/">4.0.0</a> release
notes.
</p>

<p align="justify">
 KDE, including all its libraries and its applications, is available for free
under Open Source licenses. KDE can be obtained in source and various binary
formats from <a
href="http://download.kde.org/unstable/4.5.95/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<h4>
  Installing 4.6 RC2 Binary Packages
</h4>
<p align="justify">
  <em>Packages</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.6 RC2
for some versions of their distribution, and in other cases community volunteers
have done so.
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/unstable/4.5.95/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.5.95">4.6 RC2 Info
Page</a>.
</p>

<h4>
  Compiling 4.6 RC2
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.6 RC2 may be <a
href="http://download.kde.org/unstable/4.5.95/src/">freely downloaded</a>.
Instructions on compiling and installing 4.6 RC2
  are available from the <a href="/info/4.5.95#binary">4.6 RC2 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>


