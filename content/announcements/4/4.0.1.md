---
aliases:
- ../announce-4.0.1
date: '2008-02-05'
description: KDE Community Ships First Maintenance Update for Fourth Major Version
  for Leading Free Software Desktop.
title: KDE 4.0.1 Release Announcement
---

<h3 align="center">
   KDE Project Ships First Translation and Service Release for Leading Free Software
Desktop
</h3>

<p align="justify">
KDE Community Ships First Translation and Service Release of the 4.0
Free Desktop, Containing Numerous Bugfixes, Performance Improvements and
Translation Updates
</p>

<p align="justify">
The <a href="http://www.kde.org/">KDE
Community</a> today announced the immediate availability of KDE 4.0.1, the first
bugfix and maintenance release for the latest generation of the most advanced and powerful
free desktop. KDE 4.0.1 ships with a basic
desktop and many other packages like administration, network, education,
utilities, multimedia, games, artwork, web development and more. KDE's
award-winning tools and applications are available in almost 50 languages.
</p>
<p align="justify">
 KDE, including all its libraries and its applications, is available for free
under Open Source licenses. KDE can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.0.1/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<h4>
  <a id="changes">Enhancements</a>
</h4>
<p align="justify">

KDE 4.0.1 is a maintenance release which provides corrections of problems
reported using the <a href="http://bugs.kde.org/">KDE bug tracking system</a>
and enhanced support for existing and new translations.
<br />
Improvements in this release include, but are not limited to:

</p>
<ul>
    <li>
    Konqueror, KDE's webbrowser has seen numerous stability and performance fixes
    in its HTML rendering engine KHTML, in its Flash plugin loader and in KJS, the 
    JavaScript engine.
    </li>
    <li>
    Stability problems have been addressed in components that are used all over the 
    KDE codebase. Translations in this release are more complete.
    </li>
    <li>
    KWin, the KDE window manager has improved detection of compositing support, some effects
    have been fixed.
    </li>
</ul>

<p align="justify">
Aside from these fundamentals, work has also been done in many applications like
Okular, System Settings and KStars. New translations include Danish, Frisian,
Kazakh and Czech.
 </p>

<p align="justify">
 For a more detailed list of improvements since the KDE 4.0 release in January
2008, please refer to the <a
href="/announcements/changelogs/changelog4_0to4_0_1">KDE
4.0.1 Changelog</a>.
</p>

<p align="justify">
 Additional information about the enhancements of the KDE 4.0.x release series
is available in the <a href="../4.0/">KDE 4.0
Announcement</a>.
</p>
<h4>
  Installing KDE 4.0.1 Binary Packages
</h4>
<p align="justify">
  <em>Packagers</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE 4.0.1
for some versions of their distribution, and in other cases community volunteers
have done so.
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.0.1/">http://
download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.0.1">KDE 4.0.1 Info
Page</a>.
</p>

<h4>
  Compiling KDE 4.0.1
</h4>
<p align="justify">
  <a id="source_code"></a><em>Source Code</em>.
  The complete source code for KDE 4.0.1 may be <a
href="http://download.kde.org/stable/4.0.1/src/">freely downloaded</a>.
Instructions on compiling and installing KDE 4.0.1
  are available from the <a href="/info/4.0.1#binary">KDE 4.0.1 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
project that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>

<p align="justify">
We look forward to hearing from you soon!
</p>

<h4>About KDE 4</h4>
<p>
KDE 4.0 is the innovative Free Software desktop containing lots of applications
for every day use as well as for specific purposes. Plasma is a new desktop
shell developed for
KDE 4, providing an intuitive interface to interact with the desktop and
applications. The Konqueror web browser integrates the web with the desktop. The
Dolphin file manager, the Okular document reader and the System Settings control
center complete the basic desktop set. 
<br />
KDE is built on the KDE Libraries which provide easy access to resources on the
network by means of KIO and advanced visual capabilities through Qt4. Phonon and
Solid, which are also part of the KDE Libraries add a multimedia framework and
better hardware integration to all KDE applications.
</p>


