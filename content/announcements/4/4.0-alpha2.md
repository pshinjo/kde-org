---
aliases:
- ../announce-4.0-alpha2
date: '2007-07-04'
description: KDE Project Ships Second Alpha Release for Leading Free Software Desktop..
title: KDE 4.0-alpha2 Release Announcement
custom_about: true
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
   KDE Project Ships Second Alpha Release for Leading Free
   Software Desktop, Codename "Cernighan"
</h3>

<p align="justify">
  KDE 4.0-alpha2 features considerable enhancements of <a href="http://plasma.kde.org">Plasma</a>,
  the KDE 4 desktop shell.
</p>

<p align="justify">
The KDE Community is happy to announce the immediate availability of the second alpha 
release of the K Desktop Environment. This release comes straight out of Glasgow, 
the largest city in Scotland where <a href="http://akademy.kde.org">aKademy</a> is currently taking place. 
Hundreds of KDE hackers are working like crazy to hunt down bugs, complete features 
for KDE 4.0 and sit together developing and finishing new and exciting applications 
for the new major version of the leading Free Desktop.
<p />


The most exciting new development is currently going on in 
<a href="http://plasma.kde.org">Plasma</a>, KDE 4's new shell for the desktop. 
Plasma provides krunner, an application to directly launch programs and start other 
tasks. Plasmoids are applets that display information such as the time, information about hardware 
devices and also provide access to online resources, for example 
showing RSS feeds, images or providing dictionary lookup.
<p />
System Settings, the replacement for KControl is an improved user interface hosting various modules for 
configuring the desktop and other aspects of the system is another addition worth mentioning.
<p />
The upcoming weeks will be spent on further stabilizing and streamlining the underlying 
KDE libraries to provide a stable interface for programmers for the whole KDE4 lifecycle. 
Furthermore, the focus will shift to finishing the applications that are shipped with 
the base desktop and polishing their user interfaces.
<p />
The end of July will see a full freeze of the kdelibs module, manifested in the first 
beta release of KDE 4.0 with the final release currently planned for the end of October this year.
<p />
For the bravehearts who want to try KDE 4.0 Alpha2, please refer to 
<a href="/info/3.91">this page</a> 
to find ways to have a peak at the current status yourself. As the pace of development 
would outdate screenshots very quickly, the best way to find out about progress is to
refer to sites such as <a href="http://dot.kde.org">The Dot</a> or 
<a href="http://www.planetkde.org">Planet KDE</a>, the collection of KDE developers' weblogs.

<h4>
  Supporting KDE
</h4>
<p align="justify">
KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a> project 
that exists and grows only because of the
help of many volunteers that donate their time and effort. KDE
is always looking for new volunteers and contributions, whether its
help with coding, bug fixing or reporting, writing documentation,
translations, promotion, money, etc. All contributions are gratefully
appreciated and eagerly accepted. Please read through the <a href="/community/donations/">Supporting
KDE page</a> for further information. </p>

<p align="justify">
We look forward to hearing from you soon!
</p>

<h4>
  About KDE
</h4>
<p align="justify">
  KDE is an <a href="/community/awards/">award-winning</a>, independent <a href="/people/">project of hundreds</a>
  of developers, translators, artists and other professionals worldwide collaborating over the Internet
  to create and freely distribute a sophisticated, customizable and stable
  desktop and office environment employing a flexible, component-based,
  network-transparent architecture and offering an outstanding development
  platform.</p>

<p align="justify">
  The KDE 3 branch continues to provide a stable, mature desktop including a state-of-the-art browser
  (<a href="http://konqueror.kde.org/">Konqueror</a>), a personal information
  management suite (<a href="http://kontact.org/">Kontact</a>), a full 
  office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
  set of networking application and utilities, and an
  efficient, intuitive development environment featuring the excellent IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.</p>

<p align="justify">
  KDE is working proof
  that the Open Source "Bazaar-style" software development model can yield
  first-rate technologies on par with and superior to even the most complex
  commercial software.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Trademark Notices.</em>
  KDE<sup>&#174;</sup> and the K Desktop Environment<sup>&#174;</sup> logo are 
  registered trademarks of KDE e.V.

  Linux is a registered trademark of Linus Torvalds.

  UNIX is a registered trademark of The Open Group in the United States and
  other countries.

  All other trademarks and copyrights referred to in this announcement are
  the property of their respective owners.
  </font>
</p>

<hr />
