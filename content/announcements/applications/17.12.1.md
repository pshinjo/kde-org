---
aliases:
- ../announce-applications-17.12.1
date: 2018-01-11
description: KDE Ships KDE Applications 17.12.1
changelog: true
layout: application
title: KDE Ships KDE Applications 17.12.1
version: 17.12.1
---

{{% i18n_var "January 11, 2018. Today KDE released the first stability update for <a href='%[1]s'>KDE Applications 17.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../17.12.0" %}}

About 20 recorded bugfixes include improvements to Kontact, Dolphin, Filelight, Gwenview, KGet, Okteta, Umbrello, among others.

Improvements include:

- Sending mails in Kontact has been fixed for certain SMTP servers
- Gwenview's timeline and tag searches have been improved
- JAVA import has been fixed in Umbrello UML diagram tool
