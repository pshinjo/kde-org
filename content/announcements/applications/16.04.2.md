---
aliases:
- ../announce-applications-16.04.2
changelog: true
date: 2016-06-14
description: KDE Ships KDE Applications 16.04.2
layout: application
title: KDE Ships KDE Applications 16.04.2
version: 16.04.2
---

{{% i18n_var "June 14, 2016. Today KDE released the second stability update for <a href='%[1]s'>KDE Applications 16.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../16.04.0" %}}

More than 25 recorded bugfixes include improvements to akonadi, ark, artikulate, dolphin. kdenlive, kdepim, among others.

{{% i18n_var "This release also includes Long Term Support version of KDE Development Platform %[1]s." "4.14.21" %}}