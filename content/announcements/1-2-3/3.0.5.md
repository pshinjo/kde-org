---
aliases:
- ../announce-3.0.5
custom_about: true
custom_contact: true
date: '2002-11-18'
description: KDE 3.0.5 is a security update over KDE 3.0.4.
title: KDE 3.0.5 Release Announcement
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
   KDE Project Ships Enhanced Service, Security Release for Third
   Generation of Leading Open Source Desktop
</h3>

<p align="justify">
  <strong>
    The KDE Project Ships Enhanced Service, Security Release for the
    Leading Linux/UNIX Desktop, Offering Enterprises, Governments, Schools
    and Others an Outstanding Free and Open Desktop Solution
  </strong>
</p>
<p align="justify">
  The <a href="http://www.kde.org/">KDE
  Project</a> today announced the immediate availability of KDE 3.0.5,
  the third generation of KDE's <em>free</em>, powerful desktop for Linux
  and other UNIXes.  KDE 3.0.5 is available in
  <a href="http://i18n.kde.org/teams/index.php">51</a> languages -
  including the addition of Basque for the first time in KDE 3 - and ships
  with the core KDE libraries, the base desktop environment,
  an <a href="http://www.kdevelop.org/">integrated development environment</a>, 
  and hundreds of applications and other desktop enhancements
  from the other KDE base packages (PIM, administration, network,
  edutainment, development, utilities, multimedia, games, artwork, and
  others).
</p>
<p align="justify">
  KDE 3.0.5 provides mainly two important security fixes over KDE 3.0.4, 
  which shipped early October 2002. Besides that, some minor bugfixes
  have been included as well. 

  For a list of some changes since KDE 3.0.4, please see the
  <a href="/announcements/changelogs/changelog3_0_4to3_0_5">change
  log</a>, and for additional information about the security corrections
  please see the separate security advisories
  ( 
   <a href="http://www.kde.org/info/security/advisory-20021111-1.txt">resLISa / LISa</a>;
   <a href="http://www.kde.org/info/security/advisory-20021111-2.txt">rlogin protocol</a>
  ).
</p>
<p align="justify">
  KDE, including all its libraries and its applications, is
  available <em><strong>for free</strong></em> under Open Source licenses.
  KDE can be obtained in source and numerous binary formats from the KDE
  <a href="http://download.kde.org/stable/3.0.5/">http</a> or
  <a href="http://www.kde.org/ftpmirrors.html">ftp</a> mirrors,
  and can also be obtained on
  <a href="http://www.kde.org/cdrom.html">CD-ROM</a> or with any
  of the major Linux/UNIX systems shipping today.
</p>
<p align="justify">
  For more information about the KDE 3 series, please read the
  <a href="/announcements/announce-3.0">KDE 3.0
  announcement</a> and the <a href="/info/3.0.5">KDE
  3.0.5 information page</a>.
</p>

<a id="binary"></a>
<h4>
  Installing KDE 3.0.5 Binary Packages
</h4>
<p align="justify">
  <u>Binary Packages</u>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of
  KDE 3.0.5 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  <a href="http://download.kde.org/stable/3.0.5/">http</a> or 
  <a href="http://www.kde.org/ftpmirrors.html">ftp</a> mirrors.
  Additional binary packages, as well as updates to the packages now
  available, may become available via the KDE servers over the coming
  weeks.
</p>
<p align="justify">
  Please note that the KDE Project makes these packages available from the
  KDE web site as a convenience to KDE users.  The KDE Project is not
  responsible for these packages as they are provided by third
  parties - typically, but not always, the distributor of the relevant
  distribution - using tools, compilers, library versions and quality
  assurance procedures over which KDE exercises no control.  In addition,
  please note that some vendors provide binary packages solely through
  their servers.  If you cannot find a binary package for your OS, or you
  are displeased with the quality of binary packages available for your OS,
  please read the <a href="http://www.kde.org/packagepolicy.html">KDE
  Binary Package Policy</a> and/or contact your OS vendor.
</p>
<p align="justify">
  <u>Library Requirements / Options</u>.
  The library requirements for a particular binary package vary with the
  system on which the package was compiled.  Please bear in mind that
  some binary packages may require a newer version of Qt and other libraries
  than was shipped with the system (e.g., LinuxDistro X.Y
  may have shipped with Qt-3.0.0 but the packages below may require
  Qt-3.0.3).  For general library requirements for KDE, please see the text at
  <a href="#source_code-library_requirements">Source Code - Library
  Requirements</a> below.
</p>
<p align="justify">
  <a id="package_locations"><u>Package Locations</u></a>.
  At the time of this release, pre-compiled packages are available through the
  KDE servers (or elsewhere, where indicated) for the following distributions
  and versions (please be sure to
  read any applicable <code>README</code> first):
</p>

<p align="justify">
  Binary packages contributed by users rather than the relevant OS vendor
  are located in the
  <a href="http://download.kde.org/stable/kde-3.0.5/contrib/">contrib</a>
  directory.
</p>
<p align="justify">
  Additional binary packages will likely become available over the coming
  days and weeks; please check the
  <a href="/info/3.0.5">KDE 3.0.5 Info Page</a> for
  updates.
</p>
<h4>
  Compiling KDE 3.0.5
</h4>
<p align="justify">
  <a id="source_code-library_requirements"></a><u>Library
  Requirements / Options</u>.
  KDE 3.0.5 requires
  <a href="http://www.trolltech.com/developer/download/qt-x11.html">Qt
  3.0.3 for X11</a>.
  In addition, KDE can take advantage of a number of other
  <a href="../3.0/#source_code-library_requirements">optional
  libraries and applications</a> to improve your desktop experience and
  capabilities.
</p>
<p align="justify">
  <u>Compiler Requirements</u>.
  KDE is designed to be cross-platform and hence to compile with a large
  variety of Linux/UNIX compilers.  However, KDE is advancing very rapidly
  and the ability of native compilers on various UNIX systems to compile
  KDE depends on users of those systems
  <a href="http://bugs.kde.org/">reporting</a> compile problems to
  the responsible developers.
</p>
<p align="justify">
  With respect to the most popular KDE compiler,
  <a href="http://gcc.gnu.org/">gcc/egcs</a>, please note that some
  components of KDE will not compile properly with gcc versions earlier
  than gcc-2.95, such as egcs-1.1.2 or gcc-2.7.2, or with unpatched
  versions of <a href="http://gcc.gnu.org/gcc-3.0/gcc-3.0.html">gcc</a> 3.0.x.
  However, KDE should compile properly with gcc 3.1 and 3.2.
</p>
<p align="justify">
  <a id="source_code"></a><u>Source Code / SRPMs</u>.
  The complete source code for KDE 3.0.5 is available for
  <a href="http://download.kde.org/stable/3.0.5/src/">download</a>.
  These source packages have been digitally signed with
  <a href="http://www.gnupg.org/">GnuPG</a> using the KDE Archives PGP Key
  (available from the
  <a href="http://www.kde.org/signature.html">KDE Signature page</a>
  or public key servers), and their respective MD5 sums are listed on the
  <a href="/info/3.0.5">KDE 3.0.5 Info Page</a>.
</p>
<p align="justify">
  Additionally, source rpms are available for 
  <a href="http://download.kde.org/stable/3.0.5/RedHat/8.0/SRPMS/">RedHat</a>,
  <a href="http://download.kde.org/stable/3.0.5/SuSE/SRPMS/">SuSE Linux</a> and
  <a href="http://download.kde.org/stable/3.0.5/Turbo/SRPMS/">Turbo Linux</a>.
</p>
<p align="justify">
  <u>Further Information</u>.  For further
  instructions on compiling and installing KDE 3.0.5, please consult
  the <a href="http://developer.kde.org/build/index.html">installation
  instructions</a> and, if you should encounter compilation difficulties, the
  <a href="http://developer.kde.org/build/compilationfaq.html">KDE
  Compilation FAQ</a>.
</p>

<h4>
  KDE Sponsorship
</h4>
<p align="justify">
  Besides the superb and invaluable efforts by the
  <a href="http://www.kde.org/gallery/index.html">KDE developers</a>
  themselves, significant support for KDE development has been provided by
  <a href="http://www.mandrakesoft.com/">MandrakeSoft</a> and
  <a href="http://www.suse.com/">SuSE</a>.  In addition,
  the members of the <a href="http://www.kdeleague.org/">KDE
  League</a> provide significant support for KDE promotion, and the
  <a href="http://www.uni-tuebingen.de/">University of T&uuml;bingen</a>
  and the <a href="http://www.uni-kl.de/">University of Kaiserslautern</a>
  provide most of the Internet bandwidth for the KDE project.  Thanks!
</p>
<h4>
  About KDE
</h4>
<p align="justify">
  KDE is an independent project of hundreds of developers, translators,
  artists and other professionals worldwide collaborating over the Internet
  to create and freely distribute a sophisticated, customizable and stable
  desktop and office environment employing a flexible, component-based,
  network-transparent architecture and offering an outstanding development
  platform.  KDE provides a stable, mature desktop, a full, component-based
  office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
  set of networking and administration tools and utilities, and an
  efficient, intuitive development environment featuring the excellent IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.  KDE is working proof
  that the Open Source "Bazaar-style" software development model can yield
  first-rate technologies on par with and superior to even the most complex
  commercial software.
</p>
<hr />
<p align="justify">
  <font size="2">
  <em>Trademarks Notices.</em>
  KDE, K Desktop Environment, KDevelop and KOffice are trademarks of KDE e.V.

  Compaq, Alpha and Tru64 are either trademarks and/or service marks or
  registered trademarks and/or service marks of Hewlett-Packard Company.

  IBM and PowerPC are registered trademarks of IBM Corporation.

  Intel is a trademark or registered trademark of Intel Corporation or its
  subsidiaries in the United States and other countries.

  Linux is a registered trademark of Linus Torvalds.

  PostScript is a registered trademark of Adobe Systems Incorporated.

  Trolltech and Qt are trademarks of Trolltech AS.

  UNIX and Motif are registered trademarks of The Open Group.

  All other trademarks and copyrights referred to in this announcement are
  the property of their respective owners.
  </font>
</p>
<hr />

<table id ="press" border=0 cellpadding=8 cellspacing=0 align="center">
<tr>
  <th colspan=2 align="left">
    Press Contacts:
  </th>
</tr>
<tr Valign="top">
  <td >
    United&nbsp;States:
  </td>
  <td >
Andreas Pour<br />
KDE League, Inc.<br />

[pour@kde.org](mailto:pour@kde.org)<br>
(1) 917 312 3122

  </td>
</tr>
<tr valign="top"><td>
Europe (French and English):
</td><td >
David Faure<br>

[faure@kde.org](mailto:faure@kde.org)<br>
(33) 4 3250 1445

</td></tr>
<tr Valign="top">
  <td >
    Europe (English and German):
  </td>
  <td>
    Ralf Nolden<br>
    
  [nolden@kde.org](mailto:nolden@kde.org) <br>
      (49) 2421 502758
  </td>
</tr>
<tr Valign="top">
  <td >
    Europe (English):
  </td>
  <td>
   Jono Bacon <br>
    
  [jono@kde.org](mailto:jono@kde.org) <br>
  </td>
</tr>
<tr Valign="top">
  <td >
      Southeast Asia (English and Indonesian):
  </td>
  <td>
   Ariya Hidayat <br>
    
  [ariya@kde.org](mailto:ariya@kde.org) <br>
  (62) 815 8703177

  </td>
</tr>
</table>