---
aliases:
- ../announce-3.0.1
custom_about: true
custom_contact: true
date: '2002-05-22'
description: KDE 3.0.1 is a bugfix and translation update for the successful KDE 3.0
  release.
title: KDE 3.0.1 Release Announcement
---

FOR IMMEDIATE RELEASE

<h3 align="center">
   KDE Project Ships Translation and Service Release for Leading
   Open Source Desktop
</h3>

The KDE Project Ships the Third-Generation of the Leading Desktop for Linux/UNIX, Offering Enterprises, Governments, Schools, and Businesses an Outstanding Free and Open Desktop Solution

The <a href="http://www.kde.org/">KDE
Project</a> today announced the immediate availability of KDE 3.0.1,
the third generation of KDE's <em>free</em>, powerful desktop for Linux
and other UNIXes. KDE 3.0.1 ships with the core KDE libraries, the
base desktop environment, an <a href="http://www.kdevelop.org/">integrated
development environment</a>,
and hundreds of applications and other desktop enhancements
from the other KDE base packages (PIM, administration, network,
edutainment, development, utilities, multimedia, games, artwork, and
more).

KDE 3.0.1 is primarily a translation release for KDE 3.0, which shipped
in early April. In addition, KDE 3.0.1 offers a number of performance
and usability enhancements. For an extended list of changes since
KDE 3.0, please see the
<a href="/announcements/changelogs/changelog3_0to3_0_1">change
log</a>.

"As is typical for KDE service releases, KDE 3.0.1 completes and polishes
translations which had not fully caught up to the code released with
KDE 3.0," noted <a href="m&#0097;&#105;lto&#x3a;&#116;h&#100;&#64;&#x6b;&#100;&#101;.&#x6f;rg">Thomas Diehl</a>, the KDE
translation team coordinator. "Already available in 50
languages, KDE is ideal for international use. In addition, KDE 3
features a vastly improved localization framework.
We invite all translators to join an existing or create a new
<a href="http://i18n.kde.org/teams/index.php">translation team</a>, and
look forward to a future KDE release that will ship in even more languages."

KDE, including all its libraries and its applications, is
available <em>for free</em> under Open Source licenses.
KDE can be obtained in source and numerous binary formats from the KDE
<a href="http://download.kde.org/stable/3.0.1/">http</a> or
<a href="http://www.kde.org/ftpmirrors.html">ftp</a> mirrors,
and can also be obtained on
<a href="http://www.kde.org/cdrom.html">CD-ROM</a> or with any
of the major Linux/UNIX systems shipping today.

For more information about the KDE 3 series, please read the
<a href="/announcements/announce-3.0">KDE 3.0
announcement</a> and the
<a href="/info/3.0.1">KDE 3.0.x information page</a>.

#### Installing KDE 3.0.1 Binary Packages

<u>Binary Packages</u>.
Some Linux/UNIX OS vendors have kindly provided binary packages of
KDE 3.0.1 for some versions of their distribution, and in other cases
community volunteers have done so.
Some of these binary packages are available for free download from KDE's
<a href="http://download.kde.org/stable/3.0.1/">http</a> or
<a href="http://www.kde.org/ftpmirrors.html">ftp</a> mirrors.
Additional binary packages, as well as updates to the packages now
available, may become available over the coming weeks. Frequently
package updates are made available only at the respective vendors'
download servers, so please check their for updates as well.

Please note that the KDE Project makes these packages available from the
KDE web site as a convenience to KDE users. The KDE Project is not
responsible for these packages as they are provided by third
parties - typically, but not always, the distributor of the relevant
distribution - using tools, compilers, library versions and quality
assurance procedures over which KDE exercises no control. In addition,
please note that some vendors provide binary packages solely through
their servers. If you cannot find a binary package for your OS, or you
are displeased with the quality of binary packages available for your
system, please read the
<a href="http://www.kde.org/packagepolicy.html">KDE Binary Package
Policy</a> and/or contact your OS vendor.

<u>Library Requirements / Options</u>.
The library requirements for a particular binary package vary with the
system on which the package was compiled. Please bear in mind that
some binary packages may require a newer version of Qt and other libraries
than was shipped with the system (e.g., LinuxDistro X.Y
may have shipped with Qt-3.0.0 but the packages below may require
Qt-3.0.3). For general library requirements for KDE, please see the text at
<a href="#source_code-library_requirements">Source Code - Library
Requirements</a> below.

<a name="package_locations"><u>Package Locations</u></a>.
At the time of this release, pre-compiled packages are available through the
KDE servers for the following distributions and versions (please be sure to
read the applicable <code>README</code> first):

<ul>
  <li>
    <a href="http://www.conectiva.com/">Conectiva Linux</a>
    (<a href="http://download.kde.org/stable/3.0.1/Conectiva/8.0/README">README</a>
    /
    <a href="http://download.kde.org/stable/3.0.1/Conectiva/8.0/LEIAME">LEIAME</a>):
    <ul>
      <li>
        8.0:  <a href="http://download.kde.org/stable/3.0.1/Conectiva/8.0/conectiva/">Intel
        i386</a>
      </li>
    </ul>
  </li>
  <li>  
    <a href="http://www.linux-mandrake.com/">Mandrake Linux</a>:
    <ul>
      <li>
        <a href="http://download.kde.org/stable/3.0.1/Mandrake/noarch/">Language packages</a> (all versions and architectures)
      </li>
      <li>
        8.2:  <a href="http://download.kde.org/stable/3.0.1/Mandrake/8.2/">Intel i586</a>
      </li>
      <li>
        8.1:  <a href="http://download.kde.org/stable/3.0.1/Mandrake/8.1/">Intel i586</a>
      </li>
      <li>
        8.0:  <a href="http://download.kde.org/stable/3.0.1/Mandrake/8.0/">Intel i586</a>
      </li>
    </ul>
  </li> 
  <li>
    <a href="http://www.slackware.org/">Slackware Linux</a> 
    (<a href="http://download.kde.org/stable/3.0.1/Slackware/README">README</a>):
    <ul>
      <li>
        <a href="http://download.kde.org/stable/3.0.1/Slackware/kde-i18n/">Language packages</a>
      </li>
      <li>
       8.0: 
       <a href="http://download.kde.org/stable/3.0.1/Slackware/">Intel i386</a>
      </li>
    </ul>
  </li>
  <li>
    <a href="http://www.suse.com/">SuSE Linux</a>
    (<a href="http://download.kde.org/stable/3.0.1/SuSE/README">README</a>):
    <ul>
      <li>
        <a href="http://download.kde.org/stable/3.0.1/SuSE/NOARCH/">Language
        packages</a> (all versions and architectures)
      </li>
      <li>
        8.0:
        <a href="http://download.kde.org/stable/3.0.1/SuSE/i386/8.0/">Intel
        i386</a>
      </li>
      <li>
        7.3:
        <a href="http://download.kde.org/stable/3.0.1/SuSE/i386/7.3/">Intel
        i386</a>,
        <a href="http://download.kde.org/stable/3.0.1/SuSE/ppc/7.3/">IBM
        PowerPC</a>,
        <a href="http://download.kde.org/stable/3.0.1/SuSE/sparc/7.3/">Sun
        Sparc</a>
      </li>
      <li>
        7.2:
        <a href="http://download.kde.org/stable/3.0.1/SuSE/i386/7.2/">Intel
        i386</a>
      </li>
      <li>
        7.1:
        <a href="http://download.kde.org/stable/3.0.1/SuSE/i386/7.1/">Intel
        i386</a>,
        <a href="http://download.kde.org/stable/3.0.1/SuSE/ppc/7.1/">IBM
        PowerPC</a>
      </li>
      <li>
        7.0:
        <a href="http://download.kde.org/stable/3.0.1/SuSE/i386/7.0/">Intel
        i386</a>,
        <a href="http://download.kde.org/stable/3.0.1/SuSE/s390/7.0/">IBM
        S390</a>
      </li>
    </ul>
  </li>
  <li>
    <a href="http://www.tru64unix.compaq.com/">Tru64</a>
    (<a href="http://download.kde.org/stable/3.0.1/Tru64/README.Tru64">README</a>):
    <ul>
      <li>
        4.0d, e, f and g and 5.x:
        <a href="http://download.kde.org/stable/3.0.1/Tru64/">Compaq Alpha</a>
      </li>
    </ul>
  </li>
  <li>
    <a href="http://www.turbolinux.com/">Turbolinux</a> 
    (<a href="http://download.kde.org/stable/3.0.1/Turbo/README">README</a>):
    <ul>
      <li>
        <a href="http://download.kde.org/stable/3.0.1/Turbo/noarch/">Language
        packages</a> (all versions and architectures)</li>
      <li>
       7.0:
       <a href="http://download.kde.org/stable/3.0.1/Turbo/i586/">Intel i586</a>
      </li>
    </ul>
   </li>
</ul>

Additional binary packages will likely become available over the coming
days and weeks.

#### Compiling KDE 3.0.1

<a id="source_code-library_requirements"></a>
<u>Library
Requirements / Options</u>.
KDE 3.0.1 requires
<a href="http://www.trolltech.com/developer/download/qt-x11.html">Qt
3.0.3 for X11</a>.
In addition, KDE can take advantage of a number of other
<a href="../3.0/#source_code-library_requirements">optional
libraries and applications</a> to improve your desktop experience and
capabilities.

<u>Compiler Requirements</u>.
KDE is designed to be cross-platform and hence to compile with a large
variety of Linux/UNIX compilers. However, KDE is advancing very rapidly
and the ability of native compilers on various UNIX systems to compile
KDE depends on users of those systems
<a href="http://bugs.kde.org/">reporting</a> compile problems to
the responsible developers.

With respect to the most popular KDE compiler,
<a href="http://gcc.gnu.org/">gcc/egcs</a>, please note that some
components of KDE will not compile properly with gcc versions earlier
than gcc-2.95, such as egcs-1.1.2 or gcc-2.7.2, or with unpatched
versions of <a href="http://gcc.gnu.org/gcc-3.0/gcc-3.0.html">gcc</a> 3.0.x.
However, KDE should compile properly with gcc 3.1, provided that
neither debugging support nor strict syntax checking is enabled.

<u>Source Code / SRPMs</u>.
The complete source code for KDE 3.0.1 is available for free
<a href="http://download.kde.org/stable/3.0.1/src/">download</a>:

<table border="0" cellpadding="4" cellspacing="0">
  <tr valign="top">
    <th align="left">Location</th>
    <th align="left">Description</th>
    <th align="left">Size</th>
    <th align="left">MD5&nbsp;Sum</th>
  </tr>
  <tr valign="top">
    <td><a href="http://download.kde.org/stable/3.0.1/src/arts-1.0.1.tar.bz2">arts-1.0.1.tar.bz2</a></td>
    <td>Multimedia Subsystem</td>
    <td>996&nbsp;kB</td>
    <td><tt>ab2978abbbfa306dcb169205676f3e2c</tt></td>
  </tr>
  <tr valign="top">
    <td><a href="http://download.kde.org/stable/3.0.1/src/kdelibs-3.0.1.tar.bz2">kdelibs-3.0.1.tar.bz2</a></td>
    <td>Core Libraries</td>
    <td>7.3&nbsp;MB</td>
    <td><tt>3a9b75af1d8916194cae27935c1c59d5</tt></td>
  </tr>
  <tr valign="top">
    <td><a href="http://download.kde.org/stable/3.0.1/src/kdebase-3.0.1.tar.bz2">kdebase-3.0.1.tar.bz2</a></td>
    <td>Core Desktop Applications</td>
    <td>13&nbsp;MB</td>
    <td><tt>30399832405fa2286f8fb4fd57148b5c</tt></td>
  </tr>
  <tr valign="top">
    <td><a href="http://download.kde.org/stable/3.0.1/src/kdeaddons-3.0.1.tar.bz2">kdeaddons-3.0.1.tar.bz2</a></td>
    <td>Various Addons / Plugins<br />(Multimedia / Konqueror)</td>
    <td>900&nbsp;kB</td>
    <td><tt>341be30069550f32bd3894bb0da51ec4</tt></td>
  </tr>
  <tr valign="top">
    <td><a href="http://download.kde.org/stable/3.0.1/src/kdeadmin-3.0.1.tar.bz2">kdeadmin-3.0.1.tar.bz2</a></td>
    <td>System Adminiatration</td>
    <td>1.3&nbsp;MB</td>
    <td><tt>9f62e6bc85397034a04767de37c48b44</tt></td>
  </tr>
  <tr valign="top">
    <td><a href="http://download.kde.org/stable/3.0.1/src/kdeartwork-3.0.1.tar.bz2">kdeartwork-3.0.1.tar.bz2</a></td>
    <td>Artwork / Themes</td>
    <td>11&nbsp;MB</td>
    <td><tt>87ca9fbac6a97b6aa0689f53fca41565</tt></td>
  </tr>
  <tr valign="top">
    <td><a href="http://download.kde.org/stable/3.0.1/src/kdebindings-3.0.1.tar.bz2">kdebindings-3.0.1.tar.bz2</a></td>
    <td>Language Bindings (devel)</td>
    <td>4.9&nbsp;MB</td>
    <td><tt>19405b2df3b11c89bdd711d8a5fb89e2</tt></td>
  </tr>
  <tr valign="top">
    <td><a href="http://download.kde.org/stable/3.0.1/src/kdeedu-3.0.1.tar.bz2">kdeedu-3.0.1.tar.bz2</a></td>
    <td>Edutainment</td>
    <td>8.7&nbsp;MB</td>
    <td><tt>2b5056b1aea7fc82af2e9dc5aa3906fb</tt></td>
  </tr>
  <tr valign="top">
    <td><a href="http://download.kde.org/stable/3.0.1/src/kdegames-3.0.1.tar.bz2">kdegames-3.0.1.tar.bz2</a></td>
    <td>Games</td>
    <td>7.0&nbsp;MB</td>
    <td><tt>1827591711ebadce4389a3d98ca5565d</tt></td>
  </tr>
  <tr valign="top">
    <td><a href="http://download.kde.org/stable/3.0.1/src/kdegraphics-3.0.1.tar.bz2">kdegraphics-3.0.1.tar.bz2</a></td>
    <td>Graphics</td>
    <td>2.6&nbsp;MB</td>
    <td><tt>9d081640a9524ab64853c2568bb507ec</tt></td>
  </tr>
  <tr valign="top">
    <td><a href="http://download.kde.org/stable/3.0.1/src/kdemultimedia-3.0.1.tar.bz2">kdemultimedia-3.0.1.tar.bz2</a></td>
    <td>Multimedia</td>
    <td>5.6&nbsp;MB</td>
    <td><tt>a1aec32291ecab4d683d11bdad360874</tt></td>
  </tr>
  <tr valign="top">
    <td><a href="http://download.kde.org/stable/3.0.1/src/kdenetwork-3.0.1.tar.bz2">kdenetwork-3.0.1.tar.bz2</a></td>
    <td>Networking</td>
    <td>3.7&nbsp;MB</td>
    <td><tt>4c8fd0d02855efdf2f02372bb7cf137e</tt></td>
  </tr>
  <tr valign="top">
    <td><a href="http://download.kde.org/stable/3.0.1/src/kdepim-3.0.1.tar.bz2">kdepim-3.0.1.tar.bz2</a></td>
    <td>Personal Information Management</td>
    <td>3.1&nbsp;MB</td>
    <td><tt>cbc606a7f63b8abd1d03441ced3fb8e6</tt></td>
  </tr>
  <tr valign="top">
    <td><a href="http://download.kde.org/stable/3.0.1/src/kdesdk-3.0.1.tar.bz2">kdesdk-3.0.1.tar.bz2</a></td>
    <td>Development Tools</td>
    <td>1.8&nbsp;MB</td>
    <td><tt>4eeaef85f70c274e79e0300d054394a8</tt></td>
  </tr>
  <tr valign="top">
    <td><a href="http://download.kde.org/stable/3.0.1/src/kdetoys-3.0.1.tar.bz2">kdetoys-3.0.1.tar.bz2</a></td>
    <td>Amusement / Toys</td>
    <td>1.4&nbsp;MB</td>
    <td><tt>c3520fd9c223fe27480ce0291f4631e2</tt></td>
  </tr>
  <tr valign="top">
    <td><a href="http://download.kde.org/stable/3.0.1/src/kdeutils-3.0.1.tar.bz2">kdeutils-3.0.1.tar.bz2</a></td>
    <td>Utilities</td>
    <td>1.5&nbsp;MB</td>
    <td><tt>ad6cd9a0cf8033a04ef9c2be8030a5b6</tt></td>
  </tr>
  <tr valign="top">
    <td><a href="http://download.kde.org/stable/3.0.1/src/kde-i18n-3.0.1.tar.bz2">kde-i18n-3.0.1.tar.bz2</a><sup>*</sup></td>
    <td>Translations<sup>*</sup></td>
    <td>95&nbsp;MB</td>
    <td><tt>c79b9abad46229b67773daba4551f699</tt></td>
  </tr>
  <tr valign="top">
    <td><a href="http://download.kde.org/stable/3.0.1/src/kdevelop-2.1.1_for_KDE_3.0.tar.bz2">kdevelop-2.1.1_for_KDE_3.0.tar.bz2</a></td>
    <td>Development IDE (KDE 3.0.x)</td>
    <td>3.3&nbsp;MB</td>
    <td><tt>eea8e3b3b3d3bb88933185b3bdf29d75</tt></td>
  </tr>
  <tr valign="top">
    <td><a href="http://download.kde.org/stable/3.0.1/src/kdevelop-2.1.1_for_KDE_2.2.tar.bz2">kdevelop-2.1.1_for_KDE_2.2.tar.bz2</a></td>
    <td>Development IDE (KDE 2.2.x)</td>
    <td>3.1&nbsp;MB</td>
    <td><tt>c41db80c84e57d1184dcf4eb40b26e11</tt></td>
  </tr>
</table>
<sup>*</sup>  The translation package has been split into individual language
packages so you can
<a href="http://download.kde.org/stable/3.0.1/src/">download</a> only the
translations you need.

Additionally, source rpms are available for
<a href="http://download.kde.org/stable/3.0.1/Conectiva/8.0/SRPMS.kde/">Conectiva
Linux</a>, <a href="http://download.kde.org/stable/3.0.1/Mandrake/SRPMS/">Mandrake Linux</a>
and
<a href="http://download.kde.org/stable/3.0.1/SuSE/SRPMS/">SuSE Linux</a>.

<u>Further Information</u>. For further
instructions on compiling and installing KDE 3.0.1, please consult
the <a href="http://developer.kde.org/build/index.html">installation
instructions</a> and, if you should encounter compilation difficulties, the
<a href="http://developer.kde.org/build/compilationfaq.html">KDE Compilation FAQ</a>.
Please visit the <a href="http://www.kde.org/info/3.0.1.html">KDE 3.0.1 information page</a>
for updates.

#### KDE Sponsorship

Besides the superb and invaluable efforts by the
<a href="http://www.kde.org/gallery/index.html">KDE developers</a>
themselves, significant support for KDE development has been provided by
<a href="http://www.mandrakesoft.com/">MandrakeSoft</a> and
<a href="http://www.suse.com/">SuSE</a>. In addition,
the members of the <a href="http://www.kdeleague.org/">KDE
League</a> provide significant support for KDE promotion, and the
<a href="http://www.uni-tuebingen.de/">University of T&uuml;bingen</a>
provides most of the Internet bandwidth for the KDE project.
Thanks!

#### About KDE

KDE is an independent project of hundreds of developers, translators,
artists and other professionals worldwide collaborating over the Internet
to create and freely distribute a sophisticated, customizable and stable
desktop and office environment employing a flexible, component-based,
network-transparent architecture and offering an outstanding development
platform. KDE provides a stable, mature desktop, a full, component-based
office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
set of networking and administration tools and utilities, and an
efficient, intuitive development environment featuring the excellent IDE
<a href="http://www.kdevelop.org/">KDevelop</a>. KDE is working proof
that the Open Source "Bazaar-style" software development model can yield
first-rate technologies on par with and superior to even the most complex
commercial software.

<hr  />

  <font size="2">
  <em>Trademarks Notices.</em>
  KDE, K Desktop Environment, KDevelop and KOffice are trademarks of KDE e.V.

Compaq, Alpha and Tru64 are either trademarks and/or service marks or
registered trademarks and/or service marks of Hewlett-Packard Company.

IBM and PowerPC are registered trademarks of IBM Corporation.

Intel is a trademark or registered trademark of Intel Corporation or its
subsidiaries in the United States and other countries.

Linux is a registered trademark of Linus Torvalds.

Trolltech and Qt are trademarks of Trolltech AS.

UNIX and Motif are registered trademarks of The Open Group.

All other trademarks and copyrights referred to in this announcement are
the property of their respective owners.
</font>

<hr />

<table id ="press" border=0 cellpadding=8 cellspacing=0 align="center">
<tr>
  <th colspan=2 align="left">
    Press Contacts:
  </th>
</tr>
<tr Valign="top">
  <td >
    United&nbsp;States:
  </td>
  <td >
Andreas Pour<br />
KDE League, Inc.<br />

[pour@kde.org](mailto:pour@kde.org)<br>
(1) 917 312 3122

  </td>
</tr>
<tr valign="top"><td>
Europe (French and English):
</td><td >
David Faure<br>

[faure@kde.org](mailto:faure@kde.org)<br>
(33) 4 3250 1445

</td></tr>
<tr Valign="top">
  <td >
    Europe (English and German):
  </td>
  <td>
    Ralf Nolden<br>
    
  [nolden@kde.org](mailto:nolden@kde.org) <br>
      (49) 2421 502758
  </td>
</tr>
<tr Valign="top">
  <td >
    Europe (English):
  </td>
  <td>
   Jono Bacon <br>
    
  [jono@kde.org](mailto:jono@kde.org) <br>
  </td>
</tr>
</table>