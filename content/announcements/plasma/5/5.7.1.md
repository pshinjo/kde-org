---
aliases:
- ../../plasma-5.7.1
changelog: 5.7.0-5.7.1
date: 2016-07-12
layout: plasma
youtube: A9MtFqkRFwQ
figure:
  src: /announcements/plasma/5/5.7.0/plasma-5.7.png
  class: text-center mt-4
asBugfix: true
---

- Fix shadow rendering calculations. <a href="http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=62d09fad123d9aab5afcff0f27d109ef7c6c18ba">Commit.</a> Fixes bug <a href="https://bugs.kde.org/365097">#365097</a> 'Krunner has broken shadow / corners'
- Make the systray work with scripting shell again. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6adba6315275f4ef6ebf8879c851830c268f7a51">Commit.</a>

- Fix microphone increase/decrease volume actions. <a href="http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=9f9d980414ef6b12d24bde8cf2967b519be29524">Commit.</a>
- Fix files/folders in desktop not opening with right click context menu. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6911541421dc068cee531a5fd5f322c0db5d7492">Commit.</a> Fixes bug <a href="https://bugs.kde.org/364530">#364530</a>
