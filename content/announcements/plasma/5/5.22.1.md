---
date: 2021-06-15
changelog: 5.22.0-5.22.1
layout: plasma
asBugfix: true
draft: false
---

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.22/FINAL.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.22/FINAL.mp4" poster="https://kde.org/announcements/plasma/5/5.22.1/5.22.1-video-cover.png" >}}

+ KWin: Platforms/drm: support NVidia as secondary GPU with CPU copy. [Commit.](http://commits.kde.org/kwin/5363451eae46aa87c4fffb27a197c2902f67e817) Fixes bug [#431062](https://bugs.kde.org/431062)
+ Weather applet: Point bbcukmet to new location API. [Commit.](http://commits.kde.org/plasma-workspace/398762cf8763d56c5b6d6ce21ff29d30f3bb98d3) Fixes bug [#430643](https://bugs.kde.org/430643)
+ Wallpapers: Add milky way. [Commit.](http://commits.kde.org/plasma-workspace-wallpapers/4f8e26400b0d04bde60adf99f55e2b34cc6c97ab) Fixes bug [#438349](https://bugs.kde.org/438349)
