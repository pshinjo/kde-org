---
aliases:
- ../../plasma-5.12.6
changelog: 5.12.5-5.12.6
date: 2018-06-27
layout: plasma
youtube: xha6DJ_v1E4
figure:
  src: /announcements/plasma/5/5.12.0/plasma-5.12.png
  class: text-center mt-4
asBugfix: true
---

- Fix avatar picture aliasing and stretching in kickoff. <a href="https://commits.kde.org/plasma-desktop/9da4da7ea7f71dcf6da2b6b263f7a636029c4a25">Commit.</a> Fixes bug <a href="https://bugs.kde.org/369327">#369327</a>. Phabricator Code review <a href="https://phabricator.kde.org/D12469">D12469</a>
- Discover: Fix there being more security updates than total updates in notifier. <a href="https://commits.kde.org/discover/9577e7d1d333d43d899e6eca76bfb309483eb29a">Commit.</a> Fixes bug <a href="https://bugs.kde.org/392056">#392056</a>. Phabricator Code review <a href="https://phabricator.kde.org/D13596">D13596</a>
- Discover Snap support: make it possible to launch installed applications. <a href="https://commits.kde.org/discover/d9d711ff42a67a2df24317af317e6722f89bd822">Commit.</a>
