---
date: 2021-11-30
changelog: 5.23.3-5.23.4
layout: plasma
video: false
asBugfix: true
draft: false
---

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.23/Plasma_25AE_Final.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.23/Plasma_25AE_Final.mp4" poster="https://kde.org/announcements/plasma/5/5.23.0/Plasma25AE_b.png" >}}

+ Breeze style: Reduce groove opacity for greater contrast with scrollbar/slider/etc. [Commit.](http://commits.kde.org/breeze/dc4236dab2999209f1574dae83e8eb001cf77608) Fixes bug [#444203](https://bugs.kde.org/444203)
+ Applets/weather: Make cursor a pointing hand when hovering over source link. [Commit.](http://commits.kde.org/kdeplasma-addons/ea18868f6b49577b1349438d6e058199705a25b0)
+ Plasma Systemmonitor: Don't make right click popup modal. [Commit.](http://commits.kde.org/plasma-systemmonitor/874d867b161fa134305c86be63740a44bc39fcaf)
