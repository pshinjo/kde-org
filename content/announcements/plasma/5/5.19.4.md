---
aliases:
- ../../plasma-5.19.4
changelog: 5.19.3-5.19.4
date: 2020-07-28
layout: plasma
video: true
asBugfix: true
---

+ Plasma Networkmanager: Make hotspot configuration dialog bigger. <a href="https://commits.kde.org/plasma-nm/42806c6c82511b705a204334cab4c52c66a9f8b9">Commit.</a>
+ Only open KCM in systemsettings if it can be displayed. <a href="https://commits.kde.org/plasma-workspace/5bc6d83ae083632fa4eef21a03b1ef59dd9a852e">Commit.</a> Fixes bug <a href="https://bugs.kde.org/423612">#423612</a>
+ Plasma Vault: Reset password field when the user clicks Ok. <a href="https://commits.kde.org/plasma-vault/5e6a53ba55fd60ace3adbd4ca57f90a5cd44992f">Commit.</a> Fixes bug <a href="https://bugs.kde.org/424063">#424063</a>
