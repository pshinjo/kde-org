---
qtversion: 5.15.2
date: 2022-05-14
layout: framework
libCount: 83
---


### Baloo

* [Timeline KIO] Don't announce that we can write

### Breeze Icons

* Add view-left-new action
* New debug step icons

### Extra CMake Modules

* Drop lib prefix when building for Windows (MinGW)
* Allow ecm_add_{qt,}wayland_{client,server}_protocol take targets
* ECMQueryQt: always use CMake target to find the qmake/qtpaths binary
* KDEGitCommitHooks: only configure pre-commit hook if needed

### KActivitiesStats

* Make replace not block because of missing item removal
* Add ResultModel::forgetResources method

### KDE Doxygen Tools

* only bootstrap when explicitly asked for
* Add a dummy install target to cmake

### KAuth

* Add INSTALL_BROKEN_KAUTH_POLICY_FILES cmake option

### KCalendarCore

* Create UIDs if necessary (bug 339726)

### KCMUtils

* Fix filtering of plugin in KPluginWidget
* Check executables exist in PATH before passing them to QProcess
* KPluginModel: Add method to get KPluginMetaData config object for given plugin id
* KPluginDelegate: Add configTriggered signal
* Refactor QML components of MPluginSelector to have less dependencies and have better layout
* KPluginModel: Add isSaveNeededChanged singal
* Import AboutPlugin dialog from QML
* Expose proxy sort model as private QML API
* Create QML version of KPluginSelector
* Allow consumers to get sort proxy model
* Export the KPluginModel class & make it usable from QML

### KConfig

* KConfigGroup: fix writePathEntry/readPathEntry roundtrip for symlinks
* Support storing QUuid

### KConfigWidgets

* KCommandBar: remove installed event filter in destructor (bug 452527)
* [kcolorschememanager] Rework and improve auto theme switching (bug 452091)
* [kcolorschememanager] Don't save colors on application start
* Move kstatefulbrush implementation to its own file
* fix: KRecentFilesAction saveEntries and loadEntries use the correct group when no group is passed
* Add move constructor and assignment operator for KColorScheme
* Make it clear that KStandardAction::name gives you ascii

### KCoreAddons

* KPluginMetaData: Fix setting of MetaDataOption when building without deprecations
* processlist: don't call procstat_getpathname() at all (FreeBSD)
* ListOpenFilesJob: avoid creating the processlist on FreeBSD multiple times

### KDeclarative

* Add PlaceholderMessage to GridView KCMs

### KFileMetaData

* Check executables exist in PATH before passing them to QProcess
* Create and install version header file
* exiv2extractor: add support for Canon CR3 raw image

### KGlobalAccel

* Add BUILD_RUNTIME option (default ON)
* x11: Implement deactivation
* Add KGlobalAccel::globalShortcutActiveChanged

### KDE GUI Addons

* Un-pluginify modifierkeyinfo
* Add plugin for wayland keystates

### KHolidays #

* Add QML API for the sun and moon computations
* New function for plasma to generate holidays without astro seasons (bug 445324)
* Report intermediate lunar phases as well
* Category added as parameter
* Add functions for holiday only
* Remove double entries and correct indian- (bug 441275)

### KI18n

* KF5I18nConfig: Add missing find_dependency call

### KImageFormats

* avif: prepare for breaking change in libavif
* XCF: Support to QImageIOHandler::Size option
* psd: Fix crash on broken files
* psd: duotone read
* psd: Don't crash with broken images
* psd: Header depth has to be 8 for CM_INDEXED color_mode
* psd: Protect against broken images
* avif: lossless support
* psd: Don't assert on broken files
* PSD: Performance improvements and support to missing common formats

### KIO

* Temporarily revert "Consider slow files as remote files in previewjob"
* KFileWidget: set standard zoomIn/Out keyboard shortcuts
* KFileWidget: allow icon sizes to go up to 512 (bug 452139)
* PreviewJob: consider all the available thumbnail cache pool sizes
* dropjob: Extract Ark data in ctor (bug 453390)
* dropjob: upstream ark behaviour from various file view implementations
* KFileWidget: KF6, fix view size glitch on initial show
* Don't put job/ioworker on hold after getting the mimetype (bug 452729)
* Don't build kio_trash on Android
* Remove unused Blkid dependency
* Mark setSubUrl for cleanup in KF6
* KCoreDirLister: modernise for loops
* [KUrlNavigatorPlacesSelector] Disconnect setupDone signal when done
* [KUrlNavigatorPlacesSelector] Do storage setup also when requesting a new tab (bug 452923)
* [KFilePlacesView] Pass widget to QStyle::styleHint
* Revert "Replace He with They in Doc not found page"
* KFileItemDelegate: Add a semi-transparent effect to the icons of hidden files
* [WidgetsAskUserActionHandler] Use WarningYesNo for permanently deleting files
* [KDirOperator] Use QSortFilterProxyModel::invalidate() instead of 'fake resorting'
* KFilePlacesView: use animations only if QStyle::SH_Widget_Animation_Duration > 0 (bug 448802)
* Replace He with They in Doc not found page
* KFilePlacesModel: make it more accessible from QML
* KUrlNavigator: offer open in new window action too (bug 451809)
* Prepare KCoreDirLister::matches(Mime)Filter to be private
* KDirOperator: do not overwrite standard back actions
* KFilePlacesView: Improve touch support
* KCoreDirLister: handle dir permission change in non-canonical paths
* KCoreDirLister: handle file removal in non-canonical paths
* KRecentDocument: fix erroneous messages about overwriting
* KRecentDocument: ensure the path to the XBEL file existss (for CI)
* Fix ~KTcpSocket crash with libc++ (e.g. FreeBSD)

### Kirigami

* ScrollablePage: Stop duck-typing QML types
* SwipeListItem: Expose the width of overlayLoader
* Dialog: Unbreak standardButton() method
* ApplicationItem: Fix Shortcut's warning about sequence vs. sequences
* ApplicationItem: Clean it up a bit
* columnview: Fix memory leak caused by `QQmlComponent::create`
* columnview: Remove m_attachedObjects
* AbstractApplicationItem: Fix copy-pasta mistake
* correctly hide the bottom floating buttons when ther eis a page footer
* ToolBarPageHeader: Do not assign undefined to real values
* ColumnView notifies for contentChildren when page is removed (bug 452883)
* ActionToolBar: fix moreButton visibility (bug 449031)
* LoadingPlaceholder: remove redundant explanation
* Improve ListItemDragHandle
* Add LoadingPlaceholder component
* AboutPage: Prevent infinite loop (bug 447958)
* PlaceholderMessage: add types
* Introduce TabletModeChangedEvent
* [doc]: Remove double inheritance arrow for Kirigami.PagePoolAction

### KItemModels

* Fix assertions in KDescendantsProxyModel (bug 452043)
* Fix punctuation/whitespace of runtime rate-limited deprecation warnings

### KNewStuff

* Action: simplify expression for engine property
* Deprecate KNS3::QtQuickDialogWrapper::exec (bug 450702)
* qtquickdialogwrapper: Fix memory leak caused by `QQmlComponent::create` (bug 452865)
* Use Kirigami.LoadingPlaceholder component
* Delete our own custom PlaceholderMessage
* Dialog: Port to Layouts
* Dialog: use a real ToolBar for the footer instead of a homemade one
* Dialog: Add a little separator line between footer and content
* Convey lack of hotness visually in placeholder message

### KNotification

* Don't send alpha channel if pixmap has none

### KPackage Framework

* Remove CMAKE_AUTOMOC_RELAXED_MODE

### KPeople

* Add avatar image provider

### KPty

* KPtyProcess: call childProcessModifier() of parent class first

### KQuickCharts

* Use ECM_MODULE_DIR instead of ECM_DIR/../modules

### KRunner

* Make Qt::Gui dependency for deprecation free builds internal
* Deprecate public KConfigGroup include of AbstractRunner class

### KService

* KService: Do not link KCoreAddons and KConfig publicly when building without deprecations
* kservice.h: Wrap public KCoreAddons includes in deprecation macros

### KTextEditor

* fix vector init in commands()
* inc ui file version after action name fixes
* EmulatedCommandBarTest: port from processEvents to QTRY_VERIFY
* create commands and motions once
* avoid storing instance in commands/motions
* avoid cursor move on insert of line at EOF on save (bug 453252)
* use proper name for selection menu
* Multicursors: Use shorter actions names
* Multicursors: Move current cursor to end of line when cursors are created from selection
* more deprecation fixes for Qt 6
* Fix completion leaks, don't use null parent
* Add option to show folding markers on hover only
* Paint using path once instead of painting multiple rects
* Fix background visible through selection with custom painted selection
* vimode: add another motion command for moving down one line
* Fix help link for editing command line
* Fix crash when ModeMenuList wasn't init but we try to reload it (bug 452282)
* Enable auto-brackets by default
* Improve Color theme config page

### KUnitConversion

* Don't use GenericDataLocation on Android

### KWayland

* client: implement plasma-surface openUnderCursor
* Fix include path in the generated pkgconfig file
* [plasmawindowmanagement] Add resourceName

### KWidgetsAddons

* Use KDatePickerPopup in KDateComboBox
* Add support for custom date word maps, as done in KDateComboBox
* Share date range support between KDateComboBox and KDatePickerPopup
* Allow to change KDatePickerPopup modes at runtime
* Implement date word actions as done in KDateCombobox
* Build the date picker menu dynamically on demand
* Add KDatePickerPopup
* KPageDialog: Add a new face type with a flat list of small icons

### KXMLGUI

* ui_standards.rc: add tag so kate can insert a menu between Edit and View
* Fix saving of state config if one has autosave enabled (bug 451725)
* Replace OS-specific system information code with QSysInfo (bug 450862)

### NetworkManagerQt

* WirelessNetwork: Fix reference access point for the active network

### Plasma Framework

* IconItem: Allow specifying a custom loader
* plasmastyle: Import PlasmaCore in MenuStyle
* Don't assert if the theme is not found, just warn
* desktoptheme: Install plasmarc
* plasmaquick: Fix memory leak caused by `QQmlComponent::create`
* Revert "Prevent tooltips from being incorrectly dismissed" (bug 439522)
* wallpaperinterface: Add some APIs needed for wallpaper accent color support
* Fix use-after-free in ContainmentInterface (bug 451267)
* Fix osd dialog position (bug 452648)
* mark plasmapkg as nongui executable
* Fix check for argument length in DataEngine loader (bug 452596)
* desktoptheme: Convert desktop to json files
* desktoptheme: Separate config from theme metadata
* Convert desktop files of plugins to json
* examples: Convert desktop files of KPackages to json
* Keep PlasmaComponents.Highlight on its former behaviour (bug 452555)
* Dialog: Do not update layout parameters while hidden (bug 452512)
* Wrap KPluginInfo includes in deprecation wrappers
* PlaceholderMessage: Remove Kirigami references
* PlaceholderMessage: add types
* PC3 toggle controls: fix odd heights misaligning indicators (bug 447977)
* Fix search field left padding when icon is hidden
* ExpandableListItem: deprecate contextmenu and allow actions+custom view
* Deprecate DataEngine related code in Plasma::PluginLoader
* Plasma::Theme: Port last KPluginInfo usage
* Plasma::Theme: Allow packages to ship metadata.json file, move config to separate file
* Add include needed by Qt6 forgotten in d74a8286e1

### Prison

* Remove duplicate header between .h/.cpp file
* Fix out-of-bounds read on the Aztec special char table
* Enable macOS support
* Fix PrisonScanner target name
* Consider flipped video frames when computing the barcode positioon
* Add barcode scanner component for barcode scanning from live video

### Purpose

* Port to ecm_add_qml_module

### QQC2StyleBridge

* Remove infoChanged signal from KQuickStyleItem
* Replace connect to self in KQuickStyleItem with direct method calls
* Recalculate metrics when tablet mode changes
* Fix use of a no longer existing id

### Solid

* udev/cpuinfo_arm: Add missing CPU ID

### Syntax Highlighting

* cmake.xml: Updates for CMake 3.23
* Fix haxe rawstring escaping

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
