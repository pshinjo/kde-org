---
title: "Randa Meetings 2014 fundraising"
goal: 20000
---

€15397.06 raised of a €20000 goal

<div style="width: 100%; height: 30px; border: 1px solid #888; background: rgb(204,204,204); position: relative;"><div style="height: 30px; background: rgb(68,132,242); width: 77%"></div></div>
<br/>

<b>The Randa Meetings 2014 Fundraiser has finished. Thank you everybody who supported us in this fundraiser. We didn't reach the set goal but we collected quite some money and that means there is going to be Randa Meetings in August 2014. See <a href="http://planet.kde.org">http://planet.kde.org</a> for more information to come and go to the <a href="https://www.kde.org/community/donations/">KDE donation page</a> if you want to support us further.</b>

For the fifth year, the intense sprints in Randa, Switzerland will include key KDE projects and top developers, all working concurrently under one roof, isolated from noise and distractions.

Previous Randa Meetings have been concentrated and productive, generating exceptional results; organizers and participants expect the same and more this year. And the meetings have produced significant breakthroughs for KDE software users and developers.


* <a href="http://dot.kde.org/2011/06/29/platform-frameworks-kde-hackers-meet-switzerland">KDE Frameworks 5 were started at the Randa Meetings in 2011</a>
* <a href="http://www.kdenlive.org/users/j-b-m/randa-kde-sprint">Kdenlive came closer to KDE in 2011</a>
* <a href="http://dot.kde.org/2010/06/19/report-successful-multimedia-and-edu-sprint-randa">After a discussion with the Qt offices in Brisbane it was decided to keep Phonon and further develop it</a>


Participants donate their time to help improve the software you love and this is why we need money to cover hard expenses like accommodation and travel to get the volunteer contributors to Randa. If you are not attending, you can still support the Randa Meetings by making a donation. As in the past, the Randa Meetings will benefit everyone who uses KDE software.

We have a fundraising goal of €<?php echo $goal; ?>. Please donate what you can to help make the Randa Meetings 2014 possible. This fund campaign ends on the 9th of July (one month before the beginning of the Randa Meetings 2014).

{{<figure src="/content/fundraisers/randameetings2014/randa1.jpg" class="float-right" width="400px">}}


Randa Meetings 2014 projects:

  * <a href="http://amarok.kde.org/">Amarok/Multimedia/Phonon</a>
  * <a href="http://mail.kde.org/pipermail/kde-frameworks-devel/2014-April/014575.html">KDE Books</a>
  * <a href="http://edu.kde.org">KDE Edu</a> and <a href="http://www.gcompris.net">GCompris</a>
  * <a href="http://community.kde.org/Frameworks/Porting_Notes">KDE Frameworks 5 porting</a>
  * <a href="http://www.proli.net/2014/03/19/kde-sdk-next-how-will-we-develop-in-a-year/">KDE SDK</a>
  * <a href="http://www.kdenlive.org">Kdenlive</a>
  * <a href="http://userbase.kde.org/Gluon">Gluon</a>


Links:

  * <a href="http://www.randa-meetings.ch">Website of the Randa Meetings</a>
  * <a href="http://sprints.kde.org/sprint/212">Participants and plans for the Randa Meetings 2014</a>
  * <a href="http://community.kde.org/Sprints/Randa">More information about past and current Randa Meetings</a>


Read more about the Randa Meetings and the Fundraising on <a href="http://dot.kde.org/2014/05/27/randa-moving-kde-forward">KDE.News</a> where you can write your comments.

The costs for the Randa Meetings 2014 are composed of the following items:

* International travel costs: EUR 10'000
* Swiss train tickets: EUR 3'000
* House rental: EUR 4'000
* Expense allowance: EUR 2'000
* Miscellaneous: EUR 1'000


Even if we don't reach our ambitious goal, the money received will be spent for the Randa Meetings (although with fewer participants) or other KDE purposes (like other sprints).

If you prefer to use international bank transfers please <a href="/community/donations/#moneytransfer">see this page</a>.<br />
Please write us <a href="mailto:randa@kde.org">an email</a> so we can add you to the list of donors manually.

### List of donations

<table class="table" width="100%">
  <tbody>
    <tr>
      <th width="20">No.</th>
      <th width="100">Date</th>
      <th width="80">Amount</th>
      <th style="text-align:left">Donor Name</th>
    </tr>
    <tr>
      <td align="center">428</td>
      <td align="center">12th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Ben Williges</td>
    </tr>
    <tr>
      <td align="center">427</td>
      <td align="center">12th July 2014</td>
      <td align="right">€&nbsp;30.00</td>
      <td>Philipp Doebereiner</td>
    </tr>
    <tr>
      <td align="center">426</td>
      <td align="center">12th July 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Ingo Kloecker</td>
    </tr>
    <tr>
      <td align="center">425</td>
      <td align="center">12th July 2014</td>
      <td align="right">€&nbsp;15.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">424</td>
      <td align="center">12th July 2014</td>
      <td align="right">€&nbsp;6.66</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">423</td>
      <td align="center">12th July 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Josep Maria Ferrer Puigdomenech</td>
    </tr>
    <tr>
      <td align="center">422</td>
      <td align="center">12th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Thomas Eckart</td>
    </tr>
    <tr>
      <td align="center">421</td>
      <td align="center">12th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>M Robert Riemann</td>
    </tr>
    <tr>
      <td align="center">420</td>
      <td align="center">12th July 2014</td>
      <td align="right">€&nbsp;115.00</td>
      <td>Juliane Andereggen</td>
    </tr>
    <tr>
      <td align="center">419</td>
      <td align="center">12th July 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Dominic Tubach</td>
    </tr>
    <tr>
      <td align="center">418</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Marco Poli</td>
    </tr>
    <tr>
      <td align="center">417</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Marco Poli</td>
    </tr>
    <tr>
      <td align="center">416</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Michael Linksvayer</td>
    </tr>
    <tr>
      <td align="center">415</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Philippe Cattin</td>
    </tr>
    <tr>
      <td align="center">414</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">413</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Sune Vuorela</td>
    </tr>
    <tr>
      <td align="center">412</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;21.21</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">411</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Joseph Marques</td>
    </tr>
    <tr>
      <td align="center">410</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Roland Schlenker</td>
    </tr>
    <tr>
      <td align="center">409</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">408</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">407</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Michal Hlavac</td>
    </tr>
    <tr>
      <td align="center">406</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">405</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">404</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;15.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">403</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Jeffrey Trull</td>
    </tr>
    <tr>
      <td align="center">402</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Myriam Rita Schweingruber</td>
    </tr>
    <tr>
      <td align="center">401</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Anthony RENOUX</td>
    </tr>
    <tr>
      <td align="center">400</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">399</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Lambert Martijn Otten</td>
    </tr>
    <tr>
      <td align="center">398</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">397</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">396</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Jos Poortvliet</td>
    </tr>
    <tr>
      <td align="center">395</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">394</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Henrik Sonesson</td>
    </tr>
    <tr>
      <td align="center">393</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">392</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">391</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">390</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Joffroy Urbain</td>
    </tr>
    <tr>
      <td align="center">389</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Valerio De Angelis</td>
    </tr>
    <tr>
      <td align="center">388</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">387</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">386</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">385</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Riccardo Ciman</td>
    </tr>
    <tr>
      <td align="center">384</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Anderson de Arruda Casimiro</td>
    </tr>
    <tr>
      <td align="center">383</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">382</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">381</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">380</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Andrew Blackshaw</td>
    </tr>
    <tr>
      <td align="center">379</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">378</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Stefan Borggraefe</td>
    </tr>
    <tr>
      <td align="center">377</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Giles Turner</td>
    </tr>
    <tr>
      <td align="center">376</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">375</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Jan Wiele</td>
    </tr>
    <tr>
      <td align="center">374</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">373</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">372</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">371</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Sergiy Kachanyuk</td>
    </tr>
    <tr>
      <td align="center">370</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">369</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">368</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Tomasz Siekierda</td>
    </tr>
    <tr>
      <td align="center">367</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Nikolaos Chatzidakis</td>
    </tr>
    <tr>
      <td align="center">366</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Laurent Tromeur</td>
    </tr>
    <tr>
      <td align="center">365</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Silvio Kozasa</td>
    </tr>
    <tr>
      <td align="center">364</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Martin Gysel</td>
    </tr>
    <tr>
      <td align="center">363</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">362</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Teemu Erkkola</td>
    </tr>
    <tr>
      <td align="center">361</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">360</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">359</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;7.00</td>
      <td>Tilman Priesner</td>
    </tr>
    <tr>
      <td align="center">358</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Christoph Baldauf</td>
    </tr>
    <tr>
      <td align="center">357</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Jason Hihn</td>
    </tr>
    <tr>
      <td align="center">356</td>
      <td align="center">9th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">355</td>
      <td align="center">8th July 2014</td>
      <td align="right">€&nbsp;120.00</td>
      <td>Tore Havn</td>
    </tr>
    <tr>
      <td align="center">354</td>
      <td align="center">8th July 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">353</td>
      <td align="center">8th July 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">352</td>
      <td align="center">8th July 2014</td>
      <td align="right">€&nbsp;25.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">351</td>
      <td align="center">8th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">350</td>
      <td align="center">8th July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Jan Sommer</td>
    </tr>
    <tr>
      <td align="center">349</td>
      <td align="center">8th July 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Frank Roscher</td>
    </tr>
    <tr>
      <td align="center">348</td>
      <td align="center">8th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">347</td>
      <td align="center">8th July 2014</td>
      <td align="right">€&nbsp;80.00</td>
      <td>Carlo Jeffery</td>
    </tr>
    <tr>
      <td align="center">346</td>
      <td align="center">8th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Stefan Soyka</td>
    </tr>
    <tr>
      <td align="center">345</td>
      <td align="center">8th July 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Pablo Estigarribia Davyt</td>
    </tr>
    <tr>
      <td align="center">344</td>
      <td align="center">8th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">343</td>
      <td align="center">8th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Pierre Salois</td>
    </tr>
    <tr>
      <td align="center">342</td>
      <td align="center">8th July 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Thomas Schyschka</td>
    </tr>
    <tr>
      <td align="center">341</td>
      <td align="center">8th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Luca Sartorelli</td>
    </tr>
    <tr>
      <td align="center">340</td>
      <td align="center">8th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">339</td>
      <td align="center">8th July 2014</td>
      <td align="right">€&nbsp;25.00</td>
      <td>Vincent de Phily</td>
    </tr>
    <tr>
      <td align="center">338</td>
      <td align="center">8th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">337</td>
      <td align="center">8th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">336</td>
      <td align="center">8th July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Aitor Morant Intxausti</td>
    </tr>
    <tr>
      <td align="center">335</td>
      <td align="center">8th July 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">334</td>
      <td align="center">8th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Justin Geibel</td>
    </tr>
    <tr>
      <td align="center">333</td>
      <td align="center">7th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">332</td>
      <td align="center">6th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Pedro Rodrigues</td>
    </tr>
    <tr>
      <td align="center">331</td>
      <td align="center">6th July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Andrei Nistor</td>
    </tr>
    <tr>
      <td align="center">330</td>
      <td align="center">6th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Francois Delpierre</td>
    </tr>
    <tr>
      <td align="center">329</td>
      <td align="center">6th July 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">328</td>
      <td align="center">5th July 2014</td>
      <td align="right">€&nbsp;80.00</td>
      <td>Jean-Baptiste Mardelle</td>
    </tr>
    <tr>
      <td align="center">327</td>
      <td align="center">5th July 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">326</td>
      <td align="center">4th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">325</td>
      <td align="center">4th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">324</td>
      <td align="center">4th July 2014</td>
      <td align="right">€&nbsp;72.00</td>
      <td>Christopher Fritz</td>
    </tr>
    <tr>
      <td align="center">323</td>
      <td align="center">4th July 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Thomas Schwarzgruber</td>
    </tr>
    <tr>
      <td align="center">322</td>
      <td align="center">4th July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Shantanu Tushar</td>
    </tr>
    <tr>
      <td align="center">321</td>
      <td align="center">4th July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Angus Mackenzie</td>
    </tr>
    <tr>
      <td align="center">320</td>
      <td align="center">3rd July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">319</td>
      <td align="center">3rd July 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Henrik Jensen</td>
    </tr>
    <tr>
      <td align="center">318</td>
      <td align="center">3rd July 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">317</td>
      <td align="center">3rd July 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">316</td>
      <td align="center">3rd July 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">315</td>
      <td align="center">3rd July 2014</td>
      <td align="right">€&nbsp;120.00</td>
      <td>Etienne Biardeaud</td>
    </tr>
    <tr>
      <td align="center">314</td>
      <td align="center">3rd July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Bogdan Mihaila</td>
    </tr>
    <tr>
      <td align="center">313</td>
      <td align="center">3rd July 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Kenneth Aar</td>
    </tr>
    <tr>
      <td align="center">312</td>
      <td align="center">3rd July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">311</td>
      <td align="center">2nd July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">310</td>
      <td align="center">2nd July 2014</td>
      <td align="right">€&nbsp;30.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">309</td>
      <td align="center">2nd July 2014</td>
      <td align="right">€&nbsp;0.19</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">308</td>
      <td align="center">2nd July 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Whitney Misja</td>
    </tr>
    <tr>
      <td align="center">307</td>
      <td align="center">2nd July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">306</td>
      <td align="center">2nd July 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">305</td>
      <td align="center">2nd July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Kees Boekhoven</td>
    </tr>
    <tr>
      <td align="center">304</td>
      <td align="center">2nd July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Wolfgang Romey</td>
    </tr>
    <tr>
      <td align="center">303</td>
      <td align="center">2nd July 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">302</td>
      <td align="center">2nd July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">301</td>
      <td align="center">2nd July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>stephen donovan</td>
    </tr>
    <tr>
      <td align="center">300</td>
      <td align="center">2nd July 2014</td>
      <td align="right">€&nbsp;30.00</td>
      <td>Ari Aviran</td>
    </tr>
    <tr>
      <td align="center">299</td>
      <td align="center">2nd July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">298</td>
      <td align="center">1st July 2014</td>
      <td align="right">€&nbsp;25.00</td>
      <td>Claudio Desideri</td>
    </tr>
    <tr>
      <td align="center">297</td>
      <td align="center">1st July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">296</td>
      <td align="center">1st July 2014</td>
      <td align="right">€&nbsp;5.61</td>
      <td>Evandro Pires Alves</td>
    </tr>
    <tr>
      <td align="center">295</td>
      <td align="center">1st July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">294</td>
      <td align="center">1st July 2014</td>
      <td align="right">€&nbsp;200.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">293</td>
      <td align="center">1st July 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">292</td>
      <td align="center">1st July 2014</td>
      <td align="right">€&nbsp;120.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">291</td>
      <td align="center">1st July 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">290</td>
      <td align="center">1st July 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">289</td>
      <td align="center">1st July 2014</td>
      <td align="right">€&nbsp;30.00</td>
      <td>Johannes Tress</td>
    </tr>
    <tr>
      <td align="center">288</td>
      <td align="center">1st July 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Lars Johannes Hetland</td>
    </tr>
    <tr>
      <td align="center">287</td>
      <td align="center">30th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Thiago Bauermann</td>
    </tr>
    <tr>
      <td align="center">286</td>
      <td align="center">30th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Diego Figueroa</td>
    </tr>
    <tr>
      <td align="center">285</td>
      <td align="center">30th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Thomas Gufler</td>
    </tr>
    <tr>
      <td align="center">284</td>
      <td align="center">30th June 2014</td>
      <td align="right">€&nbsp;25.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">283</td>
      <td align="center">30th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Waldo Web Design LLC</td>
    </tr>
    <tr>
      <td align="center">282</td>
      <td align="center">30th June 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Paul Eggleton</td>
    </tr>
    <tr>
      <td align="center">281</td>
      <td align="center">30th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Artur de Souza</td>
    </tr>
    <tr>
      <td align="center">280</td>
      <td align="center">30th June 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Afief Halumi</td>
    </tr>
    <tr>
      <td align="center">279</td>
      <td align="center">30th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Andreas Gustafsson</td>
    </tr>
    <tr>
      <td align="center">278</td>
      <td align="center">30th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">277</td>
      <td align="center">30th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>EpiGeneticK9nz</td>
    </tr>
    <tr>
      <td align="center">276</td>
      <td align="center">30th June 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">275</td>
      <td align="center">30th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">274</td>
      <td align="center">30th June 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Rolf Gottschling</td>
    </tr>
    <tr>
      <td align="center">273</td>
      <td align="center">30th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">272</td>
      <td align="center">30th June 2014</td>
      <td align="right">€&nbsp;25.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">271</td>
      <td align="center">30th June 2014</td>
      <td align="right">€&nbsp;15.00</td>
      <td>Ritesh Sarraf</td>
    </tr>
    <tr>
      <td align="center">270</td>
      <td align="center">30th June 2014</td>
      <td align="right">€&nbsp;200.00</td>
      <td>Home</td>
    </tr>
    <tr>
      <td align="center">269</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Andrea Cavaliero</td>
    </tr>
    <tr>
      <td align="center">268</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Marcin S?gol</td>
    </tr>
    <tr>
      <td align="center">267</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;6.00</td>
      <td>Maximilian Wurster</td>
    </tr>
    <tr>
      <td align="center">266</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;80.00</td>
      <td>Philip Cohn-Cort</td>
    </tr>
    <tr>
      <td align="center">265</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Sebastian Schmitt</td>
    </tr>
    <tr>
      <td align="center">264</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Richard Dohmeier</td>
    </tr>
    <tr>
      <td align="center">263</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;35.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">262</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Lambert Martijn Otten</td>
    </tr>
    <tr>
      <td align="center">261</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">260</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>David Roth</td>
    </tr>
    <tr>
      <td align="center">259</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">258</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">257</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">256</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Aurelie Guidon</td>
    </tr>
    <tr>
      <td align="center">255</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">254</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;30.00</td>
      <td>Stefan Hofbauer</td>
    </tr>
    <tr>
      <td align="center">253</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Roberto Araneda</td>
    </tr>
    <tr>
      <td align="center">252</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">251</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Dr. Frank Lukas</td>
    </tr>
    <tr>
      <td align="center">250</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">249</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;150.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">248</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;30.00</td>
      <td>nicolas schumacher</td>
    </tr>
    <tr>
      <td align="center">247</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Hans - Walter Gardt</td>
    </tr>
    <tr>
      <td align="center">246</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">245</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Kayra Akman</td>
    </tr>
    <tr>
      <td align="center">244</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">243</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Robin Guitton</td>
    </tr>
    <tr>
      <td align="center">242</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Kurt Hindenburg</td>
    </tr>
    <tr>
      <td align="center">241</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;25.00</td>
      <td>Magnus Lundborg</td>
    </tr>
    <tr>
      <td align="center">240</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">239</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">238</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Xavier Francisco Gilabert</td>
    </tr>
    <tr>
      <td align="center">237</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">236</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Yury Vidineev</td>
    </tr>
    <tr>
      <td align="center">235</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Damian Kaczmarek</td>
    </tr>
    <tr>
      <td align="center">234</td>
      <td align="center">29th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Morten Sjoegren</td>
    </tr>
    <tr>
      <td align="center">233</td>
      <td align="center">28th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Radek Novacek</td>
    </tr>
    <tr>
      <td align="center">232</td>
      <td align="center">28th June 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">231</td>
      <td align="center">28th June 2014</td>
      <td align="right">€&nbsp;42.00</td>
      <td>Ian Stanistreet</td>
    </tr>
    <tr>
      <td align="center">230</td>
      <td align="center">28th June 2014</td>
      <td align="right">€&nbsp;80.00</td>
      <td>Carlo Jeffery</td>
    </tr>
    <tr>
      <td align="center">229</td>
      <td align="center">28th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">228</td>
      <td align="center">28th June 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Christoph Burger-Scheidlin</td>
    </tr>
    <tr>
      <td align="center">227</td>
      <td align="center">28th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Arien de Jongste</td>
    </tr>
    <tr>
      <td align="center">226</td>
      <td align="center">28th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">225</td>
      <td align="center">28th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Yuen Hoe Lim</td>
    </tr>
    <tr>
      <td align="center">224</td>
      <td align="center">28th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">223</td>
      <td align="center">28th June 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Carlos Uribe</td>
    </tr>
    <tr>
      <td align="center">222</td>
      <td align="center">27th June 2014</td>
      <td align="right">€&nbsp;15.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">221</td>
      <td align="center">27th June 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Smit Mehta</td>
    </tr>
    <tr>
      <td align="center">220</td>
      <td align="center">27th June 2014</td>
      <td align="right">€&nbsp;2.00</td>
      <td>Rahul Ghose</td>
    </tr>
    <tr>
      <td align="center">219</td>
      <td align="center">26th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">218</td>
      <td align="center">26th June 2014</td>
      <td align="right">€&nbsp;250.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">217</td>
      <td align="center">26th June 2014</td>
      <td align="right">€&nbsp;12.50</td>
      <td>Mr S Drever</td>
    </tr>
    <tr>
      <td align="center">216</td>
      <td align="center">26th June 2014</td>
      <td align="right">€&nbsp;200.00</td>
      <td>Jo Christer Oiongen</td>
    </tr>
    <tr>
      <td align="center">215</td>
      <td align="center">26th June 2014</td>
      <td align="right">€&nbsp;200.00</td>
      <td>Mats Ake Simon Persson</td>
    </tr>
    <tr>
      <td align="center">214</td>
      <td align="center">26th June 2014</td>
      <td align="right">€&nbsp;25.00</td>
      <td>Keezhe Veettil Rajeesh</td>
    </tr>
    <tr>
      <td align="center">213</td>
      <td align="center">26th June 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Sebastien Morenne</td>
    </tr>
    <tr>
      <td align="center">212</td>
      <td align="center">26th June 2014</td>
      <td align="right">€&nbsp;4.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">211</td>
      <td align="center">26th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">210</td>
      <td align="center">25th June 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">209</td>
      <td align="center">25th June 2014</td>
      <td align="right">€&nbsp;30.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">208</td>
      <td align="center">25th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">207</td>
      <td align="center">25th June 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Frederik Gladhorn</td>
    </tr>
    <tr>
      <td align="center">206</td>
      <td align="center">25th June 2014</td>
      <td align="right">€&nbsp;6.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">205</td>
      <td align="center">24th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Sebastian Nellen</td>
    </tr>
    <tr>
      <td align="center">204</td>
      <td align="center">24th June 2014</td>
      <td align="right">€&nbsp;36.00</td>
      <td>Kevin Adler</td>
    </tr>
    <tr>
      <td align="center">203</td>
      <td align="center">23rd June 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">202</td>
      <td align="center">23rd June 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Nadine Pfefferkorn</td>
    </tr>
    <tr>
      <td align="center">201</td>
      <td align="center">23rd June 2014</td>
      <td align="right">€&nbsp;25.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">200</td>
      <td align="center">23rd June 2014</td>
      <td align="right">€&nbsp;15.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">199</td>
      <td align="center">23rd June 2014</td>
      <td align="right">€&nbsp;34.00</td>
      <td>Kieran McCusker</td>
    </tr>
    <tr>
      <td align="center">198</td>
      <td align="center">22nd June 2014</td>
      <td align="right">€&nbsp;11.00</td>
      <td>MEHMET TOPSAKAL</td>
    </tr>
    <tr>
      <td align="center">197</td>
      <td align="center">22nd June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Pedro Vaz</td>
    </tr>
    <tr>
      <td align="center">196</td>
      <td align="center">22nd June 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Andreas Cord-Landwehr</td>
    </tr>
    <tr>
      <td align="center">195</td>
      <td align="center">21st June 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">194</td>
      <td align="center">21st June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">193</td>
      <td align="center">21st June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Geert Janssens</td>
    </tr>
    <tr>
      <td align="center">192</td>
      <td align="center">21st June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">191</td>
      <td align="center">21st June 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">190</td>
      <td align="center">21st June 2014</td>
      <td align="right">€&nbsp;15.00</td>
      <td>Monica Mora</td>
    </tr>
    <tr>
      <td align="center">189</td>
      <td align="center">21st June 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td>jason stanek</td>
    </tr>
    <tr>
      <td align="center">188</td>
      <td align="center">20th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Niklas Larsson</td>
    </tr>
    <tr>
      <td align="center">187</td>
      <td align="center">20th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Algot Runeman</td>
    </tr>
    <tr>
      <td align="center">186</td>
      <td align="center">20th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">185</td>
      <td align="center">20th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Dainius Masilinas</td>
    </tr>
    <tr>
      <td align="center">184</td>
      <td align="center">20th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">183</td>
      <td align="center">19th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">182</td>
      <td align="center">19th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Jan-Matthias Braun</td>
    </tr>
    <tr>
      <td align="center">181</td>
      <td align="center">19th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Carlos Carretero</td>
    </tr>
    <tr>
      <td align="center">180</td>
      <td align="center">19th June 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">179</td>
      <td align="center">19th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">178</td>
      <td align="center">19th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Arne Brix</td>
    </tr>
    <tr>
      <td align="center">177</td>
      <td align="center">19th June 2014</td>
      <td align="right">€&nbsp;30.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">176</td>
      <td align="center">19th June 2014</td>
      <td align="right">€&nbsp;25.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">175</td>
      <td align="center">18th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">174</td>
      <td align="center">18th June 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Vesa Muhonen</td>
    </tr>
    <tr>
      <td align="center">173</td>
      <td align="center">18th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">172</td>
      <td align="center">18th June 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Nuri Tanr?kut</td>
    </tr>
    <tr>
      <td align="center">171</td>
      <td align="center">18th June 2014</td>
      <td align="right">€&nbsp;15.00</td>
      <td>Ivan Radic</td>
    </tr>
    <tr>
      <td align="center">170</td>
      <td align="center">18th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">169</td>
      <td align="center">18th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Ronny Standtke</td>
    </tr>
    <tr>
      <td align="center">168</td>
      <td align="center">18th June 2014</td>
      <td align="right">€&nbsp;25.00</td>
      <td>Martin Imobersteg</td>
    </tr>
    <tr>
      <td align="center">167</td>
      <td align="center">18th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">166</td>
      <td align="center">18th June 2014</td>
      <td align="right">€&nbsp;6.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">165</td>
      <td align="center">18th June 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Joshua Noack</td>
    </tr>
    <tr>
      <td align="center">164</td>
      <td align="center">18th June 2014</td>
      <td align="right">€&nbsp;42.00</td>
      <td>Jan Schnackenberg</td>
    </tr>
    <tr>
      <td align="center">163</td>
      <td align="center">18th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Carlos Diener</td>
    </tr>
    <tr>
      <td align="center">162</td>
      <td align="center">18th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">161</td>
      <td align="center">17th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Jose Figueres Alegre</td>
    </tr>
    <tr>
      <td align="center">160</td>
      <td align="center">17th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">159</td>
      <td align="center">17th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Gonzalo Soler Fornes</td>
    </tr>
    <tr>
      <td align="center">158</td>
      <td align="center">17th June 2014</td>
      <td align="right">€&nbsp;30.00</td>
      <td>Shervin Emami</td>
    </tr>
    <tr>
      <td align="center">157</td>
      <td align="center">16th June 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Cho Sung Jae</td>
    </tr>
    <tr>
      <td align="center">156</td>
      <td align="center">16th June 2014</td>
      <td align="right">€&nbsp;15.00</td>
      <td>Alexis Jurdant</td>
    </tr>
    <tr>
      <td align="center">155</td>
      <td align="center">16th June 2014</td>
      <td align="right">€&nbsp;15.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">154</td>
      <td align="center">16th June 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">153</td>
      <td align="center">15th June 2014</td>
      <td align="right">€&nbsp;164.00</td>
      <td>Diggory Hardy</td>
    </tr>
    <tr>
      <td align="center">152</td>
      <td align="center">15th June 2014</td>
      <td align="right">€&nbsp;25.00</td>
      <td>Thomas Fischer</td>
    </tr>
    <tr>
      <td align="center">151</td>
      <td align="center">15th June 2014</td>
      <td align="right">€&nbsp;768.00</td>
      <td>Marijn Kruisselbrink</td>
    </tr>
    <tr>
      <td align="center">150</td>
      <td align="center">15th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Rolf Eike Beer</td>
    </tr>
    <tr>
      <td align="center">149</td>
      <td align="center">15th June 2014</td>
      <td align="right">€&nbsp;75.00</td>
      <td>Christoph</td>
    </tr>
    <tr>
      <td align="center">148</td>
      <td align="center">15th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Jan Hocker</td>
    </tr>
    <tr>
      <td align="center">147</td>
      <td align="center">15th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">146</td>
      <td align="center">15th June 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Hr R Zweistra</td>
    </tr>
    <tr>
      <td align="center">145</td>
      <td align="center">14th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>David Duelm</td>
    </tr>
    <tr>
      <td align="center">144</td>
      <td align="center">14th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">143</td>
      <td align="center">14th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>shishir verma</td>
    </tr>
    <tr>
      <td align="center">142</td>
      <td align="center">14th June 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Christian Hausknecht</td>
    </tr>
    <tr>
      <td align="center">141</td>
      <td align="center">14th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">140</td>
      <td align="center">13th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Gianluca Montecchi</td>
    </tr>
    <tr>
      <td align="center">139</td>
      <td align="center">13th June 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Luca Manganelli</td>
    </tr>
    <tr>
      <td align="center">138</td>
      <td align="center">13th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">137</td>
      <td align="center">13th June 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">136</td>
      <td align="center">13th June 2014</td>
      <td align="right">€&nbsp;15.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">135</td>
      <td align="center">13th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">134</td>
      <td align="center">13th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Dominik Lutter</td>
    </tr>
    <tr>
      <td align="center">133</td>
      <td align="center">13th June 2014</td>
      <td align="right">€&nbsp;80.00</td>
      <td>Robert Zimmerman</td>
    </tr>
    <tr>
      <td align="center">132</td>
      <td align="center">12th June 2014</td>
      <td align="right">€&nbsp;19.00</td>
      <td>Ion Bulgar</td>
    </tr>
    <tr>
      <td align="center">131</td>
      <td align="center">12th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Rafael Gomes da Cruz</td>
    </tr>
    <tr>
      <td align="center">130</td>
      <td align="center">12th June 2014</td>
      <td align="right">€&nbsp;15.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">129</td>
      <td align="center">11th June 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Wade Olson</td>
    </tr>
    <tr>
      <td align="center">128</td>
      <td align="center">11th June 2014</td>
      <td align="right">€&nbsp;30.00</td>
      <td>Wolfgang Mader</td>
    </tr>
    <tr>
      <td align="center">127</td>
      <td align="center">10th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Alexander Wauck</td>
    </tr>
    <tr>
      <td align="center">126</td>
      <td align="center">10th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Brett Cohen</td>
    </tr>
    <tr>
      <td align="center">125</td>
      <td align="center">10th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Malte Eggers</td>
    </tr>
    <tr>
      <td align="center">124</td>
      <td align="center">9th June 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Bruno Coudoin</td>
    </tr>
    <tr>
      <td align="center">123</td>
      <td align="center">9th June 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Simon Volkert</td>
    </tr>
    <tr>
      <td align="center">122</td>
      <td align="center">9th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Franz Senftl</td>
    </tr>
    <tr>
      <td align="center">121</td>
      <td align="center">8th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Nicolas GANDRIAU</td>
    </tr>
    <tr>
      <td align="center">120</td>
      <td align="center">8th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Mark Cohen</td>
    </tr>
    <tr>
      <td align="center">119</td>
      <td align="center">8th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">118</td>
      <td align="center">7th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">117</td>
      <td align="center">7th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Georgios Kiagiadakis</td>
    </tr>
    <tr>
      <td align="center">116</td>
      <td align="center">7th June 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">115</td>
      <td align="center">6th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>bradley noyes</td>
    </tr>
    <tr>
      <td align="center">114</td>
      <td align="center">6th June 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Joan Pou Nogueras</td>
    </tr>
    <tr>
      <td align="center">113</td>
      <td align="center">6th June 2014</td>
      <td align="right">€&nbsp;30.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">112</td>
      <td align="center">6th June 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Marta Maria Cuesta Bobes</td>
    </tr>
    <tr>
      <td align="center">111</td>
      <td align="center">5th June 2014</td>
      <td align="right">€&nbsp;2.30</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">110</td>
      <td align="center">5th June 2014</td>
      <td align="right">€&nbsp;200.00</td>
      <td>Simon Winiker</td>
    </tr>
    <tr>
      <td align="center">109</td>
      <td align="center">5th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Juan Jo Fernandez Monreal</td>
    </tr>
    <tr>
      <td align="center">108</td>
      <td align="center">5th June 2014</td>
      <td align="right">€&nbsp;0.69</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">107</td>
      <td align="center">5th June 2014</td>
      <td align="right">€&nbsp;0.20</td>
      <td>Gustavo Castro</td>
    </tr>
    <tr>
      <td align="center">106</td>
      <td align="center">5th June 2014</td>
      <td align="right">€&nbsp;42.00</td>
      <td>Stephanie Odok</td>
    </tr>
    <tr>
      <td align="center">105</td>
      <td align="center">5th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Stephen Clark</td>
    </tr>
    <tr>
      <td align="center">104</td>
      <td align="center">5th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Clemens Werther</td>
    </tr>
    <tr>
      <td align="center">103</td>
      <td align="center">5th June 2014</td>
      <td align="right">€&nbsp;6.70</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">102</td>
      <td align="center">5th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>David Baum</td>
    </tr>
    <tr>
      <td align="center">101</td>
      <td align="center">4th June 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">100</td>
      <td align="center">4th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">99</td>
      <td align="center">4th June 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">98</td>
      <td align="center">4th June 2014</td>
      <td align="right">€&nbsp;44.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">97</td>
      <td align="center">4th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">96</td>
      <td align="center">4th June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">95</td>
      <td align="center">4th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Aleix Pol Gonzalez</td>
    </tr>
    <tr>
      <td align="center">94</td>
      <td align="center">4th June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Adriano Fantini</td>
    </tr>
    <tr>
      <td align="center">93</td>
      <td align="center">4th June 2014</td>
      <td align="right">€&nbsp;25.00</td>
      <td>Giuseppe Lovarelli</td>
    </tr>
    <tr>
      <td align="center">92</td>
      <td align="center">4th June 2014</td>
      <td align="right">€&nbsp;21.81</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">91</td>
      <td align="center">4th June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Olav Reinert</td>
    </tr>
    <tr>
      <td align="center">90</td>
      <td align="center">4th June 2014</td>
      <td align="right">€&nbsp;15.00</td>
      <td>Andrew Skripshak</td>
    </tr>
    <tr>
      <td align="center">89</td>
      <td align="center">4th June 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Mauren Ribeiro Berti</td>
    </tr>
    <tr>
      <td align="center">88</td>
      <td align="center">3rd June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">87</td>
      <td align="center">3rd June 2014</td>
      <td align="right">€&nbsp;35.00</td>
      <td>Filipe Saraiva</td>
    </tr>
    <tr>
      <td align="center">86</td>
      <td align="center">3rd June 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">85</td>
      <td align="center">3rd June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>GUILLAUME AUDARD</td>
    </tr>
    <tr>
      <td align="center">84</td>
      <td align="center">3rd June 2014</td>
      <td align="right">€&nbsp;15.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">83</td>
      <td align="center">3rd June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Matthew Williams</td>
    </tr>
    <tr>
      <td align="center">82</td>
      <td align="center">3rd June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Stefano Avallone</td>
    </tr>
    <tr>
      <td align="center">81</td>
      <td align="center">3rd June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">80</td>
      <td align="center">3rd June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Kevin Funk</td>
    </tr>
    <tr>
      <td align="center">79</td>
      <td align="center">3rd June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">78</td>
      <td align="center">3rd June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Bartosz Mikucewicz</td>
    </tr>
    <tr>
      <td align="center">77</td>
      <td align="center">3rd June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Dominik Cermak</td>
    </tr>
    <tr>
      <td align="center">76</td>
      <td align="center">3rd June 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Helio Castro</td>
    </tr>
    <tr>
      <td align="center">75</td>
      <td align="center">3rd June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Justin Charles</td>
    </tr>
    <tr>
      <td align="center">74</td>
      <td align="center">3rd June 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Joan Maspons</td>
    </tr>
    <tr>
      <td align="center">73</td>
      <td align="center">3rd June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">72</td>
      <td align="center">3rd June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">71</td>
      <td align="center">3rd June 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Fabio Zottele</td>
    </tr>
    <tr>
      <td align="center">70</td>
      <td align="center">3rd June 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">69</td>
      <td align="center">3rd June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">68</td>
      <td align="center">2nd June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Cristian Adam</td>
    </tr>
    <tr>
      <td align="center">67</td>
      <td align="center">2nd June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Carlo Jeffery</td>
    </tr>
    <tr>
      <td align="center">66</td>
      <td align="center">2nd June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">65</td>
      <td align="center">2nd June 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">64</td>
      <td align="center">2nd June 2014</td>
      <td align="right">€&nbsp;15.00</td>
      <td>Robert Welti</td>
    </tr>
    <tr>
      <td align="center">63</td>
      <td align="center">2nd June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Christian Bay</td>
    </tr>
    <tr>
      <td align="center">62</td>
      <td align="center">2nd June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>David Leutwyler</td>
    </tr>
    <tr>
      <td align="center">61</td>
      <td align="center">2nd June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>AR Soft</td>
    </tr>
    <tr>
      <td align="center">60</td>
      <td align="center">2nd June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Henrique Sant'Anna</td>
    </tr>
    <tr>
      <td align="center">59</td>
      <td align="center">2nd June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Lamarque Souza</td>
    </tr>
    <tr>
      <td align="center">58</td>
      <td align="center">2nd June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Sandro Santos Andrade</td>
    </tr>
    <tr>
      <td align="center">57</td>
      <td align="center">2nd June 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">56</td>
      <td align="center">2nd June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">55</td>
      <td align="center">1st June 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Kjetil Kilhavn</td>
    </tr>
    <tr>
      <td align="center">54</td>
      <td align="center">1st June 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">53</td>
      <td align="center">1st June 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Sebastien Renard</td>
    </tr>
    <tr>
      <td align="center">52</td>
      <td align="center">1st June 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Tobias Henze</td>
    </tr>
    <tr>
      <td align="center">51</td>
      <td align="center">1st June 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Stijn Geuens</td>
    </tr>
    <tr>
      <td align="center">50</td>
      <td align="center">1st June 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td>pascal d'Hermilly</td>
    </tr>
    <tr>
      <td align="center">49</td>
      <td align="center">31st May 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">48</td>
      <td align="center">31st May 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Johannes Hackel</td>
    </tr>
    <tr>
      <td align="center">47</td>
      <td align="center">31st May 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Alfred Gebert</td>
    </tr>
    <tr>
      <td align="center">46</td>
      <td align="center">30th May 2014</td>
      <td align="right">€&nbsp;14.69</td>
      <td>Pedro Rosado</td>
    </tr>
    <tr>
      <td align="center">45</td>
      <td align="center">30th May 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Samikshan Bairagya</td>
    </tr>
    <tr>
      <td align="center">44</td>
      <td align="center">30th May 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Thomas Kerle</td>
    </tr>
    <tr>
      <td align="center">43</td>
      <td align="center">29th May 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Niels Slot</td>
    </tr>
    <tr>
      <td align="center">42</td>
      <td align="center">29th May 2014</td>
      <td align="right">€&nbsp;12.50</td>
      <td>Jose Millan Soto</td>
    </tr>
    <tr>
      <td align="center">41</td>
      <td align="center">29th May 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Lydia Pintscher</td>
    </tr>
    <tr>
      <td align="center">40</td>
      <td align="center">29th May 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Oliver Kellogg</td>
    </tr>
    <tr>
      <td align="center">39</td>
      <td align="center">29th May 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Xavier Corredor Llano</td>
    </tr>
    <tr>
      <td align="center">38</td>
      <td align="center">28th May 2014</td>
      <td align="right">€&nbsp;15.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">37</td>
      <td align="center">28th May 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Sylvain Leterreur</td>
    </tr>
    <tr>
      <td align="center">36</td>
      <td align="center">28th May 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Alexander Schmiechen</td>
    </tr>
    <tr>
      <td align="center">35</td>
      <td align="center">28th May 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">34</td>
      <td align="center">28th May 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Jarmo Koivunen</td>
    </tr>
    <tr>
      <td align="center">33</td>
      <td align="center">28th May 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">32</td>
      <td align="center">28th May 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Matt Williams</td>
    </tr>
    <tr>
      <td align="center">31</td>
      <td align="center">28th May 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Sergiy Kachanyuk</td>
    </tr>
    <tr>
      <td align="center">30</td>
      <td align="center">28th May 2014</td>
      <td align="right">€&nbsp;30.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">29</td>
      <td align="center">28th May 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">28</td>
      <td align="center">28th May 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">27</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">26</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Ilya Diallo</td>
    </tr>
    <tr>
      <td align="center">25</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Karl Ove Hufthammer</td>
    </tr>
    <tr>
      <td align="center">24</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Harald Fernengel</td>
    </tr>
    <tr>
      <td align="center">23</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Eugenio Accorsi</td>
    </tr>
    <tr>
      <td align="center">22</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;111.00</td>
      <td>Christian Loosli</td>
    </tr>
    <tr>
      <td align="center">21</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Philipp Schmidt</td>
    </tr>
    <tr>
      <td align="center">20</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">19</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;15.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">18</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;6.00</td>
      <td>MARIOS ANDREOPOULOS</td>
    </tr>
    <tr>
      <td align="center">17</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Michiel Schouten</td>
    </tr>
    <tr>
      <td align="center">16</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>gael beaudoin</td>
    </tr>
    <tr>
      <td align="center">15</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">14</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Andreas Lauser</td>
    </tr>
    <tr>
      <td align="center">13</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;25.00</td>
      <td>Gerry Gavigan</td>
    </tr>
    <tr>
      <td align="center">12</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Andrew Kingwell</td>
    </tr>
    <tr>
      <td align="center">11</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Gustav Widmark</td>
    </tr>
    <tr>
      <td align="center">10</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Christian Gmeiner</td>
    </tr>
    <tr>
      <td align="center">9</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;25.00</td>
      <td>Torsten Bielen</td>
    </tr>
    <tr>
      <td align="center">8</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Joel Bodenmann</td>
    </tr>
    <tr>
      <td align="center">7</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Robin Appelman</td>
    </tr>
    <tr>
      <td align="center">6</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">5</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;25.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">4</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Thomas Brix Larsen</td>
    </tr>
    <tr>
      <td align="center">3</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Pascal Mages</td>
    </tr>
    <tr>
      <td align="center">2</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">1</td>
      <td align="center">27th May 2014</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Sune Vuorela</td>
    </tr>
  </tbody>
</table>
