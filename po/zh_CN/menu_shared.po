msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-07 00:51+0000\n"
"PO-Revision-Date: 2023-01-12 10:32\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"SPDX-FileCopyrightText: FULL NAME <EMAIL@ADDRESS>\n"
"SPDX-License-Identifier: LGPL-3.0-or-later\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/websites-kde-org/menu_shared.pot\n"
"X-Crowdin-File-ID: 25280\n"

#: config.yaml:0
msgid "Develop"
msgstr "开发"

#: config.yaml:0 i18n/en.yaml:0
msgid "Get Involved"
msgstr "参与"

#: i18n/en.yaml:0
msgid "The next generation desktop for Linux"
msgstr "新一代 Linux 桌面环境"

#: i18n/en.yaml:0
msgid "Install on your computer"
msgstr "安装到您的电脑"

#: i18n/en.yaml:0
msgid "Discover Plasma"
msgstr "了解 Plasma 特性"

#: i18n/en.yaml:0
msgid "Buy a device with Plasma"
msgstr "购买预装 Plasma 的设备"

#: i18n/en.yaml:0
msgid "Powerful, multi-platform, and for everyone"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Use KDE software to surf the web, keep in touch with colleagues, friends and "
"family, manage your files, enjoy music and videos; and get creative and "
"productive at work. The KDE community develops and maintains more than "
"<strong>200</strong> applications which run on any Linux desktop, and often "
"other platforms too."
msgstr ""
"KDE 社区正在开发和维护 <strong>200</strong> 多款应用程序。您可以使用它们来浏"
"览互联网，与同事、朋友和家人保持联系，管理文件，欣赏音乐和观看视频。这些程序"
"能够在任意 Linux 桌面环境中运行，并且大部分也支持其他操作系统平台。"

#: i18n/en.yaml:0
msgid "See all applications"
msgstr "查看所有应用程序"

#: i18n/en.yaml:0
msgid "Krita"
msgstr "Krita"

#: i18n/en.yaml:0
msgid ""
"Get creative and draw beautiful artwork with Krita. A professional grade "
"painting application."
msgstr "Krita 是一款专业的数字绘画程序，让您尽情发挥创意，绘制出精美的作品。"

#: i18n/en.yaml:0
msgid "Krita screenshot"
msgstr "Krita 截图"

#: i18n/en.yaml:0
msgid "Kdenlive"
msgstr "Kdenlive"

#: i18n/en.yaml:0
msgid ""
"Kdenlive allows you to edit your videos and add special effects and "
"transitions."
msgstr ""
"Kdenlive 是一款专业的非线性编辑工具，可以对视频进行剪辑并添加特效与转场效果。"

#: i18n/en.yaml:0
msgid "Kdenlive Screenshot"
msgstr "Kdenlive 屏幕截图"

#: i18n/en.yaml:0
msgid "Kontact"
msgstr "Kontact"

#: i18n/en.yaml:0
msgid ""
"Handle all your emails, calendars and contacts within a single window with "
"Kontact."
msgstr "Kontact 是一款整合了电子邮件、日程表和联系人管理功能的应用程序。"

#: i18n/en.yaml:0
msgid "Kontact Screenshot"
msgstr "Kontact 截图"

#: i18n/en.yaml:0
msgid "Kdevelop"
msgstr "KDevelop"

#: i18n/en.yaml:0
msgid ""
"KDevelop is a cross-platform IDE for C, C++, Python, QML/JavaScript and PHP"
msgstr ""
"KDevelop 是一款支持 C、C++、Python、QML/JavaScript 和 PHP 的跨平台 IDE 程序。"

#: i18n/en.yaml:0
msgid "KDevelop Screenshot"
msgstr "KDevelop 屏幕截图"

#: i18n/en.yaml:0
msgid "GCompris"
msgstr "GCompris"

#: i18n/en.yaml:0
msgid ""
"GCompris is a high quality educational software suite, including a large "
"number of activities for children aged 2 to 10."
msgstr ""
"GCompris 是一款高品质的少儿教学软件，内含大量适合 2 至 10 岁儿童的教学活动。"

#: i18n/en.yaml:0
msgid "GCompris Screenshot"
msgstr "GCompris 屏幕截图"

#: i18n/en.yaml:0
msgid "LabPlot"
msgstr "LabPlot"

#: i18n/en.yaml:0
msgid ""
"FREE, open source and cross-platform Data Visualization and Analysis "
"software accessible to everyone."
msgstr "自由免费开源的跨平台数据可视化和分析软件，人人可用。"

#: i18n/en.yaml:0
msgid "LabPlot Screenshot"
msgstr "LabPlot 截图"

#: i18n/en.yaml:0
msgid "Devices"
msgstr "设备列表"

#: i18n/en.yaml:0
msgid "Buy a computer with Plasma preinstalled"
msgstr "购买预装 Plasma 的电脑"

#: i18n/en.yaml:0
msgid "Pinebook Pro"
msgstr "Pinebook Pro"

#: i18n/en.yaml:0
msgid ""
"The Pinebook Pro is an affordable ARM powered laptop. It is modular and "
"hackable in a way that only an Open Source project can be."
msgstr ""
"Pinebook Pro 是一款价格实惠的 ARM 处理器笔记本。它使用模块化设计的开源硬件，"
"可进行深度定制。"

#: i18n/en.yaml:0
msgid "KDE Slimbook"
msgstr "KDE Slimbook"

#: i18n/en.yaml:0
msgid ""
"The Slimbook is a shiny, sleek and good looking laptop that can do any task "
"thanks to its powerful AMD Ryzen processor."
msgstr ""
"Slimbook 是一款造型大方、款式新颖的笔记本电脑。它具备高性能 AMD Ryzen 处理"
"器，能够胜任各种工作。"

#: i18n/en.yaml:0
msgid "KFocus"
msgstr "KFocus"

#: i18n/en.yaml:0
msgid ""
"The Kubuntu Focus laptop is a high-powered, workflow-focused laptop which "
"ships with Kubuntu installed."
msgstr ""
"Kubuntu Focus 笔记本电脑是一款针对繁重工作流程设计的高性能笔记本电脑。预装 "
"Kubuntu 操作系统。"

#: i18n/en.yaml:0
msgid "Other hardware manufacturers are selling devices with Plasma."
msgstr "其他制造商也在销售预装 Plasma 的设备。"

#: i18n/en.yaml:0
msgid "See all devices"
msgstr "查看所有设备"

#: i18n/en.yaml:0
msgid "View all announcements"
msgstr "显示所有公告"

#: i18n/en.yaml:0
msgid "KDE Eco"
msgstr "KDE Eco"

#: i18n/en.yaml:0
msgid "Join the sustainable software movement"
msgstr "加入可持续软件运动"

#: i18n/en.yaml:0
msgid "Join KDE Eco"
msgstr "加入 KDE Eco"

#: i18n/en.yaml:0
msgid "The KDE Community"
msgstr "KDE 社区"

#: i18n/en.yaml:0
msgid "An international team developing and distributing Open Source software."
msgstr "致力于开发和推广自由开源软件的国际社区"

#: i18n/en.yaml:0
msgid ""
"Our community has developed a wide variety of applications for "
"communication, work, education and entertainment. We have a strong focus on "
"finding innovative solutions to old and new problems, creating a vibrant, "
"open atmosphere for experimentation."
msgstr ""
"KDE 社区针对通信、工作、教育和娱乐等各个领域开发了种类繁多的自由开源应用程"
"序。我们致力于为新老问题寻找创新的解决方案，为社区营造充满活力的、有利于孕育"
"试验精神的开放氛围。"

#: i18n/en.yaml:0
msgid "What is KDE?"
msgstr "KDE 是什么？"

#: i18n/en.yaml:0
msgid "Group photo of LaKademy 2018"
msgstr "2018 年度 KDE 拉丁学院会议合影"

#: i18n/en.yaml:0
msgid "Salvador, Brazil"
msgstr "巴西萨尔瓦多"

#: i18n/en.yaml:0
msgid "Plasma Sprint"
msgstr "Plasma 开发冲刺活动"

#: i18n/en.yaml:0
msgid "Berlin, Germany"
msgstr "德国柏林"

#: i18n/en.yaml:0
msgid "Kdenlive Sprint"
msgstr "Kdenlive 开发冲刺活动"

#: i18n/en.yaml:0
msgid "Paris, France"
msgstr "法国巴黎"

#: i18n/en.yaml:0
msgid "Delhi, India"
msgstr "印度德里"

#: i18n/en.yaml:0
msgid "Google Summer of Code Students"
msgstr "Google 编程之夏的参加学生"

#: i18n/en.yaml:0
msgid "KDE GSoC students group photo"
msgstr "KDE GSoC 学生集体照"

#: i18n/en.yaml:0
msgid "Florianopolis, Brazil"
msgstr "巴西弗洛里亚诺波利斯"

#: i18n/en.yaml:0
msgid "KDE Connect Sprint"
msgstr "KDE Connect 开发冲刺活动"

#: i18n/en.yaml:0
msgid "Barcelona"
msgstr "巴塞罗那"

#. %1 link
#: i18n/en.yaml:0
msgid "You can find the full list of changes [here](%1)."
msgstr "您也可以[查看完整更改列表](%1)。"

#: i18n/en.yaml:0
msgid "Spread the Word"
msgstr "一起传播 KDE 的消息"

#: i18n/en.yaml:0
msgid ""
"Non-technical contributors are an important part of KDE’s success. While "
"proprietary software companies have huge advertising budgets for new "
"software releases, KDE depends on people talking with other people. Even for "
"those who are not software developers, there are many ways to support the "
"KDE Applications release. Report bugs. Encourage others to join the KDE "
"Community. Or [support the nonprofit organization behind the KDE community]"
"(https://relate.kde.org/civicrm/contribute/transact?reset=1&amp;id=9)"
msgstr ""
"非技术型贡献者是 KDE 成功的重要保障之一。专有软件公司一般会给每次新版软件的发"
"布划拨庞大的宣发预算，而 KDE 则只能依靠人们口耳相传才能将信息传递出去。即使您"
"不是软件开发人员，一样可以通过多种方式来支持 KDE 应用程序的发展。例如报告程序"
"缺陷，鼓励其他人加入 KDE 社区，[支持 KDE 社区背后的非营利组织等](https://"
"relate.kde.org/civicrm/contribute/transact?reset=1&amp;id=9)"

#: i18n/en.yaml:0
msgid ""
"Please spread the word on the Social Web. Submit stories to news sites, use "
"channels like delicious, digg, reddit, and twitter. Upload screenshots of "
"your new set-up to services like Facebook, Flickr, ipernity and Picasa, and "
"post them to appropriate groups. Create screencasts and upload them to "
"YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with "
"'KDE'. This makes them easy to find, and gives the KDE Promo Team a way to "
"analyze coverage for this KDE Applications release."
msgstr ""
"请在社交网络上传播我们的声音。您可以向 KDE 资讯网站投稿，使用 Delicious、"
"Digg、Reddit 和 Twitter 等渠道发声。您也可以把自己的 KDE 软件截图发表到 "
"Facebook、Flickr、ipernity 与 Picasa 等网站的对应群组中。您还可以将使用过程的"
"屏幕录像上传到 YouTube、Blip.tv 与 Vimeo 等网站。请尽可能为您发表的内容打"
"上“KDE”标签，这将使 KDE 宣发团队能够分析该 KDE 应用程序发布信息的覆盖范围。"

#: i18n/en.yaml:0
msgid "Installing KDE Applications Binary Packages"
msgstr "安装 KDE 应用程序二进制软件包"

#: i18n/en.yaml:0
msgid "Packages"
msgstr "软件包"

#: i18n/en.yaml:0
msgid ""
"Some Linux/UNIX OS vendors have kindly provided binary packages of KDE "
"Applications for some versions of their distribution, and in other cases "
"community volunteers have done so. Additional binary packages, as well as "
"updates to the packages now available, may become available over the coming "
"weeks."
msgstr ""
"许多 Linux/UNIX 操作系统发行版会在官方软件仓库提供某个版本的 KDE 应用程序二进"
"制软件包。还有一些发行版则是由社区志愿者来打包 KDE 软件。这些发行版的新增软件"
"包以及现有软件包更新版本可能要经过数周后才能来到用户面前。"

#: i18n/en.yaml:0
msgid "Package Locations"
msgstr "软件包位置"

#: i18n/en.yaml:0
msgid ""
"For a current list of available binary packages of which the KDE Project has "
"been informed, please visit the [Community Wiki](https://community.kde.org/"
"KDE_Applications/Binary_Packages)."
msgstr ""
"要查看 KDE 项目当前可用的所有二进制软件包，请访问[社区百科](https://"
"community.kde.org/KDE_Applications/Binary_Packages)的相关页面。"

#: i18n/en.yaml:0
msgid "Compiling KDE Applications"
msgstr "编译 KDE 应用程序"

#. %1 link, %2 version number, %3 link
#: i18n/en.yaml:0
msgid ""
"The complete source code for KDE Applications may be [freely downloaded]"
"(%1). Instructions on compiling and installing are available from the [KDE "
"Applications %2 Info Page](%3)."
msgstr ""
"KDE 应用程序的完整源代码可以[自由下载](%1)。编译和安装指南可从 [KDE 应用程序 "
"%2 信息页面](%3) 查看。"

#: i18n/en.yaml:0
msgid "Trademark Notices."
msgstr "商标声明。"

#: i18n/en.yaml:0
msgid ""
"KDE<sup>&#174;</sup> and the K Desktop Environment<sup>&#174;</sup> logo are "
"registered trademarks of KDE e.V.."
msgstr ""
"KDE<sup>&#174;</sup> 和 K 桌面环境<sup>&#174;</sup> 徽标是KDE e.V. 的注册商"
"标。"

#: i18n/en.yaml:0
msgid "Linux is a registered trademark of Linus Torvalds."
msgstr "Linux 是 Linus Torvalds 的注册商标。"

#: i18n/en.yaml:0
msgid ""
"UNIX is a registered trademark of The Open Group in the United States and "
"other countries."
msgstr "UNIX 是 The Open Group 在美国和其他国家的注册商标。"

#: i18n/en.yaml:0
msgid ""
"All other trademarks and copyrights referred to in this announcement are the "
"property of their respective owners."
msgstr "本公告中提到的所有其他商标和版权物均为其所有者的财产。"

#: i18n/en.yaml:0
msgid "For more information send us an email:"
msgstr "欲了解更多信息，请向我们发送电子邮件："

#: i18n/en.yaml:0
msgid "Release Announcements"
msgstr "软件发布公告"

#. %1 version number
#: i18n/en.yaml:0
msgid "KDE Frameworks %1"
msgstr "KDE 程序框架 %1"

#: i18n/en.yaml:0
msgid "Installing binary packages"
msgstr "安装二进制软件包"

#. %1 link
#: i18n/en.yaml:0
msgid ""
"On Linux, using packages for your favorite distribution is the recommended "
"way to get access to KDE Frameworks. [Get KDE Software on Your Linux Distro "
"wiki page](%1)"
msgstr ""
"在 Linux 平台，我们推荐您从正在使用的发行版的软件仓库获取 KDE 程序框架。详情"
"请查看[相关百科页面](%1)"

#: i18n/en.yaml:0
msgid "Compiling from sources"
msgstr "从源代码编译"

#. %1 version number, %2 link, %3 link
#: i18n/en.yaml:0
msgid ""
"The complete source code for KDE Frameworks %1 may be [freely downloaded]"
"(%2). Instructions on compiling and installing are available from the [KDE "
"Frameworks %1 Info Page](%3)."
msgstr ""
"KDE 程序框架 %1 的完整源代码可以[自由下载](%2)。编译和安装指南可从 [KDE 程序"
"框架 %1 信息页面](%3) 查看。"

#. %1 link, %2 version number, %3 version number
#: i18n/en.yaml:0
msgid ""
"Building from source is possible using the basic <em>cmake .; make; make "
"install</em> commands. For a single Tier 1 framework, this is often the "
"easiest solution. People interested in contributing to frameworks or "
"tracking progress in development of the entire set are encouraged to [use "
"kdesrc-build](%1). Frameworks %2 requires Qt %3."
msgstr ""
"从源代码编译时可以使用最基本的 <em>cmake .; make; make install</em> 命令组"
"合。对于单个 1 级程序框架而言，这往往是最简便的解决方案。如果您有兴趣参与 "
"KDE 程序框架的开发，抑或是要跟踪整个程序框架集合的开发进度，请[使用 kdesrc-"
"build](%1)。KDE 程序框架 %2 需要 Qt %3。"

#: i18n/en.yaml:0
msgid ""
"A detailed listing of all Frameworks and other third party Qt libraries is "
"at [inqlude.org](https://inqlude.org), the curated archive of Qt libraries. "
"A complete list with API documentation is on [api.kde.org](https://api.kde."
"org/frameworks)."
msgstr ""
"您可以在 [inqlude.org](https://inqlude.org) 网站查看所有 KDE 应用程序框架和其"
"他第三方 Qt 程序库的详细列表，这是一个经过人工挑选的 Qt 程序库信息存档网站。"
"您还可以在 [api.kde.org](https://api.kde.org/frameworks) 网站查看完整的 API "
"文档。"

#: i18n/en.yaml:0
msgid ""
"Those interested in following and contributing to the development of "
"Frameworks can check out the [git repositories](https://invent.kde.org/"
"frameworks) and follow the discussions on the [KDE Frameworks Development "
"mailing list](https://mail.kde.org/mailman/listinfo/kde-frameworks-devel). "
"Policies and the current state of the project and plans are available at the "
"[Frameworks wiki](https://community.kde.org/Frameworks). Real-time "
"discussions take place on the [#kde-devel IRC channel on Libera Chat](irc://"
"irc.libera.chat/#kde-devel)."
msgstr ""
"如果您想关注或者参与 KDE 程序框架的开发工作，请查看该项目的 [Git 源代码仓库]"
"(https://invent.kde.org/frameworks)并关注 [KDE 程序框架开发工作邮件列表]"
"(https://mail.kde.org/mailman/listinfo/kde-frameworks-devel)中的讨论。您可在"
"[KDE 程序框架百科](https://community.kde.org/Frameworks)页面查看该项目的方"
"针、状态和计划，并在 [Libera Chat 的 #kde-devel IRC 频道](irc://irc.libera."
"chat/#kde-devel)参加实时讨论。"

#: i18n/en.yaml:0
msgid "Supporting KDE"
msgstr "支持 KDE"

#: i18n/en.yaml:0
msgid ""
"KDE is a [Free Software](https://www.gnu.org/philosophy/free-sw.html) "
"community that exists and grows only because of the help of many volunteers "
"that donate their time and effort. KDE is always looking for new volunteers "
"and contributions, whether it is help with coding, bug fixing or reporting, "
"writing documentation, translations, promotion, money, etc. All "
"contributions are gratefully appreciated and eagerly accepted. Please read "
"through the [Supporting KDE page](/community/donations/) for further "
"information or become a KDE e.V. supporting member through our [Join the "
"Game](https://relate.kde.org/civicrm/contribute/transact?id=5) initiative."
msgstr ""
"KDE 是一个[自由软件](https://www.gnu.org/philosphery/free-sw.html)社区，它的"
"存续和成长有赖于众多志愿者长年累月的不懈努力。KDE 欢迎新的志愿者加入社区，也"
"欢迎其他类型的贡献，包括代码、程序缺陷报告和修复、撰写文档、翻译、推广、资金"
"支持等。 我们将欣然接受并衷心感谢所有这些支持。请在[支持 KDE](/community/"
"donations/) 页面了解更多相关信息。您也可以通过我们的[加入游戏](https://"
"reinte.kde.org/civicrm/contribute/transact?id=5)计划成为一位 KDE e.V. 支持会"
"员。"

#: i18n/en.yaml:0
msgid "Plasma 5.20"
msgstr "Plasma 5.20"

#: i18n/en.yaml:0
msgid "New and improved inside and out"
msgstr "从里到外，焕然一新"

#: i18n/en.yaml:0
msgid ""
"A massive release, containing improvements to dozens of components, widgets, "
"and the desktop behavior in general."
msgstr ""
"这是一次大幅更新版本。涉及数十款程序组件、挂件和桌面环境行为的全面改进。"

#: i18n/en.yaml:0
msgid "View full changelog"
msgstr "查看完整更新日志"

#: i18n/en.yaml:0
msgid "Live Images"
msgstr "Live 镜像"

#: i18n/en.yaml:0
msgid ""
"The easiest way to try it out is with a live image booted off a USB disk. "
"Docker images also provide a quick and easy way to test Plasma."
msgstr ""
"试用新版 Plasma 桌面环境的最简单方法是使用新版 Live 镜像系统从 U 盘启动。我们"
"还提供了 Docker 镜像方便用户测试。"

#: i18n/en.yaml:0
msgid "Download live images with Plasma 5"
msgstr "下载 KDE Plasma 5 的 Live 镜像"

#: i18n/en.yaml:0
msgid "Download Docker images with Plasma 5"
msgstr "下载 KDE Plasma 5 的 Docker 镜像"

#: i18n/en.yaml:0
msgid "Package Downloads"
msgstr "软件包下载"

#: i18n/en.yaml:0
msgid ""
"Distributions have created, or are in the process of creating, packages "
"listed on our wiki page."
msgstr "发行版已经打包或正在打包那些在我们的百科页面上列出的软件包。"

#: i18n/en.yaml:0
msgid "Package download wiki page"
msgstr "软件包下载百科页面"

#: i18n/en.yaml:0
msgid "Source Downloads"
msgstr "源代码下载"

#: i18n/en.yaml:0
msgid "You can install Plasma 5 directly from source."
msgstr "您可以直接从源代码安装 KDE Plasma 5。"

#: i18n/en.yaml:0
msgid "Community instructions to compile it"
msgstr "社区编写的编译指南"

#: i18n/en.yaml:0
msgid "Source Info Page"
msgstr "源代码信息页面"

#: i18n/en.yaml:0
msgid "Feedback"
msgstr "提交反馈"

#: i18n/en.yaml:0
msgid "You can give us feedback and get updates on our social media channels:"
msgstr "您可以通过我们的社交媒体渠道向我们提交反馈并关注项目动态："

#: i18n/en.yaml:0
msgid ""
"Discuss Plasma 5 on the [KDE Forums Plasma 5 board](https://forum.kde.org/"
"viewforum.php?f=289)."
msgstr ""
"在 [KDE 论坛的 Plasma 5 版块](https://forum.kde.org/viewforum.php?f=289)参加"
"讨论。"

#: i18n/en.yaml:0
msgid ""
"You can provide feedback direct to the developers via the [#Plasma IRC "
"channel](irc://irc.libera.chat/#plasma), [Plasma-devel mailing list](https://"
"mail.kde.org/mailman/listinfo/plasma-devel) or report issues via [Bugzilla]"
"(https://bugs.kde.org/enter_bug.cgi?product=plasmashell&amp;format=guided). "
"If you like what the team is doing, please let them know!"
msgstr ""
"您可以通过 [#Plasma IRC 频道](irc://irc.libera.chat/#plasma)、[Plasma-devel "
"邮件列表](https://mail.kde.org/mailman/listinfo/plasma-devel)直接向开发者提供"
"反馈，也可在 [Bugzilla 程序缺陷跟踪平台](https://bugs.kde.org/enter_bug.cgi?"
"product=plasmashell&amp;format=guided)上报告问题。如果您喜欢 Plasma 团队的成"
"果，也请与我们分享您的喜悦之情！"

#: i18n/en.yaml:0
msgid "Your feedback is greatly appreciated."
msgstr "我们真诚感谢您的反馈。"

#: i18n/en.yaml:0
msgid "About KDE"
msgstr "关于 KDE"

#: i18n/en.yaml:0
msgid ""
"KDE is an international technology team that creates free and open source "
"software for desktop and portable computing. Among KDE's products are a "
"modern desktop system for Linux and UNIX platforms, comprehensive office "
"productivity and groupware suites and hundreds of software titles in many "
"categories including Internet and web applications, multimedia, "
"entertainment, educational, graphics and software development. KDE software "
"is translated into more than 60 languages and is built with ease of use and "
"modern accessibility principles in mind. KDE's full-featured applications "
"run natively on Linux, BSD, Windows and macOS."
msgstr ""
"KDE 是一支国际技术团队，它的使命是为桌面和便携式计算机编写自由开源软件。KDE "
"的产品包括适用于 Linux 和 UNIX 平台的新型桌面系统、功能齐全的办公生产力套件和"
"团队协作软件、以及涵盖了网络、多媒体、娱乐、教育、图像处理和软件开发等众多领"
"域的数百款应用软件。KDE 软件被翻译成 60 多种语言，以易用性和无障碍访问为前提"
"精心构建。KDE 的全功能应用程序在 Linux、BSD、Windows和 macOS 系统中能够以原生"
"状态运行。"

#: i18n/en.yaml:0
msgid "Next slide"
msgstr "下一张幻灯片"

#: i18n/en.yaml:0
msgid "Previous slide"
msgstr "上一张幻灯片"

#: i18n/en.yaml:0
msgid "KDE Community"
msgstr "KDE 社区"

#: i18n/en.yaml:0
msgid ""
"KDE is an open community of friendly people who want to create a world in "
"which everyone has control over their digital life and enjoys freedom and "
"privacy."
msgstr ""
"KDE 是一个气氛友善的开放社区。为了建设一个人人都能掌控自己的数字生活，并享有"
"自由和隐私的美好世界，来自五洲的志同道合者在此协力前行。"

#: i18n/en.yaml:0
msgid "Recent content on KDE Community"
msgstr "KDE 社区的最新内容"

#: i18n/en.yaml:0
msgid "KDE Plasma %ver, Bugfix Release for January"
msgstr "KDE Plasma %ver，一月份程序缺陷修复版本"

#: i18n/en.yaml:0
msgid "KDE Plasma %ver, Bugfix Release for February"
msgstr "KDE Plasma %ver，二月份程序缺陷修复版本"

#: i18n/en.yaml:0
msgid "KDE Plasma %ver, Bugfix Release for March"
msgstr "KDE Plasma %ver，三月份程序缺陷修复版本"

#: i18n/en.yaml:0
msgid "KDE Plasma %ver, Bugfix Release for April"
msgstr "KDE Plasma %ver，四月份程序缺陷修复版本"

#: i18n/en.yaml:0
msgid "KDE Plasma %ver, Bugfix Release for May"
msgstr "KDE Plasma %ver，五月份程序缺陷修复版本"

#: i18n/en.yaml:0
msgid "KDE Plasma %ver, Bugfix Release for June"
msgstr "KDE Plasma %ver，六月份程序缺陷修复版本"

#: i18n/en.yaml:0
msgid "KDE Plasma %ver, Bugfix Release for July"
msgstr "KDE Plasma %ver，七月份程序缺陷修复版本"

#: i18n/en.yaml:0
msgid "KDE Plasma %ver, Bugfix Release for August"
msgstr "KDE Plasma %ver，八月份程序缺陷修复版本"

#: i18n/en.yaml:0
msgid "KDE Plasma %ver, Bugfix Release for September"
msgstr "KDE Plasma %ver，九月份程序缺陷修复版本"

#: i18n/en.yaml:0
msgid "KDE Plasma %ver, Bugfix Release for October"
msgstr "KDE Plasma %ver，十月份程序缺陷修复版本"

#: i18n/en.yaml:0
msgid "KDE Plasma %ver, Bugfix Release for November"
msgstr "KDE Plasma %ver，十一月份程序缺陷修复版本"

#: i18n/en.yaml:0
msgid "KDE Plasma %ver, Bugfix Release for December"
msgstr "KDE Plasma %ver，十二月份程序缺陷修复版本"

#: i18n/en.yaml:0
msgid "KDE Plasma %ver Release"
msgstr "KDE Plasma %ver 正式版发布"

#: i18n/en.yaml:0
msgid "KDE Plasma %ver Beta Release"
msgstr "KDE Plasma %ver 测试版发布"

#: i18n/en.yaml:0
msgid "KDE Ships Plasma %ver."
msgstr "KDE 发布了 Plasma %ver。"

#: i18n/en.yaml:0
msgid "KDE Ships Plasma %ver Beta."
msgstr "KDE 发布了 Plasma %ver 测试版。"

#: i18n/en.yaml:0
msgid "Today KDE releases a bugfix update to KDE Plasma %maj_ver."
msgstr "KDE 于今天发布了 KDE Plasma %maj_ver 的一个程序缺陷修复更新版本。"

#: i18n/en.yaml:0
msgid "Release of KDE Frameworks %ver"
msgstr "KDE 程序框架 %ver 发布"

#: i18n/en.yaml:0
msgid "KDE Ships Frameworks %ver"
msgstr "KDE 发布 KDE 程序框架 %ver"

#: i18n/en.yaml:0
msgid "KDE Ships Frameworks %ver."
msgstr "KDE 发布 KDE 程序框架 %ver。"

#: i18n/en.yaml:0
msgid "KDE today announces the release of KDE Frameworks %ver."
msgstr "KDE 于今天正式发布了 KDE 程序框架 %ver。"

#. %1 version number
#: i18n/en.yaml:0
msgid "KDE today announces the release of KDE Frameworks %1."
msgstr "KDE 于今天正式发布了 KDE 程序框架 %1。"

#. %1 a number greater than or equal to 60, %2 link
#: i18n/en.yaml:0
msgid ""
"KDE Frameworks are %1 addon libraries to Qt which provide a wide variety of "
"commonly needed functionality in mature, peer reviewed and well tested "
"libraries with friendly licensing terms. For an introduction see [the KDE "
"Frameworks release announcement](%2)."
msgstr ""
"KDE 程序框架包含针对 Qt 设计的 %1 款附加程序库。这些程序库提供了种类丰富的常"
"用功能。它们成熟稳定，经过同行评审和全面测试，使用条款友好的许可证进行发布。"
"详情请见 [KDE 程序框架发布公告]%2。"

#: i18n/en.yaml:0
msgid ""
"This release is part of a series of planned monthly releases making "
"improvements available to developers in a quick and predictable manner."
msgstr ""
"本版软件是每月定期发布计划的一部分，方便开发人员在可预测的时间段及时获取软件"
"的各种改进。"

#: i18n/en.yaml:0
msgid "New in this version"
msgstr "此版软件更新内容"

#. %1 version number
#: i18n/en.yaml:0
msgid "KDE Gear %1"
msgstr "KDE Gear %1"

#. %1 version number
#: i18n/en.yaml:0
msgid "KDE announces the release of KDE Gear %1."
msgstr "KDE 发布了 KDE Gear %1。"

#. %1 a number greater than 100
#: i18n/en.yaml:0
msgid ""
"Over %1 individual programs plus dozens of programmer libraries and feature "
"plugins are released simultaneously as part of KDE Gear."
msgstr "超过 %1 款独立程序、程序库和功能插件作为 KDE Gear 的一部分同时发布。"

#: i18n/en.yaml:0
msgid ""
"Today they all get new bugfix source releases with updated translations."
msgstr "今天它们全部获得了新的程序缺陷修复后的源代码，并更新了翻译。"

#: i18n/en.yaml:0
msgid ""
"Today they all get new bugfix source releases with updated translations, "
"including:"
msgstr "今天它们全部获得了新的程序缺陷修复后的源代码，并更新了翻译，包括："

#: i18n/en.yaml:0
msgid ""
"Distro and app store packagers should update their application packages."
msgstr "发行版和软件商店的打包人员应更新他们的应用程序包。"

#. %1 version number, %2 link
#: i18n/en.yaml:0
msgid "[%1 release notes](%2) for information on tarballs and known issues."
msgstr "[%1 版本说明](%2) 对源代码的 tarballs 包和已知问题进行了说明。"

#. %1 version number
#: i18n/en.yaml:0
msgid "%1 source info page"
msgstr "%1 源代码信息页面"

#. %1 version number
#: i18n/en.yaml:0
msgid "%1 full changelog"
msgstr "%1 完整变更日志"

#. %1 version number
#: i18n/en.yaml:0
msgid "Image of Plasma %1"
msgstr "Plasma %1 的图像"

#. %1 version number
#: i18n/en.yaml:0
msgid "KDE Plasma %1"
msgstr "KDE Plasma %1"

#. %1 version number
#: i18n/en.yaml:0
msgid "Image of Plasma %1 Beta"
msgstr "Plasma %1 测试版图像"

#. %1 version number
#: i18n/en.yaml:0
msgid "KDE Plasma %1 Beta"
msgstr "KDE Plasma %1 测试版"

#. %1 major version number, %2 full version number
#: i18n/en.yaml:0
msgid "Today KDE releases a bugfix update to KDE Plasma %1, versioned %2."
msgstr "KDE 于今天发布了一个 KDE Plasma %1 的程序缺陷修复更新，版本号为 %2。"

#. %1 version number, %2 link, %3 year
#: i18n/en.yaml:0
msgid ""
"[Plasma %1](%2) was released in January %3 with many feature refinements and "
"new modules to complete the desktop experience."
msgstr ""
"[Plasma %1](%2) 于 %3 年一月发布，带来了大量的功能改进和新程序模块，为用户提"
"供最新的完整桌面环境体验。"

#. %1 version number, %2 link, %3 year
#: i18n/en.yaml:0
msgid ""
"[Plasma %1](%2) was released in February %3 with many feature refinements "
"and new modules to complete the desktop experience."
msgstr ""
"[Plasma %1](%2) 于 %3 年二月发布，带来了大量的功能改进和新程序模块，为用户提"
"供最新的完整桌面环境体验。"

#. %1 version number, %2 link, %3 year
#: i18n/en.yaml:0
msgid ""
"[Plasma %1](%2) was released in March %3 with many feature refinements and "
"new modules to complete the desktop experience."
msgstr ""
"[Plasma %1](%2) 于 %3 年三月发布，带来了大量的功能改进和新程序模块，为用户提"
"供最新的完整桌面环境体验。"

#. %1 version number, %2 link, %3 year
#: i18n/en.yaml:0
msgid ""
"[Plasma %1](%2) was released in April %3 with many feature refinements and "
"new modules to complete the desktop experience."
msgstr ""
"[Plasma %1](%2) 于 %3 年四月发布，带来了大量的功能改进和新程序模块，为用户提"
"供最新的完整桌面环境体验。"

#. %1 version number, %2 link, %3 year
#: i18n/en.yaml:0
msgid ""
"[Plasma %1](%2) was released in May %3 with many feature refinements and new "
"modules to complete the desktop experience."
msgstr ""
"[Plasma %1](%2) 于 %3 年五月发布，带来了大量的功能改进和新程序模块，为用户提"
"供最新的完整桌面环境体验。"

#. %1 version number, %2 link, %3 year
#: i18n/en.yaml:0
msgid ""
"[Plasma %1](%2) was released in June %3 with many feature refinements and "
"new modules to complete the desktop experience."
msgstr ""
"[Plasma %1](%2) 于 %3 年六月发布，带来了大量的功能改进和新程序模块，为用户提"
"供最新的完整桌面环境体验。"

#. %1 version number, %2 link, %3 year
#: i18n/en.yaml:0
msgid ""
"[Plasma %1](%2) was released in July %3 with many feature refinements and "
"new modules to complete the desktop experience."
msgstr ""
"[Plasma %1](%2) 于 %3 年七月发布，带来了大量的功能改进和新程序模块，为用户提"
"供最新的完整桌面环境体验。"

#. %1 version number, %2 link, %3 year
#: i18n/en.yaml:0
msgid ""
"[Plasma %1](%2) was released in August %3 with many feature refinements and "
"new modules to complete the desktop experience."
msgstr ""
"[Plasma %1](%2) 于 %3 年八月发布，带来了大量的功能改进和新程序模块，为用户提"
"供最新的完整桌面环境体验。"

#. %1 version number, %2 link, %3 year
#: i18n/en.yaml:0
msgid ""
"[Plasma %1](%2) was released in September %3 with many feature refinements "
"and new modules to complete the desktop experience."
msgstr ""
"[Plasma %1](%2) 于 %3 年九月发布，带来了大量的功能改进和新程序模块，为用户提"
"供最新的完整桌面环境体验。"

#. %1 version number, %2 link, %3 year
#: i18n/en.yaml:0
msgid ""
"[Plasma %1](%2) was released in October %3 with many feature refinements and "
"new modules to complete the desktop experience."
msgstr ""
"[Plasma %1](%2) 于 %3 年十月发布，带来了大量的功能改进和新程序模块，为用户提"
"供最新的完整桌面环境体验。"

#. %1 version number, %2 link, %3 year
#: i18n/en.yaml:0
msgid ""
"[Plasma %1](%2) was released in November %3 with many feature refinements "
"and new modules to complete the desktop experience."
msgstr ""
"[Plasma %1](%2) 于 %3 年十一月发布，带来了大量的功能改进和新程序模块，为用户"
"提供最新的完整桌面环境体验。"

#. %1 version number, %2 link, %3 year
#: i18n/en.yaml:0
msgid ""
"[Plasma %1](%2) was released in December %3 with many feature refinements "
"and new modules to complete the desktop experience."
msgstr ""
"[Plasma %1](%2) 于 %3 年十二月发布，带来了大量的功能改进和新程序模块，为用户"
"提供最新的完整桌面环境体验。"

#: i18n/en.yaml:0
msgid ""
"This release adds a week's worth of new translations and fixes from KDE's "
"contributors."
msgstr "此次发布包含了 KDE 社区成员一周以来贡献的新翻译和程序缺陷修复。"

#: i18n/en.yaml:0
msgid ""
"This release adds two weeks' worth of new translations and fixes from KDE's "
"contributors."
msgstr "此次发布包含了 KDE 社区成员两周以来贡献的新翻译和程序缺陷修复。"

#: i18n/en.yaml:0
msgid ""
"This release adds three weeks' worth of new translations and fixes from "
"KDE's contributors."
msgstr "此次发布包含了 KDE 社区成员三周以来贡献的新翻译和程序缺陷修复。"

#: i18n/en.yaml:0
msgid ""
"This release adds a month's worth of new translations and fixes from KDE's "
"contributors."
msgstr "此次发布包含了 KDE 社区成员一个月以来贡献的新翻译和程序缺陷修复。"

#: i18n/en.yaml:0
msgid ""
"This release adds two months' worth of new translations and fixes from KDE's "
"contributors."
msgstr "此次发布包含了 KDE 社区成员两个月以来贡献的新翻译和程序缺陷修复。"

#: i18n/en.yaml:0
msgid ""
"This release adds three months' worth of new translations and fixes from "
"KDE's contributors."
msgstr "此次发布包含了 KDE 社区成员三个月以来贡献的新翻译和程序缺陷修复。"

#: i18n/en.yaml:0
msgid ""
"This release adds five months' worth of new translations and fixes from "
"KDE's contributors."
msgstr "此次发布包含了 KDE 社区成员五个月以来贡献的新翻译和程序缺陷修复。"

#: i18n/en.yaml:0
msgid ""
"This release adds six months' worth of new translations and fixes from KDE's "
"contributors."
msgstr "此次发布包含了 KDE 社区成员六个月以来贡献的新翻译和程序缺陷修复。"

#: i18n/en.yaml:0
msgid "The bugfixes are typically small but important and include:"
msgstr "已修复的程序缺陷大多是些小问题，却依然重要，包括："

#: i18n/en.yaml:0
msgid "Compiling KDE Gear"
msgstr "编译 KDE Gear"

#. %1 version number, %2 link, %3 link
#: i18n/en.yaml:0
msgid ""
"The complete source code for KDE Gear %1 may be [freely downloaded](%2). "
"Instructions on compiling and installing are available from the [KDE Gear %1 "
"Info Page](%3)."
msgstr ""
"KDE Gear %1 的完整源代码可以[自由下载](%2)。编译和安装指南可从 [KDE Gear %1 "
"信息页面](%3) 查看。"

#: i18n/en.yaml:0
msgid ""
"Note that packages of this release might not be available on all "
"distributions at the time of this announcement."
msgstr "提示：在此公告发布时，并非所有发行版都能立即提供最新版软件。"

#. %1 version number
#: i18n/en.yaml:0
msgid "About Plasma %1"
msgstr "关于 Plasma %1"

#. %1 version number
#: i18n/en.yaml:0
msgid "Latest Release: Plasma %1"
msgstr "最新版本：Plasma %1"

#: i18n/en.yaml:0
msgid "Replay"
msgstr "重新播放"

#: i18n/en.yaml:0
msgid "Today"
msgstr "今天"

#: i18n/en.yaml:0
msgid "Years"
msgstr "年"

#: i18n/en.yaml:0
msgid "Months"
msgstr "月"

#: i18n/en.yaml:0
msgid "Days"
msgstr "日"

#: i18n/en.yaml:0
msgid "No events for today"
msgstr "今天没有事件"

#: i18n/en.yaml:0
msgid "Events"
msgstr "事件"

#: i18n/en.yaml:0
msgid "Friday 6 september 2021"
msgstr "2021 年 9 月 6 日，星期五"

#: i18n/en.yaml:0
msgid "Simple by default, Powerful when needed"
msgstr "默认状态简洁易用，按需定制功能强大"

#: i18n/en.yaml:0
msgid "Steam Deck"
msgstr "Steam Deck"

#: i18n/en.yaml:0
msgid ""
"The Steam Deck is a portable console that runs the latest AAA games with the "
"flexibility of a full PC."
msgstr ""
"Steam Deck 是一款便携式游戏主机，它能够运行最新的 3A 大作，并且具备完整电脑的"
"使用弹性。"

#: i18n/en.yaml:0
msgid "Get it"
msgstr ""

#: i18n/en.yaml:0
msgid "Fund it"
msgstr ""

#: i18n/en.yaml:0
msgid "Art by"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"<span class=\"blue\">You</span> Make <span class=\"blue\">KDE</span> Possible"
msgstr ""

#: i18n/en.yaml:0
msgid "Donate Now"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"You make KDE Gear possible with your support and donations. Help us keep "
"improving and building apps by using the donation form."
msgstr ""
